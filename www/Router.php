<?php

namespace cmsProject;
use cmsProject\core\Exceptions\NotFoundException;
use cmsProject\services\PageService;

class Router {

    private $params = [];
    private $routes;
    private $routeCalled;

    public function __construct()
    {
        $this->getRoutes();
    }

    // Génere le fichier de cache routes.php
    private function generateCache() {
        $yaml = yaml_parse_file("routes.yml");
        $phpRoutes = var_export($yaml, true);
        file_put_contents("routes.php",
            "<?php
                  namespace cmsProject;
                    class routes {
                      public function getRoutes()
                      {
                          return  ".$phpRoutes."; }}");
    }

    // Stock la liste des routes
    private function getRoutes()
    {
        if (!file_exists("route.php")) {
            $this->generateCache();
        }
        $routesList = new routes();
        $uriParams = explode('?', $_SERVER['REQUEST_URI'], 2);
        $this->routeCalled = $uriParams[0];
        if(isset($uriParams[1]))
            $this->params = $this->getParams($uriParams[1]);
        $this->routes =  $routesList->getRoutes();
        return $this;
    }

    // Accès aux pages en fonction de la route
    public function manageUrl() {

        if (!empty($this->routes[$this->routeCalled])) {
            $c =  'cmsProject\controllers\\'.ucfirst($this->routes[$this->routeCalled]["controller"]."Controller");
            $a =  $this->routes[$this->routeCalled]["action"]."Action";
            
            //Vérifie que la class existe
            if(class_exists($c))  {
                $controller = new $c();
                    
                //Vérifie si la méthode existe
                if (method_exists($controller, $a)) {
                    
                    $controller->$a($this->params);
                    
                } else {
                    new NotFoundException();
                }
            } else {
                new NotFoundException();
            }   
                
        } else {
            $pageService = PageService::getInstance();
            $page = $pageService->findPageByUri();
            if ($page == null) {
                new NotFoundException();
            } else {
                $pageService->accessPage($page);
            }
        }
    }

    // Récupère les paramètre de de la methode Get
    private function getParams($params) {
        $explodedParams = explode('&', $params, 2);
        $result = [];
        foreach($explodedParams as $param) {
            $data = explode("=", $param);
            if(isset($data[1]))
            $result[$data[0]] =  $data[1];
        }
        return $result;
   }

   // Vérifie si la route donné en arguement existe
   public function isInRoutes($route) {
        return !empty($this->routes[$route]);
   }

}
