<!DOCTYPE html>
<html>
<head>
    <title>Installation</title>
    <link rel="stylesheet" type="text/css" href="/public/dist/main.css">
    <link rel="icon" type="image/png" href="/images/favicon.png" />
</head>
<body>

<header>
    <div class="header header-front">
        <h1 class="text-center">MERCI D'AVOIR INSTALLE ARTSCMS</h1>
    </div>
</header>
<br><br><br>
<main>
    <?= $this->content ?>
</main>

</body>
</html>