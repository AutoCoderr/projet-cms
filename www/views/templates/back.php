<!DOCTYPE html>
<?php
use cmsProject\services\AccountService;
?>
<html>
  <head>
    <title>Dashboard</title>
     <link rel="stylesheet" type="text/css" href="/public/dist/main.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
     <script src="https://kit.fontawesome.com/b6184cc755.js" crossorigin="anonymous"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.js"></script>
     <script src="/public/js/dashboard.js"></script>
     <script src="/public/js/rights.js"></script>
     <script src="/public/js/script.js"></script>
     <script src="/public/js/config.js"></script>
      <link rel="icon" type="image/png" href="/images/favicon.png" />
      <script>
          logoutPath = "<?= \cmsProject\core\helpers::getUrl("user", "logout") ?>";
          deletePagePath = "<?= \cmsProject\core\helpers::getUrl('page', 'delete') ?>";
          deleteMenuPath = "<?= \cmsProject\core\helpers::getUrl('menu', 'delete') ?>";
          deleteOeuvrePath = "<?= \cmsProject\core\helpers::getUrl('oeuvre', 'delete') ?>";
          deleteExpositionPath = "<?= \cmsProject\core\helpers::getUrl('exposition', 'delete') ?>";
          deleteUserPath = "<?= \cmsProject\core\helpers::getUrl('user', 'delete') ?>";
          deleteCommentsZonePath = "<?= \cmsProject\core\helpers::getUrl('comment', 'deleteZone') ?>";
      </script>
     <script src="/public/js/rights.js"></script>
  </head>
  <body>
    <header>
      <div class="header header-back">
        <ul>
            <li><a href="/" class="button-mysite"><i class="fa fa-window-maximize"></i> Voir mon site</a></li>
            <li>Dashboard</li>
            <li><?= $_SESSION['firstname']." ".$_SESSION['lastname'] ?></li>
        </ul>
      </div>


      <div class="sidebar">
        <nav>
          <ul>
            <li><a href="<?= cmsProject\core\helpers::getUrl('dashboard', 'overview') ?>" name="overview" class="linkDashboard <?= $this->view == "overview" ? 'active' : '' ?>"><i class="fas fa-columns"></i>Overview</a></li>
            <li><a href="<?= cmsProject\core\helpers::getUrl('page', 'pages') ?>" name="pages" class="linkDashboard <?= $this->view == "pages" ? 'active' : '' ?>"><i class="fas fa-scroll"></i>Pages</a></li>
            <li><a href="<?= cmsProject\core\helpers::getUrl('menu', 'menus') ?>" name="menus" class="linkDashboard <?= $this->view == "menus" ? 'active' : '' ?>"><i class="fas fa-bars"></i>Menus</a></li>
            <li><a href="<?= cmsProject\core\helpers::getUrl('oeuvre', 'oeuvres') ?>" name="oeuvres" class="linkDashboard <?= $this->view == "oeuvres" ? 'active' : '' ?>"><i class="fas fa-paint-brush"></i>Oeuvres</a></li>
            <li><a href="<?= cmsProject\core\helpers::getUrl('exposition', 'expositions') ?>" name="expositions" class="linkDashboard <?= $this->view == "expositions" ? 'active' : '' ?>"><i class="fas fa-ethernet"></i>Expositions</a></li>
            <li><a href="<?= cmsProject\core\helpers::getUrl('dashboard', 'commentsZones') ?>" name="comments-zones" class="linkDashboard <?= $this->view == "comments-zones" ? 'active' : '' ?>"><i class="fas fa-comments"></i>Zones de commentaire</a></li>
            <?php if (isset($_SESSION['right']) && ($_SESSION['right'] == 'administrateur' )): ?>
              <li><a href="<?= cmsProject\core\helpers::getUrl('right', 'users') ?>" name="utilisateurs" class="linkDashboard <?= $this->view == "users" ? 'active' : '' ?>"><i class="fa fa-user"></i>Utilisateurs</a></li>
            <?php endif;?>
          </ul>
          <ul>
            <?php if (isset($_SESSION['right']) && ($_SESSION['right'] == 'administrateur' )): ?>
                <li><a href="<?= cmsProject\core\helpers::getUrl('dashboard', 'options') ?>" name="options" class="linkDashboard <?= $this->view == "options" ? 'active' : '' ?>"><i class="fas fa-cog"></i></a></li>
            <?php endif;?>
            <li><a id="button-logout" href="#"><i class="fas fa-sign-out-alt"></i></a></li>
          </ul>
        </nav>

      </div>
    </header>

    <main>
        <?= $this->content ?>
    </main>

    <script src="/public/js/modal.js"></script>
    <script>
        controllerToCall = "<?= $this->view ?>";
    </script>
  </body>
</html>