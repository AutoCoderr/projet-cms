<!DOCTYPE html>
<html lang="fr">

	<head>
	    <meta charset="UTF-8">
	    <title>Arts CMS : Error</title>
	    <link rel="stylesheet" type="text/css" href="public/dist/main.css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	    <script src="public/js/script.js"></script>
        <link rel="icon" type="image/png" href="/images/favicon.png" />
        <script>
            logoutPath = "<?= \cmsProject\core\helpers::getUrl("user", "logout") ?>";
        </script>
	</head>

	<body>
	    <header>
	    </header>

	    <main>
            <?= $this->content ?>
		</main>

	    <footer>
	    </footer>
	</body>

</html>