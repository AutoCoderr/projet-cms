<!DOCTYPE html>
<html lang="fr">

	<head>
	    <meta charset="UTF-8">
	    <title>ArtsCMS</title>
	    <link rel="stylesheet" type="text/css" href="/public/dist/main.css">
        <link rel="icon" type="image/png" href="/images/favicon.png" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://kit.fontawesome.com/b6184cc755.js" crossorigin="anonymous"></script>
        <script src="/public/js/profile.js"></script>
	</head>

	<body>
	    <header>
	        <div class="header header-front">
                <div class="container clearfix">
                    <a href="/" class="logo"><img src="/images/logo.png"></a>
                    <nav class="right">
                        <ul>
                            <?php if (isset($_SESSION['right']) && ($_SESSION['right'] == 'administrateur' || $_SESSION['right'] == 'mainteneur')): ?>
                                <li><a href="<?= \cmsProject\core\helpers::getUrl("dashboard", "overview") ?>">Accès Dashboard</a></li>
                            <?php endif; ?>
                            <?php if(!isset($_SESSION['user_id'])): ?>
                                <li><a href="<?= \cmsProject\core\helpers::getUrl("user", "register") ?>">Inscription</a></li>
                                <li><a href="<?= \cmsProject\core\helpers::getUrl("user", "login") ?>">Connexion</a></li>
                            <?php else: ?>
                                <li><a href='javascript:logout()'>Se déconnecter</a></li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                </div>
	        </div>

	    </header>

	    <main>
            <?= $this->content ?>
		</main>

	    <footer>
            <div class="container clearfix">
                <p>Tous les droits réservés <?= date('Y') ?> - Desigined avec Arts CMS</p>
            </div>
	    </footer>

        <script src="public/js/modal.js"></script>
        <?php if (isset($_SESSION['user_id'])): ?>
            <script>
                function logout() {
                    if (confirm('Voulez-vous vous déconnecter?')) {
                        location.href = '<?= \cmsProject\core\helpers::getUrl("user", "logout") ?>';
                    }
                }
            </script>
        <?php endif; ?>
	</body>
</html>