<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/public/dist/main.css">
        <script src="https://cdn.tiny.cloud/1/gwcafl941x0dqh003xk62ne2lq3qq4cn1xn5ki9e3izrjewv/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <link rel="icon" type="image/png" href="/images/favicon.png" />
    </head>
    <body>
        <header>
            <ul>
                <li><a href="<?= \cmsProject\core\helpers::getUrl("page", "pages") ?>">Retour au dashboard</a></li>
            </ul>
        </header>
        <main>
            <center>
                <?= $this->content ?>
            </center>
        </main>
        <footer></footer>
    </body>
    <script src="/public/js/modal.js"></script>
</html>