<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <title>ArtsCMS</title>
        <link rel="stylesheet" type="text/css" href="/public/dist/main.css">
        <link rel="icon" type="image/png" href="/images/favicon.png" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://kit.fontawesome.com/b6184cc755.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="header header-front">
            <div class="container clearfix">
                <nav class="left">
                    <ul>
                        <li><a href="/" class="nohover"><?= SITE_NAME ?></a></li>
                        <?php if (count($menus) > 0): ?>
                            <?php if (count($menus) == 1): ?>
                                <?php foreach ($menus[0]->getItems() as $item): ?>
                                    <li><a href="<?= \cmsProject\core\helpers::getPageUri($item->getPage()) ?>"><?= $item->getPage()->getNom() ?></a></li>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <?php foreach ($menus as $menu): ?>
                                    <li>
                                        <div class="dropdown">
                                            <a><?= $menu->getNom()?></a>
                                            <div class="dropdown-content">
                                                <?php foreach ($menus[0]->getItems() as $item): ?>
                                                    <a class="nohover" href="<?= \cmsProject\core\helpers::getPageUri($item->getPage()) ?>"><?= $item->getPage()->getNom() ?></a>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </ul>
                </nav>
                <nav class="right">
                    <ul>
                        <?php if (!isset($_SESSION['user_id'])): ?>
                            <li><a href='<?= \cmsProject\core\helpers::getUrl("user", "login") ?>'>Se connecter</a></li>
                            <li><a href='<?= \cmsProject\core\helpers::getUrl("user", "register") ?>'>S'inscrire</a></li>
                        <?php else: ?>
                            <li><a href='<?= \cmsProject\core\helpers::getUrl("user", "profile") ?>'>Mon profil</a></li>
                            <?php if (isset($_SESSION['right']) && ($_SESSION['right'] == 'administrateur' || $_SESSION['right'] == 'mainteneur')): ?>
                                <li><a target='_blank' href='<?= \cmsProject\core\helpers::getUrl("dashboard", "overview") ?>'>Accès dashboard</a></li>
                            <?php endif;?>
                            <li><a href='javascript:logout()'>Se déconnecter</a></li>
                        <?php endif; ?>
                    </ul>
                </nav>
            </div>
        </div>
        <br/>
        <br/>
        <br/>
        <main>
            <div class="container clearfix">
                <?= $this->content ?>
            </div>
        </main>

        <footer>
            <div class="container clearfix">
                <p>Desigined avec Arts CMS</p>
            </div>
	    </footer>

    </body>
        <?php if (isset($_SESSION['user_id'])): ?>
            <script>
                function logout() {
                    if (confirm('Voulez-vous vous déconnecter?')) {
                        location.href = '<?= \cmsProject\core\helpers::getUrl("user", "logout") ?>';
                    }
                }
            </script>
        <?php endif; ?>
</html>