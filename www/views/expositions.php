<div class="dashboard-section">
    <div class="row">
        <div class="col-md-12">
            <h1>Expositions<a id="dasboardplus"><i class="fas fa-plus fa-1x button-dasboardplus"></i></a></h1>

            <div id="dashboard-modal" class="modal" <?= (isset($errors) && isset($errors['addExposition']) && count($errors['addExposition']) > 0) ? "style='display: block;'" : "" ?>>
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <h1>Nouvelle Exposition</h1>

                    <?php $this->addModal("form", cmsProject\models\expositions::getAddExpositionForm()); ?>
                    

                </div>
            </div>

            <?php if(!empty($expositions)): ?>
                <table class="table-dashboard table-cl-4">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Description</th>
                        <th>Lieu d'exposition</th>
                        <th>Date</th>
                        <th>Modifier</th>
                        <th>Supprimer</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($expositions as $exposition): ?>
                            <tr>
                                <td><?= $exposition->getNom() ?></td>
                                <td><?= $exposition->getDescription() ?></td>
                                <td><?= $exposition->getLieux() ?></td>
                                <td><?= $exposition->getDate() ?></td>
                                <td><a href="<?= cmsProject\core\helpers::getUrl("exposition", "getExpositionById"). '?exposition_id='. $exposition->getId() ?>" class="btn_edit"><i class="fas fa-pen"></i></a></td>
                                <td><a href="javascript:confirmationExposition('<?= $exposition->getId() ?>')"><i class="fas fa-trash"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>