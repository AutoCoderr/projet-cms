<div class="dashboard-section">
    <div class="row">
        <div class="col-md-12">
            <h1>Oeuvres<a id="dasboardplus"><i class="fas fa-plus fa-1x button-dasboardplus"></i></a></h1>

            <div id="dashboard-modal" class="modal">
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <h1>Nouvelle Oeuvre</h1>
                    
                    <?php use cmsProject\models\oeuvres;

                    $this->addModal("form", oeuvres::getAddOeuvreForm()); ?>

                </div>
            </div>

            <?php if(!empty($oeuvres)): ?>
                <table class="table-dashboard table-cl-4">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Artiste</th>
                        <th>Date</th>
                        <th>Date de création</th>
                        <th>Modifier</th>
                        <th>Supprimer</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($oeuvres as $oeuvre): ?>
                            <tr>
                                <td><?= $oeuvre->getNom() ?></td>
                                <td><?= $oeuvre->getArtiste() ?></td>
                                <td><?= $oeuvre->getDate_realisation() ?></td>
                                <td><?= $oeuvre->getCreated_at() ?></td>
                                <td><a href="<?= cmsProject\core\helpers::getUrl("oeuvre", "getOeuvreById"). '?oeuvre_id='. $oeuvre->getId() ?>" class="btn_edit"><i class="fas fa-pen"></i></a></td>
                                <td><a href="javascript:confirmationOeuvre('<?= $oeuvre->getId() ?>')"><i class="fas fa-trash"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>