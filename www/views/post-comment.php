<!-- Afficher des commentaires -->
<section class="comments">
    <div class="container clearfix">
        <h1><?= $commentZone->getNom() ?> :</h1>
        <h2>Commentaires</h2>

        <!-- Affiche tout les commentaires de la zone -->
        <?php foreach ($comments as $comment):?>
            <p>De : <strong><?php echo $comment->getUser()->getFirstname(); ?></strong></p>
            <?php echo $comment->getContent(); ?>

            <!-- bouton de supression pour les admin et l'auteur du commentaire -->
            <?php if (isset($_SESSION['right']) && ($_SESSION['right'] == "administrateur" || $_SESSION['user_id'] == $comment->getUser()->getId())): ?>
                <a class="button btn-delete-comment" href="<?= \cmsProject\core\helpers::getUrl("comment", "delete"). '?comment_id='. $comment->getId() ?>"><i class="fas fa-trash"></i></a>
            <?php endif; ?>
            <hr>
        <?php endforeach;?>

        <!-- Ajouter un commentaire-->

        <?php
            if (isset($_SESSION['user_id']))
                $this->addModal("form", \cmsProject\models\comments::getAddCommentForm($commentZone->getId()));
            else
                echo "<h3>Veuillez vous connecter pour pouvoir poster un commentaire</h3>";
        ?>
    </div>
</section>