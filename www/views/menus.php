<div class="dashboard-section">
    <div class="row">
        <div class="col-md-12">
            <h1>Menu<a id="dasboardplus"><i class="fas fa-plus fa-1x button-dasboardplus"></i></a></h1>

            <div id="dashboard-modal" class="modal">
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <h1>Nouveau Menu</h1>

                    <?php use cmsProject\models\menus;

                    $this->addModal("form", menus::getAddMenuForm()); ?>

                </div>
            </div>

            <?php if(!empty($menus)): ?>
                <table class="table-dashboard table-cl-3">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Pages</th>
                        <th>Date de création</th>
                        <th>Modifier</th>
                        <th>Supprimer</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($menus as $menu): ?>
                            <tr>
                                <td><?= $menu->getNom() ?></td>
                                <td><?= implode(" ; ", $menu->getPagesNames()) ?></td>
                                <td><?= $menu->getCreated_at() ?></td>
                                <td><a href="<?= cmsProject\core\helpers::getUrl("menu", "getMenuById"). '?menu_id='. $menu->getId() ?>" class="btn_edit"><i class="fas fa-pen"></i></a></td>
                                <td><a href="javascript:confirmationMenu('<?= $menu->getId() ?>')"><i class="fas fa-trash"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>