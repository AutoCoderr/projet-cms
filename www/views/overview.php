<div class="dashboard-section">
	<div class="row">

        <div class="col-md-12">
            <h1>Overview</h1>

			<div class="container">
				 <div id="graphs" class="graphs"><canvas id="chart"></canvas></div>
			</div>
		</div>
    </div>
</div>