<section id="section2">
    <div class="container clearfix">

        <h1>Connexion</h1>

        <?php $this->addForm( "form", $loginForm ); ?>

        <a href="<?= \cmsProject\core\helpers::getUrl("user", "forgotpassword") ?>">Mot de passe oublié ?</a>
    </div>
</section>