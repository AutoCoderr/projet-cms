<div id="select<?= ucfirst(strtolower($data['name'])) ?>-modal" class="modal">
    <div class="modal-content">
        <span class="close-select<?= ucfirst(strtolower($data['name'])) ?>">x</span>
        <h1><?= $data['label'] ?></h1>
        <div class="row">

            <div class="col-md-6 col-sm-12">
                <div class="vertical-divider"></div>
                <?php if (count($data['elements']) > 0): ?>
                    <select id="<?= strtolower($data['name']) ?>">
                        <?php foreach ($data['elements'] as $element): ?>
                            <option value="<?= $element->getId() ?>"><?= $element->getNom() ?></option>
                        <?php endforeach; ?>
                    </select><br/>
                    <input type="button" value="Ajouter" id="add<?= ucfirst(strtolower($data['name'])) ?>">
                <?php else: ?>
                    <h2><?= $data['labelIfNothing'] ?></h2>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>