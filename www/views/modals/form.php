
<?php $inputData = $GLOBALS["_".(strtoupper($data["config"]["method"])??"POST")]; ?>

<form 
method="<?= $data["config"]["method"]?>" 
action="<?= $data["config"]["action"]?>"
id="<?= $data["config"]["id"]?>"
class="form-style">

      <?php foreach ($data["fields"] as $name => $config): ?>
              <?php if (!empty($config["labelTag"])):?>
                  <<?= $config["labelTag"]?>> <?= ($config["label"]??'').($config["required"] ? " *" : "") ?> </<?= $config["labelTag"]?>>
              <?php elseif ($config["type"] != "hidden"): ?>
                  <label for="<?= $name ?>"><?= ($config["label"]??'').($config["required"] ? " *" : "") ?></label>
              <?php endif; ?>
              <?php if ($config["type"] == "textarea"): ?>
                  <textarea name="<?= $name ?>"
                         placeholder="<?= $config["placeholder"]??''?>"
                         class="<?= $config["class"]??''?>"
                         id="<?= $config["id"]??''?>"
                         minlength="<?= $config["minlength"]??'' ?>"
                         maxlength="<?= $config["maxlength"]??'' ?>"
                         value="<?= $inputData[$name]??$config["value"]??''?>"
                         <?= $config["required"] ? " required='required'" : ""?>>
                  </textarea>
              <?php elseif ($config["type"] == "select"): ?>
                <select name="<?= $name ?>"
                        class="<?= $config["class"]??''?>"
                        id="<?= $config["id"]??''?>"
                    <?= $config["required"] ? " required='required'" : ""?>>
                    <?php foreach ($config["options"] as $option): ?>
                        <option value="<?= $option["value"] ?>"
                            <?= isset($config["value"]) && $config["value"] == $option["value"] ? " checked=checked" : "" ?>>
                            <?= $option["display"] ?></option>
                    <?php endforeach; ?>
                </select>
              <?php elseif ($config["type"] == "listCanAddOrDelete"): ?>
                <div name="<?= $name ?>"></div>
                <div name="<?= "other_".$name ?>"></div>
              <?php else: ?>
                  <input type="<?= $config["type"]??'' ?>"
                         name="<?= $name ?>"
                         placeholder="<?= $config["placeholder"]??''?>"
                         class="<?= $config["class"]??''?>"
                         id="<?= $config["id"]??''?>"
                         minlength="<?= $config["minlength"]??'' ?>"
                         maxlength="<?= $config["maxlength"]??'' ?>"
                         value="<?= $inputData[$name]??$config["value"]??''?>"
                         <?= $config["required"] ? " required='required'" : "" ?>
                  >
              <?php endif;?>
        <?php endforeach;?>
    <input type="submit" class="button-blue input-button" value="<?= $data["config"]["submit"]??'Valider' ?>">
</form>
<?php
if (isset($this->data["errors"][$data["config"]["actionName"]])) {
    echo "<ul>";
    foreach ($this->data["errors"][$data["config"]["actionName"]] as $error) {
        echo("<li style='color: red; margin-left: -30px'>".$error."</li>");
    }
    echo "</ul>";
} else if (isset($this->data["success"][$data["config"]["actionName"]])) {
    echo "<font color='green'>".$this->data["success"][$data["config"]["actionName"]]."</font>";
}
?>