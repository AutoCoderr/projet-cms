<div class="dashboard-section">
    <div class="row">
        <div class="col-md-12">
            <h1>Pages<a id="dasboardplus"><i class="fas fa-plus fa-1x button-dasboardplus"></i></a></h1>

            <div id="dashboard-modal" class="modal" <?= (isset($errors) && isset($errors['addPage']) && count($errors['addPage']) > 0) ? "style='display: block;'" : "" ?>>
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <h1>Nouvelle Page</h1>

                    <?php $this->addModal("form", \cmsProject\models\pages::getAddPagesForm()); ?>

                </div>
            </div>

            <?php if(!empty($pages)): ?>
                <table class="table-dashboard table-cl-3">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Uri</th>
                        <th>Date de création</th>
                        <th>Modifier</th>
                        <th>Editer la page</th>
                        <th>Supprimer</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($pages as $page): ?>
                            <tr>
                                <td><?= $page->getNom() ?></td>
                                <td><?= $page->getUri() ?></td>
                                <td><?= $page->getCreated_at() ?></td>
                                <td>
                                    <a href="<?= $page->getUri() != "/" ? \cmsProject\core\helpers::getUrl("page", "getPageById").'?page_id='. $page->getId() : "#" ?>"
                                       class="<?= $page->getUri() != "/" ? "btn_edit" : "" ?>">
                                        <i class="fas fa-pen <?= $page->getUri() == "/" ? "gray" : "" ?>"></i>
                                    </a>
                                </td>
                                <td><a href="<?= \cmsProject\core\helpers::getUrl("page", "edit")?>?id=<?= $page->getId() ?>"><i class="fa fa-pencil-square-o"></i></a></td>
                                <td><a href="<?= $page->getUri() != "/" ? "javascript:confirmationPage('".$page->getId()."')" : "#" ?>">
                                        <i class="fas fa-trash <?= $page->getUri() == "/" ? "gray" : "" ?>"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                       
                    </tbody>
                </table> 
            <?php endif; ?>
        </div>
    </div>
</div>