<div class="container">
    <h1 class="text-center"> Formulaire de Configuration</h1><br><br>
<?php
    if(isset($errors)){
        echo '<ul style="list-style:none;">';
        foreach ($errors as $error){
            echo '<li style="color: red; text-align: center" >'.$error.'</li>';
        }
        echo '</ul>';
    }
    $installer->generateForm();
?>
</div>