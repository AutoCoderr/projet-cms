<section id="section2">
    <div class="container clearfix">

        <h1>Mon Profil</h1>

        <div class="col-md-12">
            <div class="tabbed_area">
		        <ul class="tabs">
		            <li><a href="" id="profileTab" class="active">Informations</a></li>
		            <li><a href="" id="changePwdTab">Mot de passe</a></li>
		        </ul>
            </div>

            <div id="profile">
                <h3>Informations principales</h3>
                <?php $this->addForm( "form", $profileForm ); ?>
                <a class="button" href="<?= cmsProject\core\helpers::getUrl("user", "delete"). '?user_id='. $_SESSION['user_id'] ?>">Supprimer mon compte</a>
                
            </div>

            <div id="changePwd">
                <h3>Changement de mot de passe</h3>
                <?php $this->addForm( "form", $changePwdForm ); ?>
            </div>
		</div>

    </div>
</section>