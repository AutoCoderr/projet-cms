<form 
method="<?= $data->getConfig()['method'] ?>" 
action="<?= $data->getConfig()['action'] ?>"
name="<?= $data->getName() ?>"
<?php foreach($data->getConfig()['attr'] as $attr => $value)
{
  echo "$attr = '$value' ";
}
?>>
<?php
      if(!empty($data->getErrors()))
      {
        ?>
          <div class="alert alert-warning">
            <ul>
              <?php
                foreach($data->getErrors() as $key => $errorsPerField)
                {
                  foreach($errorsPerField as $error)
                  {
                    echo "<li>$error</li>";
                  }
                
                }
              ?>
            </ul>
          </div>
        <?php
      }

      echo "<br><br>";
?>


      <?php foreach ($data->getElements() as $key => $field):?>

          <!---------------SUBMIT --->
            <?php if($field->getType() == "submit"):?>
              <button
              <?php 
                if(isset($field->getOptions()['attr'])) {
                  foreach($field->getOptions()['attr'] as $attr => $value)
                    {
                      echo "$attr = '$value' ";
                    }
                  }
                  ?>
              >
                
              <?= $field->getOptions()["label"]??'' ?>
            </button>
            <?php endif;?>
            
            <!---------------TEXT --->
            <?php if($field->getType() == "text"):?>

              <label
                  for="<?= $field->getName() ?>"
                  <?php 
                  if(isset($field->getOptions()['attr_label'])) {
                    foreach($field->getOptions()['attr_label'] as $attr => $value)
                      {
                        echo "$attr = '$value' ";
                      }
                  }
                  
                    ?>
              ><?= $field->getOptions()["label"] ?><br><br></label>
              
              <input 
                value="<?= $field->getOptions()['value'] ?? $_POST[$field->getName()] ?? '' ?>"
                type="text"
                name="<?= $field->getName() ?>"
               
                <?php 
                if(isset($field->getOptions()['attr'])) {
                  foreach($field->getOptions()['attr'] as $attr => $value)
                    {
                      echo "$attr = '$value' ";
                    }
                  }
                  ?>
                <?=(!empty($field->getOptions()["required"]))?"required='required'":""?> >
              <?php endif;?>

            <!---------------PASSWORD --->
            <?php if($field->getType() == "password"):?>

              <label
                  for="<?= $field->getName() ?>"
                  <?php 
                  if(isset($field->getOptions()['attr_label'])) {
                    foreach($field->getOptions()['attr_label'] as $attr => $value)
                      {
                        echo "$attr = '$value' ";
                      }
                  }
                  
                    ?>
              ><?= $field->getOptions()["label"] ?><br><br></label>
              
              
              <input 
                value=""
                type="password"
                name="<?= $field->getName() ?>"
               
                <?php 
                if(isset($field->getOptions()['attr'])) {
                  foreach($field->getOptions()['attr'] as $attr => $value)
                    {
                      echo "$attr = '$value' ";
                    }
                  }
                  ?>
                <?=(!empty($field->getOptions()["required"]))?"required='required'":""?> >
              <?php endif;?>

             <!---------------EMAIL --->
            <?php if($field->getType() == "email"):?>

              <label
                  for="<?= $field->getName() ?>"
                  <?php 
                  if(isset($field->getOptions()['attr_label'])) {
                    foreach($field->getOptions()['attr_label'] as $attr => $value)
                      {
                        echo "$attr = '$value' ";
                      }
                  }
                  
                    ?>
              ><?= $field->getOptions()["label"] ?><br><br></label>
              
              <input 
                value="<?= $field->getOptions()['value'] ?? $_POST[$field->getName()] ?? '' ?>"
                type="email"
                name="<?= $field->getName() ?>"
               
                <?php 
                if(isset($field->getOptions()['attr'])) {
                  foreach($field->getOptions()['attr'] as $attr => $value)
                    {
                      echo "$attr = '$value' ";
                    }
                  }
                  ?>
                <?=(!empty($field->getOptions()["required"]))?"required='required'":""?> >
              <?php endif;?>
      <?php endforeach;?>
</form>
