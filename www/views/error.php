<section style="text-align: center;height:99vh;">
    <div class="container clearfix">
        <h1><?= $error ;?></h1>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <img src="../../images/panneau-oops.png">
            </div>
            <div class="col-md-12 col-sm-12">
                <a class="button-orange" href="javascript:history.back()">Revenir à la page précédente</a>
            </div>
            <div class="col-md-12 col-sm-12">
            <a class="button" href="/">Revenir à la page d'accueil</a>
            </div>
        </div>
    </div>
</section> 