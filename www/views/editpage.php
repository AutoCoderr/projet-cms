<h1>Editer : <?= $page->getNom() ?> (<?= $page->getUri() ?>)</h1>
<div class="container" style="height: 45px; width: 100%;">
    <img onclick="save()" id="save_page_button" src="/images/save.png" style=""/>
</div>
<div id="save_answer"></div>
<textarea style="height: 500px;" id="tinyMCE_elem" name="content">
</textarea>
<input type="hidden" name="id" value="<?= $page->getId() ?>"/>
<a target="_blank" href="<?= \cmsProject\core\helpers::getPageUri($page) ?>">Accéder à la page</a>
<h2>Les menus présents dans cette page :</h2>
<?php if (count($menus) > 0): ?>
    <ul>
        <?php $nb = 0; ?>
        <?php foreach ($menus as $menu): ?>
            <?php $nb += 1; ?>
            <li>
                    <span><?= $menu->getNom() ?></span>
                    <?php if (count($menus) > 1): ?>
                        <?php if ($nb < count($menus)): ?>
                            <form method="post" action="<?= \cmsProject\core\helpers::getUrl("menu", "upOrDownDisplay") ?>">
                                <input type="submit" value="Down"/>
                                <input type="hidden" name="menu" value="<?= $menu->getId() ?>">
                                <input type="hidden" name="page" value="<?= $page->getId() ?>">
                                <input type="hidden" name="action" value="down">
                            </form>
                        <?php endif; ?>
                        <?php if ($nb > 1): ?>
                            <form method="post" action="<?= \cmsProject\core\helpers::getUrl("menu", "upOrDownDisplay") ?>">
                                <input type="submit" value="Up"/>
                                <input type="hidden" name="menu" value="<?= $menu->getId() ?>">
                                <input type="hidden" name="page" value="<?= $page->getId() ?>">
                                <input type="hidden" name="action" value="up">
                            </form>
                        <?php endif; ?>
                    <?php endif; ?>
                    <form method="post" action="<?= \cmsProject\core\helpers::getUrl("menu", "removeFromPage")?>">
                        <input type="hidden" name="menu" value="<?= $menu->getId() ?>">
                        <input type="hidden" name="page" value="<?= $page->getId() ?>">
                        <input type="submit" value="Retirer"/>
                    </form>
            </li>
        <?php endforeach; ?>
    </ul>
<?php else: ?>
    <p>Aucun menu</p>
<?php endif; ?>
<?php if (count($otherMenus) > 0): ?>
    <?php $this->addModal("form", \cmsProject\models\pages::getAddMenuToPageForm($otherMenus,$page->getId())) ?>
<?php endif; ?>
<br/>
<br/>
<?php $this->addModal("modalSelect", \cmsProject\models\oeuvres::getSelectOeuvreModal($oeuvres)); ?>
<?php $this->addModal("modalSelect", \cmsProject\models\expositions::getSelectExpositionModal($expositions)); ?>
<?php $this->addModal("modalSelect", \cmsProject\models\comments_zones::getSelectCommentsZonesModal($comments_zones)); ?>
<script>
    let content = `<?= str_replace("\n","",$page->getContent()) ?>`;
    let idPage = <?= $page->getId() ?>;
</script>
<script src="/public/js/pageBuilder.js"></script>