<div class="dashboard-section">
	<div class="container">

        <h1>Configuration</h1>

        <div class="col-md-12">
            <div class="tabbed_area">
		        <ul class="tabs">
		            <li><a href="" id="configSiteTab" class="active">Site</a></li>
		            <li><a href="" id="configDatabaseTab">Base de données</a></li>
                <li><a href="" id="configMailTab">Serveur Mail</a></li>
		        </ul>
            </div>

            <div id="configSite">
                <h3>Informations du site</h3>
                <?php $this->addForm( "form", $siteForm ); ?>
                <a class="button" href="<?= cmsProject\core\helpers::getUrl('sitemap', 'sitemap') ?>" name="sitemap">Générer le Sitemap</a>
            </div>

            <div id="configDatabase">
                <h3>Base de données</h3>
                <?php $this->addForm( "form", $databaseForm ); ?>
            </div>

            <div id="configMail">
                <h3>Serveur Mail</h3>
                <?php $this->addForm( "form", $mailForm ); ?>
            </div>
		</div>
  </div>
</div>