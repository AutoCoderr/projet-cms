<div class="dashboard-section">
    <div class="row">
        <div class="col-md-12">
            <h1>Utilisateurs</h1>

            <ul>
                <?php foreach($users as $user): ?>
                    <li>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <h3 class="text-upercase"><?= $user->getEmail() ?></h3>
                                </div>
                                <div class="col-md-6">
                                    <select class="select-role">
                                        <?php foreach($rights as $right): ?>
                                            <option <?= ($right->getId() == $user->getRight_id() ) ? 'selected="selected"' : '' ?> value="<?= $right->getId() ?>"><?= $right->getRole() ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <a class="button" href="javascript:confirmationUser('<?= $user->getId() ?>')"><i class="fas fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>