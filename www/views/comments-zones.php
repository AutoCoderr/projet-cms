<div class="dashboard-section">
    <div class="row">
        <div class="col-md-12">
            <h1>Zones de commentaires<a id="dasboardplus"><i class="fas fa-plus fa-1x button-dasboardplus"></i></a></h1>

            <div id="dashboard-modal" class="modal">
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <h1>Nouvelle zone de commentaire</h1>

                    <?php $this->addModal("form", cmsProject\models\comments_zones::getAddComments_ZoneForm()); ?> 
                </div>
            </div>

            <?php if(!empty($comments_zones)): ?>
                <table class="table-dashboard table-cl-2">
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Date</th>
                        <th>Modifier</th>
                        <th>Supprimer</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($comments_zones as $comments_zone): ?>
                            <tr>
                                <td><?= $comments_zone->getNom() ?></td>
                                <td><?= $comments_zone->getCreated_at() ?></td>
                                <td><a href="<?= cmsProject\core\helpers::getUrl("comment", "getCommentsZoneById"). '?comments_zone_id='. $comments_zone->getId() ?>" class="btn_edit"><i class="fas fa-pen"></i></a></td>
                                <td><a href="javascript:confirmationCommentsZone('<?= $comments_zone->getId() ?>')"><i class="fas fa-trash"></i></a></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>