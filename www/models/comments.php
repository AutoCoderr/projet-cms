<?php
namespace cmsProject\models;

use cmsProject\core\Model;
use cmsProject\core\helpers;
use cmsProject\core\ModelInterface;

use cmsProject\managers\CommentsZoneManager;
use cmsProject\managers\CommentManager;
use cmsProject\managers\UserManager;

class comments extends Model implements ModelInterface {
    protected $id;
    protected $content;
    protected $user_id;
    protected $comments_zone_id;
    protected $created_at;
    protected $updated_at;

    public function initRelation(): array {
        return [
            'users' => users::class
            //'comments_zones' => comments_zones::class
        ];
    }

    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setContent(string $content): self {
        $this->content = strip_tags($content, '<b><strong><i>');
        return $this;
    }

    public function getContent(): string {
        return $this->content;
    }

    // Lie le nouveau commentaire à l'utilisateur
    public function setUser_id($user_id) {
        $this->user_id = $user_id;
    }

    public function getUser_id() {
        return $this->user_id;
    }

    public function getUser(): users {
        return (new UserManager())->find($this->user_id);
    }

    public function setComments_zone_id(int $comments_zone_id): self {
        $this->comments_zone_id = $comments_zone_id;
        return $this;
    }

    public function getComments_zone_id(): int {
        return $this->comments_zone_id;
    }

    // Lie le commentaire à la zone de commentaire
    public function setComments_zone($comments_zone): self {
        $this->comments_zone_id = $comments_zone->getId();
        return $this;
    }

    public function getComments_zone() {
        return (new CommentsZoneManager())->find($this->comments_zone_id);
    }

    public function setCreated_at(): self {
        $this->created_at = date('Y-m-d H:i');
        return $this;
    }

    public function getCreated_at() {
        return $this->created_at;
    }

    public function getUpdated_at() {
        return $this->updated_at;
    }

    public static function getAddCommentForm($comments_zone_id){
        return [
            "config"=>[
                "method"=>"POST",
                "action"=>helpers::getUrl("comment", "add"),
                "class"=>"form-style-login",
                "actionName" => "addComments",
                "id"=>"",
                "submit"=>"Ajouter",
                "fields_col" => 12,
                "errorMsg" => "Votre commentaire n'a pas pu être créer"
            ],
            "fields"=>[
                "content"=>[
                    "type"=>"text",
                    "label"=>"contenu :",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "errorMsg"=>"Le commentaire n'est pas valide"
                ],
                "comments_zone_id"=>[
                    "type"=>"hidden",
                    "class"=>"",
                    "value"=>$comments_zone_id,
                    "id"=>"",
                    "required"=>true
                ]
                
            ]
        ];
    }

}