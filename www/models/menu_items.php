<?php
namespace cmsProject\models;

use cmsProject\core\Model;
use cmsProject\core\ModelInterface;

use cmsProject\managers\MenuManager;
use cmsProject\managers\PageManager;
use cmsProject\models\menus;
use cmsProject\models\pages;

class menu_items extends Model implements ModelInterface {

    protected $id;
    protected $menu_id;
    protected $page_id;
    protected $created_at;

    public function initRelation(): array {
        return [
           'menu' => menus::class,
           'page' => pages::class
        ];
    }

    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setMenu_id(int $menu_id): self {
        $this->menu_id = $menu_id;
        return $this;
    }

    public function getMenu_id(): int {
        return $this->menu_id;
    }

    public function setMenu(menus $menu): self {
        $this->menu_id = $menu->getId();
        return $this;
    }

    public function getMenu(): menus {
        return (new MenuManager)->find($this->menu_id);
    }

    public function setPage_id(int $page_id): self {
        $this->page_id = $page_id;
        return $this;
    }

    public function getPage_id(): int {
        return $this->page_id;
    }

    public function setPage(pages $page): self {
        $this->page_id = $page->getId();
        return $this;
    }

    public function getPage(): pages {
        return (new PageManager)->find($this->page_id);
    }

    public function getCreated_at(): string {
        return $this->created_at;
    }

}