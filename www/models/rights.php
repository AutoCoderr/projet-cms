<?php
namespace cmsProject\models;

use cmsProject\core\Model;

class rights extends Model {
    protected $id;
    protected $role;
    protected $created_at;
    protected $updated_at;

    public function initRelation(): array {
        return [

        ];
    }

    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setRole(string $role): self {
        $this->role = $role;
        return $this;
    }

    public function getRole(): string {
       return $this->role;
    }

    public function getCreated_at(): string {
        return $this->created_at;
    }

    public function getUpdated_at(): string {
        return $this->updated_at;
    }
}