<?php
namespace cmsProject\models;

use cmsProject\core\Model;
use cmsProject\core\helpers;
use cmsProject\core\ModelInterface;

use cmsProject\models\sites;

class oeuvres extends Model implements ModelInterface {
    protected $id;
    protected $nom;
    protected $description;
    protected $url_image;
    protected $artiste;
    protected $dimensions;
    protected $date_realisation;
    protected $created_at;

    public function initRelation(): array {
        return [
           'site' => sites::class
        ];
    }

    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setNom(string $nom): self {
        $this->nom = trim(strip_tags($nom,'<b><strong><i>'));
        return $this;
    }

    public function getNom(): string {
        return $this->nom;
    }

    public function setDescription(string $description): self {
        $this->description = trim(strip_tags($description,'<b><strong><i>'));
        return $this;
    }

    public function getDescription(): string {
        return $this->description;
    }

    public function setUrl_image(?string $url_image): self {
        $this->url_image = str_replace("'", "\'", str_replace('"','\"', $url_image));
        return $this;
    }

    public function getUrl_image(): ?string {
        return $this->url_image;
    }

    public function setArtiste(?string $artiste): self {
        if (!empty($artiste)) {
            $this->artiste = strip_tags($artiste,'<b><strong><i>');
        }
        return $this;
    }

    public function getArtiste(): ?string {
        return $this->artiste;
    }

    public function setDimensions(?string $dimensions): self {
        if (!empty($dimensions)) {
            $this->dimensions = strip_tags($dimensions,'<b><strong><i>');
        }
        return $this;
    }

    public function getDimensions(): ?string {
        return $this->dimensions;
    }

    public function setDate_realisation(?string $date_realisation): self {
        if (!empty($date_realisation)) {
            $this->date_realisation = $date_realisation;
        }
        return $this;
    }

    public function getDate_realisation(): ?string {
        return $this->date_realisation;
    }

    public function setCreated_at(): self {
        $this->created_at = date('Y-m-d H:i');
        return $this;
    }

    public function getCreated_at(): string {
        return $this->created_at;
    }

    public static function getAddOeuvreForm(){
        return [
            "config"=>[
                "model"=>get_called_class(),
                "method"=>"POST",
                "action"=>helpers::getUrl("oeuvre", "add"),
                "class"=>"form-style-login",
                "actionName" => "addOeuvre",
                "id"=>"",
                "submit"=>"Ajouter",
                "fields_col" => 12,
                "errorMsg" => "Votre oeuvre n'a pas pu être enregistrée"
            ],

            "fields"=>[
                "nom"=>[
                    "type"=>"text",
                    "label"=>"Nom",
                    "placeholder"=>"Nom de l'oeuvre",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>2,
                    "maxlength"=>50,
                    "errorMsg"=>"Le format du nom ne correspond pas"
                ],
                "description"=>[
                    "type"=>"text",
                    "label"=>"Description",
                    "placeholder"=>"Description de l'oeuvre",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>2,
                    "maxlength"=>300,
                    "errorMsg"=>"Le format de la description ne correspond pas"
                ],
                "artiste"=>[
                    "type"=>"text",
                    "label"=>"Artiste",
                    "placeholder"=>"Artiste de l'oeuvre",
                    "class"=>"",
                    "id"=>"",
                    "required"=>false,
                    "maxlength"=>100,
                    "errorMsg"=>"Le format de l'artiste ne correspond pas"
                ],
				"url_image"=>[
					"type"=>"text",
					"label"=>"Url de l'image",
					"placeholder"=>"Ex : https://images.com/joconde.png",
					"class"=>"",
					"id"=>"",
					"required"=>false,
					"maxlength"=>500,
					"errorMsg"=>"Le format de l'url de l'image ne correspond pas"
				],
                "dimensions"=>[
                    "type"=>"text",
                    "label"=>"Dimensions",
                    "placeholder"=>"Dimensions de l'oeuvre",
                    "class"=>"",
                    "id"=>"",
                    "required"=>false,
                    "errorMsg"=>"Les dimensions de l'oeuvre ne sont pas valides"
                ],
                "date_realisation"=>[
                    "type"=>"date",
                    "label"=>"Date de réalisation",
                    "placeholder"=>"Date de réalisation de l'oeuvre",
                    "class"=>"",
                    "id"=>"",
                    "required"=>false,
                    "errorMsg"=>"Le format de la date ne correspond pas"
                ]
            ]
        ];
    }

    public static function getSelectOeuvreModal($oeuvres) {
        return [
            "label"=>"Quelle oeuvre voulez vous implémenter?",
            "labelIfNothing"=>"Il n'y a pas d'oeuvre",
            "name"=>"oeuvre",
            "elements"=>$oeuvres
        ];
    }

}
