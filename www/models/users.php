<?php
namespace cmsProject\models;

use cmsProject\core\helpers;
use cmsProject\core\Model;
use cmsProject\core\ModelInterface;
use cmsProject\managers\RightManager;

class users extends Model implements ModelInterface {

    protected $id;
    protected $password;
    protected $firstname;
    protected $lastname;
    protected $email;
    protected $phone;
    protected $right_id;
    protected $created_at;
    protected $updated_at;


    public function initRelation(): array {
        return [
           
        ];
    }

    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setPassword(string $password): self {
        $this->password = trim(strip_tags($password));
        return $this;
    }

    public function getPassword(): string {
        return $this->password;
    }

    public function setFirstname(string $firstname): self {
        $this->firstname = ucwords(strtolower(trim(strip_tags($firstname))));
        return $this;
    }

    public function getFirstname(): string {
        return $this->firstname;
    }

    public function setLastname(string $lastname): self {
        $this->lastname = strtoupper(trim(strip_tags($lastname)));
        return $this;
    }

    public function getLastname(): string {
        return $this->lastname;
    }

    public function setEmail(string $email): self {
        $this->email = strtolower(trim($email));
        return $this;
    }

    public function getEmail(): string {
        return $this->email;
    }

    public function setPhone(?string $phone): self {
        $this->phone = trim(strip_tags($phone));
        return $this;
    }

    public function getPhone(): ?string {
        return $this->phone;
    }

    public function getRight_id(): int {
        return $this->right_id;
    }

    public function setRight_id(int $right_id): self {
        $this->right_id = $right_id;
        return $this;
    }

    public function getCreated_at(): string {
        return $this->created_at;
    }

    public function setCreated_at(string $created_at): self {
        $this->created_at = $created_at;
        return $this;
    }

    public function getUpdated_at(): ?string {
        return $this->updated_at;
    }

    public function setUpdated_at(?string $updated_at): self {
        $this->updated_at = $updated_at;
        return $this;
    }

    public function getRight(): rights {
        return (new RightManager())->find($this->right_id);
    }

    public static function getRegisterForm(){
        return [
            "config"=>[
                "model"=>get_called_class(),
                "method"=>"POST",
                "action"=>helpers::getUrl("user", "register"),
                "class"=>"form-style",
                "actionName" => "register",
                "id"=>"",
                "submit"=>"S'inscrire",
                "fields_col" => 12,
                "errorMsg" => "Inscription échouée"
            ],

            "fields"=>[
                "firstname"=>[
                    "type"=>"text",
                    "label"=>"Prénom",
                    "placeholder"=>"Votre prénom",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>2,
                    "maxlength"=>50,
                    "errorMsg"=>"Le format de votre prénom ne correspond pas"
                ],
                "lastname"=>[
                    "type"=>"text",
                    "label"=>"Nom",
                    "placeholder"=>"Votre nom",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>2,
                    "maxlength"=>50,
                    "errorMsg"=>"Le format de votre nom ne correspond pas"
                ],
                "email"=>[
                    "type"=>"email",
                    "label"=>"Email",
                    "placeholder"=>"Votre adresse email",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "maxlength"=>100,
                    "uniq"=>["table"=>"users","column"=>"email"],
                    "errorMsg"=>"Le format de votre email ne correspond pas"
                ],
                "password"=>[
                    "type"=>"password",
                    "label"=>"Mot de passe",
                    "placeholder"=>"Votre mot de passe",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>8,
                    "errorMsg"=>"Votre mot de passe doit faire entre 8 et 20 caractères avec une minuscule et une majuscule"
                ],
                "passwordConfirm"=>[
                    "type"=>"password",
                    "label"=>"Confirmation mot de passe",
                    "placeholder"=>"Confirmer votre mot de passe",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>8,
                    "confirmWith"=>"password",
                    "errorMsg"=>"Votre mot de passe de confirmation ne correspond pas"
                ]
            ]
        ];
    }

    public static function getLoginForm(){
        return [
            "config"=>[
                "model"=>get_called_class(),
                "method"=>"POST",
                "action"=>helpers::getUrl("user", "login"),
                "class"=>"form-style-login",
                "actionName"=>"login",
                "id"=>"",
                "submit" => "Connexion",
                "fields_col" => 24,
                "errorMsg" => "Connexion échouée"
            ],

            "fields"=>[
                "email"=>[
                    "type"=>"email",
                    "label"=>"Email",
                    "placeholder"=>"Votre email",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>2,
                    "maxlength"=>50,
                    "errorMsg"=>"Le format de votre login ne correspond pas",
                ],
                "password"=>[
                    "type"=>"password",
                    "label"=>"Mot de passe",
                    "placeholder"=>"Votre mot de passe",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "maxlength"=>30,
                    "errorMsg"=>"Votre mot de passe doit faire entre 6 et 20 caractères avec une minuscule et une majuscule",
                ]
            ]
        ];
    }

    public static function getResetPasswordForm() {
        return [
            "config"=>[
                "method"=>"POST",
                "action"=>helpers::getUrl("user", "resetPassword"),
                "class"=>"form-style-login",
                "actionName"=>"resetPassword",
                "id"=>"",
                "submit" => "Réinitialiser le mot de passe",
                "fields_col" => 24,
                "errorMsg" => "Réinitialisation échouée, vérifiez que le mail est bien correct",
                "successMsg" => "Réinitialisation effectuée avec succès"
            ],

            "fields"=>[
                "email"=>[
                    "type"=>"email",
                    "label"=>"Email",
                    "placeholder"=>"Votre email",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>2,
                    "maxlength"=>50,
                    "errorMsg"=>"Le format de votre email ne correspond pas",
                ]
            ]
        ];
    }

    public static function hashPasswd($passwd) {
        for ($i=0;$i<NB_SALT_PASSWD;$i++) {
            $passwd = sha1($passwd.PASSWORD_SALT);
        }
        return $passwd;
    }
}
