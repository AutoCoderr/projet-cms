<?php
namespace cmsProject\models;

use cmsProject\core\Model;
use cmsProject\core\helpers;
use cmsProject\core\ModelInterface;

use cmsProject\models\sites;

class expositions extends Model implements ModelInterface {
    protected $id;
    protected $nom;
    protected $description;
    protected $lieux;
    protected $start_date;
    protected $end_date;
    protected $created_at;
    protected $updated_at;

    public function initRelation(): array {
        return [
           'site' => sites::class
        ];
    }

    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setNom(string $nom): self {
        $this->nom = $nom;
        return $this;
    }

    public function getNom(): string {
        return $this->nom;
    }

    public function setDescription(string $description): self {
        $this->description = trim(strip_tags($description, "<b><strong><i>"));
        return $this;
    }

    public function getDescription(): string {
        return $this->description;
    }

    public function setLieux(string $lieux): self {
        if (!empty($lieux)) {
            $this->lieux = strip_tags($lieux, "<b><strong><i>");
        }
        return $this;
    }

    public function getLieux(): ?string {
        return $this->lieux;
    }

    public function getOeuvres(): ?array  {
        return $this->getFromManyToMany("oeuvres");
    }

    public function addOeuvre($id): ?bool {
        return $this->insertInManyToMany("oeuvres", $id);
    }

    public function removeOeuvre($id = null): ?bool {
        return $this->deleteFromManyToMany("oeuvres", $id);
    }

    public function setStart_date(?string $start_date): self {
        if (!empty($start_date)) {
            $this->start_date = $start_date;
        }
        return $this;
    }

    public function getStart_date(): ?string {
        return $this->start_date;
    }

    public function setEnd_date(?string $end_date): self {
        if (!empty($end_date)) {
            $this->end_date = $end_date;
        }
        return $this;
    }

    public function getEnd_date(): ?string {
        return $this->end_date;
    }

    public function getDate(): ?string {
        if ( !empty($this->start_date) ) {
            if ( !empty($this->end_date) && ($this->start_date != $this->end_date) ) {
                return $this->start_date ." - ". $this->end_date;
            } else {
                return $this->start_date;
            }
        } else {
            return $this->end_date;
        }
    }

    public function setCreated_at(): self {
        $this->created_at = date('Y-m-d H:i');
        return $this;
    }

    public function getCreated_at(): string {
        return $this->created_at;
    }

    public function getUpdated_at(): ?string {
        return $this->updated_at;
    }

    public static function getAddExpositionForm(){
        return [
            "config"=>[
                "model"=>get_called_class(),
                "method"=>"POST",
                "action"=>helpers::getUrl("exposition", "add"),
                "class"=>"form-style-login",
                "actionName" => "addExposition",
                "id"=>"",
                "submit"=>"Ajouter",
                "fields_col" => 12,
                "errorMsg" => "Votre exposition n'a pas pu être enregistrée"
            ],

            "fields"=>[
                "nom"=>[
                    "type"=>"text",
                    "label"=>"Nom",
                    "placeholder"=>"Nom de l'exposition",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>2,
                    "maxlength"=>50,
                    "errorMsg"=>"Le format du nom ne correspond pas"
                ],
                "description"=>[
                    "type"=>"text",
                    "label"=>"Description",
                    "placeholder"=>"Description de l'exposition",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>2,
                    "maxlength"=>300,
                    "errorMsg"=>"Le format de la description ne correspond pas"
                ],
                "lieux"=>[
                    "type"=>"text",
                    "label"=>"Lieu de l'exposition",
                    "placeholder"=>"ex: Paris",
                    "class"=>"",
                    "id"=>"",
                    "required"=>false,
                    "maxlength"=>100,
                    "errorMsg"=>"Le format du lieu ne correspond pas"
                ],
                "start_date"=>[
                    "type"=>"datetime-local",
                    "label"=>"Début de l'exposition",
                    "placeholder"=>"",
                    "class"=>"",
                    "id"=>"",
                    "required"=>false,
                    "errorMsg"=>"Le format de la date de début ne correspond pas"
                ],
                "end_date"=>[
                    "type"=>"datetime-local",
                    "label"=>"Fin de l'exposition",
                    "placeholder"=>"",
                    "class"=>"",
                    "id"=>"",
                    "required"=>false,
                    "errorMsg"=>"Le format de la date de fin ne correspond pas"
                ],
                "oeuvres"=>[
                    "type"=>"listCanAddOrDelete",
                    "label"=>"Les oeuvres :",
                    "class"=>"",
                    "id"=>"",
                    "required"=>false,
                    "errorMsg"=>"Les oeuvres sont mal renseignées"
                ]
            ]
        ];
    }

    public static function getSelectExpositionModal($expositions) {
        return [
            "label"=>"Quelle exposition voulez vous implémenter?",
            "labelIfNothing"=>"Il n'y a pas d'exposition",
            "name"=>"exposition",
            "elements"=>$expositions
        ];
    }
}
