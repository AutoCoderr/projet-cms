<?php
namespace cmsProject\models;

use cmsProject\core\Model;
use cmsProject\core\helpers;
use cmsProject\core\ModelInterface;

use cmsProject\core\QueryBuilder;

use cmsProject\managers\CommentsZoneManager;
use cmsProject\managers\CommentManager;

class comments_zones extends Model implements ModelInterface {
    protected $id;
    protected $nom;
    protected $created_at;
    protected $updated_at;

    public function initRelation(): array {
        return [
            'page' => pages::class
        ];
    }

    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setNom(string $nom): self {
        $this->nom = trim(strip_tags($nom, '<b>strong<i>'));
        return $this;
    }

    public function getNom(): string {
        return $this->nom;
    }

    public function setCreated_at(): self {
        $this->created_at = date('Y-m-d H:i');
        return $this;
    }

    public function getCreated_at() {
        return $this->created_at;
    }

    public function setUpdated_at(): self {
        $this->created_at = date('Y-m-d H:i');
        return $this;
    }

    public function getUpdated_at() {
        return $this->updated_at;
    }

    public static function getAddComments_ZoneForm(){
        return [
            "config"=>[
                "method"=>"POST",
                "action"=>helpers::getUrl("comment", "addZone"),
                "class"=>"form-style-login",
                "actionName" => "addZone",
                "id"=>"",
                "submit"=>"Ajouter",
                "fields_col" => 12,
                "errorMsg" => "Votre zone de commentaire n'a pas pu être créer"
            ],
            "fields"=>[
                "nom"=>[
                    "type"=>"text",
                    "label"=>"Nom",
                    "placeholder"=>"Nom de la zone de commentaire",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>2,
                    "maxlength"=>50,
                    "errorMsg"=>"Le format du nom ne correspond pas"
                ],
            ]
        ];
    }

    public static function getSelectCommentsZonesModal($commentsZones) {
        return [
            "label"=>"Quelle zone de commentaire voulez vous implémenter?",
            "labelIfNothing"=>"Il n'y a pas de zone de commentaire",
            "name"=>"comments_zone",
            "elements"=>$commentsZones
        ];
    }

}