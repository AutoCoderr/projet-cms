<?php
namespace cmsProject\models;

use cmsProject\core\helpers;
use cmsProject\core\Model;
use cmsProject\core\ModelInterface;

use cmsProject\managers\SiteManager;
use cmsProject\models\sites;

class pages extends Model implements ModelInterface {
    protected $id;
    protected $nom;
    protected $uri;
    protected $content;
    protected $created_at;
    protected $updated_at;

    public function initRelation(): array {
        return [
           'site_id' => sites::class
        ];
    }

    public function setId(int $id): self {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setNom(string $nom): self {
        $this->nom = trim(strip_tags($nom, '<b><strong><i>'));
        return $this;
    }

    public function getNom(): string {
        return $this->nom;
    }

    public function setUri(string $uri): self {
        $this->uri = strip_tags($uri);
        $this->uri = ($this->uri[0]!="/") ? "/".$this->uri:$this->uri;
        return $this;
    }

    public function getUri(): string {
        return $this->uri;
    }

    public function getMenus() {
        return $this->getFromManyToMany("menus", ['otherColumns' => ['rank'], 'orderBy' => 'rank']);
    }

    public function addMenu($id_menu, $rank) {
        return $this->insertInManyToMany("menus", $id_menu, ["otherToAdd" => ["rank" => $rank]]);
    }

    public function removeMenu($idMenu) {
        return $this->deleteFromManyToMany("menus", $idMenu);
    }

    public function updateMenu($idMenu, $columnsToUpdate) {
        return $this->updateManyToMany("menus", $idMenu, $columnsToUpdate);
    }

    public function setContent(string $content): self {
        $this->content = $content;
        return $this;
    }

    public function getContent(): string {
        return $this->content;
    }

    public function setCreated_at(): self {
        $this->created_at = date('Y-m-d H:i');
        return $this;
    }

    public function getCreated_at(): string {
        return $this->created_at;
    }

    public function setUpdated_at(): self {
        $this->created_at = date('Y-m-d H:i');
        return $this;
    }

    public function getUpdated_at(): ?string {
        return $this->updated_at;
    }

    public static function getAddPagesForm(){
        return [
            "config"=>[
                "model"=>get_called_class(),
                "method"=>"POST",
                "action"=>helpers::getUrl("page", "add"),
                "class"=>"form-style-login",
                "actionName" => "addPage",
                "id"=>"",
                "submit"=>"Ajouter",
                "fields_col" => 12,
                "errorMsg" => "Votre page n'a pas pu être créer"
            ],

            "fields"=>[
                "nom"=>[
                    "type"=>"text",
                    "label"=>"Nom",
                    "placeholder"=>"Nom de la page",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>2,
                    "maxlength"=>50,
                    "errorMsg"=>"Le format du nom ne correspond pas"
                ],
                "uri"=>[
                    "type"=>"text",
                    "label"=>"Uri",
                    "placeholder"=>"Uri de la page",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>2,
                    "maxlength"=>300,
                    "errorMsg"=>"L'uri est indisponible ou est incorrect'",
                    "uniq"=>["table"=>"pages","column"=>"uri"],
                    "notIn"=>"routes"
                ]
            ]
        ];
    }

    public static function getAddMenuToPageForm($menus = [], $idPage = null) {
        return [
            "config"=>[
                "model"=>get_called_class(),
                "method"=>"POST",
                "action"=>helpers::getUrl("menu", "addToPage"),
                "class"=>"form-style-login",
                "actionName" => "addMenuToPage",
                "id"=>"",
                "submit"=>"Ajouter",
                "fields_col" => 12,
                "errorMsg" => "Ce menu n'a pas pu être ajouté",
                "successMsg" => "Menu ajouté avec succès!"
            ],

            "fields"=>[
                "menu"=>[
                    "type"=>"select",
                    "label"=>"Les menus",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "errorMsg"=>"Veuillez spécifier une bonne catégorie",
                    "options"=>$menus
                ],
                "page" => [
                    "type"=>"hidden",
                    "value"=>$idPage,
                    "required"=>true,
                ]
            ]
        ];
    }
}
