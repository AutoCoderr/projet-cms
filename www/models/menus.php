<?php
namespace cmsProject\models;

use cmsProject\core\helpers;
use cmsProject\core\Model;
use cmsProject\core\ModelInterface;

use cmsProject\core\QueryBuilder;
use cmsProject\managers\ItemMenuManager;
use cmsProject\managers\MenuManager;
use cmsProject\managers\SiteManager;
use cmsProject\models\sites;

class menus extends Model implements ModelInterface {

    protected $id;
    protected $nom;
    protected $menu_id;
    protected $created_at;
    protected $updated_at;
    protected $rank;

    public function initRelation(): array {
        return [
           'site' => sites::class,
           'parent' => menus::class
        ];
    }

    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setNom(string $nom): self {
        $this->nom = trim(strip_tags($nom,'<b><strong><i>'));
        return $this;
    }

    public function getNom(): string {
        return $this->nom;
    }

    public function setMenu_id(?int $menu_id): self {

        if ( !is_null($menu_id) )
            $this->menu_id = $menu_id;

        return $this;
    }

    public function getMenu_id(): ?int {
        return $this->menu_id;
    }

    public function addItem($idPage) {
        $item = new menu_items();
        $item->setMenu_id($this->id);
        $item->setPage_id($idPage);
        $itemManager = new ItemMenuManager();
        $itemManager->save($item);
        return true;
    }

    public function removeItem($idPage) {
        $queryBuilder = new QueryBuilder();
        $query = $queryBuilder->select("M.id")
            ->from("menu_items", "M")
            ->where("M.menu_id = :menu_id AND M.page_id = :page_id")
            ->addToQuery("LIMIT 1")
            ->setParameter("menu_id", $this->id)
            ->setParameter("page_id", $idPage)
            ->getQuery()
            ->getArrayResult();
        $item = (new ItemMenuManager())->delete($query[0]["id"]);

        return true;
    }

    public function getItems() {
        return (new ItemMenuManager)->findByMenuId($this->id);
    }

    public function getPagesNames() {
        $names = [];
        $items = $this->getItems();
        foreach ($items as $item) {
            $names[] = $item->getPage()->getUri();
        }
        return $names;
    }


    public function setMenu(?menus $menu): self {

        if ( !is_null($menu) )
            $this->menu_id = $menu->getId();

        return $this;
    }

    public function getMenu(): ?menus {
        return (new MenuManager)->find($this->menu_id);
    }

    public function deleteItems() {
        $queryBuilder = new QueryBuilder();
        $items = $queryBuilder->select()->from("menu_items", "M")
            ->where("menu_id = :menu_id")->setParameter("menu_id", $this->id)
            ->getQuery()->getArrayResult(menu_items::class);
        foreach ($items as $item) {
            (new ItemMenuManager())->delete($item->getId());
        }
    }

    public function deletePages($idPage = null) {
        $this->deleteFromManyToMany("pages", $idPage);
    }

    public function setCreated_at(): self {
        $this->created_at = date('Y-m-d H:i');
        return $this;
    }

    public function getCreated_at(): string {
        return $this->created_at;
    }

    public function setUpdated_at(): self {
        $this->created_at = date('Y-m-d H:i');
        return $this;
    }

    public function getUpdated_at(): ?string {
        return $this->updated_at;
    }

    public function setRank($rank) {
        $this->rank = $rank;
    }

    public function getRank() {
        return $this->rank;
    }

    public static function getAddMenuForm(){
        return [
            "config"=>[
                "model"=>get_called_class(),
                "method"=>"POST",
                "action"=>helpers::getUrl("menu", "add"),
                "class"=>"form-style-login",
                "actionName" => "addMenu",
                "id"=>"",
                "submit"=>"Ajouter",
                "fields_col" => 12,
                "errorMsg" => "Votre menu n'a pas pu être créer"
            ],

            "fields"=>[
                "nom"=>[
                    "type"=>"text",
                    "label"=>"Nom",
                    "placeholder"=>"Nom du menu",
                    "class"=>"",
                    "id"=>"",
                    "required"=>true,
                    "minlength"=>2,
                    "maxlength"=>50,
                    "errorMsg"=>"Le format du nom ne correspond pas"
                ],
                "pages"=>[
                    "type"=>"listCanAddOrDelete",
                    "label"=>"Les pages :",
                    "class"=>"",
                    "id"=>"",
                    "required"=>false,
                    "errorMsg"=>"Les pages sont mal renseignées"
                ]
            ]
        ];
    }
}
