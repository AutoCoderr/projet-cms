<?php
namespace cmsProject\controllers;

use cmsProject\core\View;
use cmsProject\core\MiddlewareHandler;
use cmsProject\core\Controller;
use cmsProject\core\File;
use cmsProject\managers\PageManager;
use cmsProject\managers\MenuManager;
use cmsProject\managers\OeuvreManager;
use cmsProject\managers\ExpositionManager;
use cmsProject\managers\CommentsZoneManager;
use cmsProject\managers\UserManager;

use cmsProject\forms\ConfigSiteType;
use cmsProject\forms\ConfigDatabaseType;
use cmsProject\forms\ConfigMailType;

class DashboardController extends Controller {

	public function __construct() {
		MiddlewareHandler::launch('checkRights', ["administrateur", "mainteneur"]); // accès aux admins et mainteneur
	}

	public function overviewAction() {

        $this->render("overview", "back");
    }

    public function graphAction() {

        MiddlewareHandler::launch('handleToken'); // vérifie le token
        
        $data = [
            [
                'label' => 'Nb pages',
                'value' => count( (new PageManager())->findAll() ),
            ],
            [
                'label' => 'Nb menus',
                'value' => count( (new MenuManager())->findAll() ),
            ],
            [
                'label' => 'Nb oeuvres',
                'value' => count( (new OeuvreManager())->findAll() ),
            ],
            [
                'label' => 'Nb expositions',
                'value' => count( (new ExpositionManager())->findAll() ),
            ]
        ];

        header("Content-type: application/json; charset=utf-8");
        echo json_encode(['data' => $data]); // renvoie les informations du dashboard sous format json

    }

    public function optionsAction() {
        
        MiddlewareHandler::launch('checkRights', ["administrateur"]); // accès seulement aux admins

        // Configuration Site Form
        $configSiteForm = $this->createForm(ConfigSiteType::class); // initialise le form

        $configSiteForm->handle(); // traitement

        if($configSiteForm->isSubmit() && $configSiteForm->isValid())
        {  

            if ( (new File('.env'))->replace('SITE_NAME='. SITE_NAME, 'SITE_NAME='. strip_tags($_POST['name'])) ) {
                $this->redirectTo('dashboard', 'options'); // remplace la variable SITE_NAME dans le .env
            } else {
                $configSiteForm->setError('configSite', 'Impossible de changer le nom du site');
            }
        }

        // Display Database Form
        $configDatabaseForm = $this->createForm(ConfigDatabaseType::class);

        // Configuration Mail Form
        $configMailForm = $this->createForm(ConfigMailType::class); // initialise le form

        $configMailForm->handle(); // traitement

        if($configMailForm->isSubmit() && $configMailForm->isValid())
        {  

            foreach($_POST as $key => $value) {

                $var = 'MAIL_'. strtoupper($key);

                //remplace les variables dans le .env
                if ( (new File('.env'))->replace( $var .'='. constant($var), $var .'='. strip_tags($value))) {
                    $this->redirectTo('dashboard', 'options');
                } else {
                    $configMailForm->setError('configMail', 'Impossible de changer la variable '. $var);
                }
            }
        }

        $this->render("options", "back", [
            "siteForm" => $configSiteForm,
            "databaseForm" => $configDatabaseForm,
            "mailForm" => $configMailForm
        ]);
    }
    
    /**
     *  Affiche les zones de commentaires
     */
    public function commentsZonesAction(){
        $view = new View("comments-zones");

        $commentsZoneManager = new CommentsZoneManager();
        $commentsZones = $commentsZoneManager->findAll();
        
        $view->assign("comments_zones", $commentsZones);

    }
}