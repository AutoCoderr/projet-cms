<?php
namespace cmsProject\controllers;

use cmsProject\core\MiddlewareHandler;
use cmsProject\core\View;

use cmsProject\managers\UserManager;
use cmsProject\managers\RightManager;


class RightController {

    public function __construct() {
		MiddlewareHandler::launch('checkRights', ["administrateur"]); // accès seulement aux admins
	}

    /**
     * Gestion des droits
     */

    /**
     * Assigne un droit à un utilisateur
     */
    public function usersAction(){

        $view = new View("users");

        $users = ( new UserManager() )->findAllUserWithoutMe();
        $rights = ( new RightManager() )->findAll();
        
        $view->assign("users", $users);
        $view->assign("rights", $rights);

    }

    /**
     * Met à jour le droit d'une utilisateur
     */
    public function updateRightAction() {

        MiddlewareHandler::launch('handleToken'); // vérifie le token
        
        $userManager = new UserManager();

        $users = $userManager->findBy([ 'email' => $_POST['email'] ]);

        if(!empty($users)) {

            $currentUser = $users[0];

            if($currentUser->getid() != $_SESSION["user_id"]){

                $currentUser->setRight_id($_POST['right_id']);

                $userManager->save($currentUser);
            }
        }


    }

}