<?php
namespace cmsProject\controllers;

use cmsProject\core\MiddlewareHandler;
use cmsProject\core\View;
use cmsProject\core\helpers;

use cmsProject\managers\PageManager;

use cmsProject\models\pages;

class SitemapController {
    
    /**
     * Genere un sitemap ou le met à jour
     */
    public function sitemapAction() {

    if (empty($_SESSION['user_id'])) {
        header("Location: /");
    } else {
        
        $pageManager = new PageManager();
        $pages = $pageManager->findPageWhereRouteIsNot('/');
        
        // Le nom de domaine du site
        $domaine = $_SERVER['HTTP_HOST'];

        // format de la date
        $date = date('Y-m-d H:i', time());
        $handle = fopen("./sitemap.xml", "w");
        // Ecrit dans le fichier ./sitemap.xml
        fwrite($handle,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
            <urlset
                xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"
                xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
                xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9
                http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">
                                
                <url>
                    <loc>$domaine/</loc>
                    <lastmod>".$date."</lastmod>
                    <priority>1.00</priority>
                </url>
                <url>
                    <loc>$domaine/login</loc>
                    <lastmod>".$date."</lastmod>
                    <priority>0.80</priority>
                </url>
                <url>
                    <loc>$domaine/register</loc>
                    <lastmod>".$date."</lastmod>
                    <priority>0.80</priority>
                </url>
                <url>
                    <loc>$domaine/forgotpassword</loc>
                    <lastmod>".$date."</lastmod>
                    <priority>0.75</priority>
                </url>"
                
            );
            // Parcours les pages pour les afficher dans le sitemap une à une
            foreach ($pages as $page){
                fwrite($handle, "
                <url>
                    <loc>$domaine".$page->getUri()."</loc>
                    <lastmod>".$page->getCreated_at()."</lastmod>
                    <priority>0.70</priority>
                </url>
            ");
            }

            fwrite($handle, "</urlset>");
        fclose($handle);
        $view = new View("sitemap", "back");
        $view->assign("pages",$pages);

        }
    }
}