<?php
namespace cmsProject\controllers;

use cmsProject\core\Controller;
use cmsProject\core\helpers;
use cmsProject\core\Validator;
use cmsProject\core\View;
use cmsProject\Core\Exceptions\ExceptionHandler;

use cmsProject\models\users;

use cmsProject\services\AccountService;

use cmsProject\managers\UserManager;

use cmsProject\forms\LoginType;
use cmsProject\forms\RegisterType;
use cmsProject\forms\ForgotPwdType;
use cmsProject\forms\ProfileType;
use cmsProject\forms\ChangePwdType;

class UserController extends Controller
{

    /**
     * Connexion de l'utilisateur
     */
    public function loginAction() {

        $loginForm = $this->createForm(LoginType::class); // initialisation du form

        $loginForm->handle(); // traitement

        if($loginForm->isSubmit() && $loginForm->isValid())
        {  
            $accountService = AccountService::getInstance();
            $authenticated  = $accountService->authenticate($_POST['email'], $_POST['password']);

            if ($authenticated) 
            {
                $this->redirectTo("dashboard", "overview");
            } 
            else 
            {
                $loginForm->setError('login', 'Connexion échouée');
            }
        }

        $this->render("connexion", "front", [
            "loginForm" => $loginForm
        ]);
    }

    /**
     * Inscription de l'utilisateur
     */
    public function registerAction() {

        $registerForm = $this->createForm(RegisterType::class); // initialisation du form

        $registerForm->handle(); // traitement

        if($registerForm->isSubmit() && $registerForm->isValid())
        {  
            
            $accountService = AccountService::getInstance();
            $added          = $accountService->addUser($_POST);

            if ($added)
            {
                $accountService->authenticate($_POST['email'], $_POST['password']);
                $this->redirectTo('/');
            } else {
                $registerForm->setError('register', 'Inscription échouée');
            }
        }

        $this->render("inscription", "front", [
            "registerForm" => $registerForm
        ]);
    }

    /**
     * Deconnexion de l'utilisateur
     */
    public function logoutAction() {

        $accountService = AccountService::getInstance();

        if ($accountService->logout()) {
            header("Location: /");
        }

    }

    /**
     * Affichage des utilisateurs
     */
    public function getAction() {
        $userManager = new UserManager();

        $user = $userManager->find($params['id']);


        if(!$user) {
            new ExceptionHandler("User not found");
        }
        
        //$users = $userManager->findAll();

        //$partialUsers = $userManager->findBy(['firstname' => "Arnaud%"], ['id' => 'desc']);

        $userManager->delete(5);

        echo "get user";

    }

    /**
     * Supression d'un utilisateur
     */
    public function deleteAction() {
        $accountService = AccountService::getInstance();
        $deletedUser = $accountService->deleteUser();
        if($deletedUser){
            if($_SESSION['user_id'] == $_GET['user_id']){
                header("Location: /");
            }else{
                header("Location: ". $_SERVER['HTTP_REFERER']);
            }
            
        }

            
    }

    /**
     * Mot de passe oublié avec envoi de mail
     */
    public function forgotpasswordAction() {
        
        $forgotPwdForm = $this->createForm(ForgotPwdType::class); // initialisation du form

        $forgotPwdForm->handle(); // traitement

        if($forgotPwdForm->isSubmit() && $forgotPwdForm->isValid())
        {  
        
            $accountService = AccountService::getInstance();
            $reseted        = $accountService->resetPassword($_POST['email']);

            if ($reseted) 
            {
                $this->redirectTo("/");
            } else {
                $forgotPwdForm->setError('resetPassword', 'Réinitialisation échouée, vérifiez que le mail est bien correct');
            }
        }

        $this->render("forgotpassword", "front", [
            "forgotPwdForm" => $forgotPwdForm
        ]);
    }

    /**
     * Affiche la page de profile
     */
    public function profileAction() {

        $userManager = new UserManager();

        $user = $userManager->find($_SESSION['user_id']);

        $profileForm = $this->createForm(ProfileType::class, $user); // initialisation du form

        $profileForm->handle(); // traitement

        
        $changePwdForm = $this->createForm(ChangePwdType::class); // initialisation du form

        $changePwdForm->handle(); // traitement

        if($changePwdForm->isSubmit() && $changePwdForm->isValid())
        {  
            $user->setPassword(users::hashPasswd($_POST['newPassword']));
            $userManager->save($user);

            $this->redirectTo('user', 'profile');
        }

        $this->render("profile", "front", [
            "profileForm" => $profileForm,
            "changePwdForm" => $changePwdForm
        ]);
    }
}
