<?php

namespace cmsProject\controllers;

use cmsProject\core\MiddlewareHandler;
use cmsProject\core\Controller;

use cmsProject\services\TokenService;

class TokenController extends Controller
{

    public function __construct(){
		MiddlewareHandler::launch('checkRights', ["administrateur", "mainteneur"]); // accès aux admins et mainteneur
    }

    /*
    * Génère un token et le renvoie sous la forme d'un json
    */
    public function generateAction(){

        $tokenService = new TokenService();

        $token = $tokenService->setUser_id($_SESSION['user_id'])
                              ->setSecret_key(AJAX_TOKEN_ENCODER)
                              ->generate();

        echo json_encode(["token" => $token]);
    }
}
