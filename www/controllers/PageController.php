<?php
namespace cmsProject\controllers;

use cmsProject\core\Validator;
use cmsProject\core\View;
use cmsProject\core\MiddlewareHandler;
use cmsProject\core\Exceptions\UnauthorizedException;

use cmsProject\managers\PageManager;

use cmsProject\models\pages;
use cmsProject\services\PageService;

class PageController {

    public function __construct() {
		MiddlewareHandler::launch('checkRights', ["administrateur", "mainteneur"]); // accès aux admins et mainteneur
	}

    /**
     * Permet de gérer les pages
     */
    public function pagesAction(){

        $view = new View("pages");

        $pageManager = new PageManager();

        $pages = $pageManager->findAll();

        $view->assign("pages", $pages);

    }

    /**
     * Ajout d'une page
     */
    public function addAction() {

        $form = pages::getAddPagesForm();
        $errors[$form["config"]["actionName"]] = Validator::checkForm($form,$_POST);

        $pageManager = new PageManager();

        if (count($errors[$form["config"]["actionName"]]) == 0) {

            $page = new pages();
            $page->setNom($_POST['nom']);
            $page->setUri($_POST['uri']);
            $page->setContent("<p></p>");
            $page->setCreated_at();
            $page->setUpdated_at();

            $pageManager->save($page);

            $_POST = array();
        }

        $view = new View("pages");

        $pages = $pageManager->findAll();

        $view->assign("pages", $pages);
        $view->assign("errors", $errors);

    }

    /**
     * Modifier une le nom ou l'url page
     */
    public function updateAction() {

        $pageManager = new PageManager();

        $page = $pageManager->find($_GET['page_id']);
        $form = pages::getAddPagesForm();
        $errors[$form["config"]["actionName"]] = Validator::checkForm($form,$_POST);

        if (count($errors[$form["config"]["actionName"]]) == 0) {
            if ($page->getUri() != "/") {
                $page->setNom($_POST['nom']);
                $page->setUri($_POST['uri']);
                $page->setUpdated_at();

                $_POST = array();

                $pageManager->save($page);
            }
        }

        $view = new View("pages");

        $pages = $pageManager->findAll();

        $view->assign("pages", $pages);
        $view->assign("errors", $errors);

    }

    /**
     * Supprimer une page
     */
    public function deleteAction() {

        if (empty($_GET['page_id'])) {
            new UnauthorizedException("Invalid Parameter", 401);
        }

        $pageManager = new PageManager();
        $page = $pageManager->find($_GET['page_id']);
        if ($page->getUri() != "/") {
            $pageManager->delete($_GET['page_id']);
        }

        $this->pagesAction();

    }

    /**
     * modifier une page
     */
	public function editAction() {

        if (empty($_GET['id'])) {
            new UnauthorizedException("Id non renseigné");
        }

	    if (empty($_SESSION['user_id'])) {
	        new UnauthorizedException("Vous n'êtes pas connecté");
        }

	    $id = $_GET['id'];

	    $pageService = PageService::getInstance();
	    $infos = $pageService->getInfoPage($id);

	    $view = new View("editpage", "edit");
	    $view->assign("page", $infos["page"]);
        $view->assign("menus", $infos["menus"]);
        $view->assign("otherMenus", $infos["otherMenus"]);
        $view->assign("oeuvres", $infos["oeuvres"]);
        $view->assign("expositions", $infos["expositions"]);
        $view->assign("comments_zones", $infos["comments_zones"]);
    }

    /**
     * Enrgistre les modifications d'une page
     */
    public function saveAction() {

	    if (empty($_POST['content']) || empty($_POST['id'])) {
            echo json_encode(["rep" => "failed", "msg" => "Il manque des informations"]);
            return;
        }

	    if (!isset($_SESSION['user_id'])) {
            echo json_encode(["rep" => "failed", "msg" => "Vous n'êtes pas connecté"]);
            return;
        }

	    $pageManager = new PageManager();

        $page =  $pageManager->find($_POST['id']);

        if ($page->getNom() == null) {
            echo json_encode(["rep" => "failed", "msg" => "Cette page n'existe pas ou vous ne pouvez pas l'éditer"]);
            return;
        }

        $page->setContent($_POST['content']);
        $pageManager->save($page);

        echo json_encode(["rep" => "success"]);
    }

    /**
     * Affiche les pages en fonction de leur id
     */
    public function getPageByIdAction() {

        MiddlewareHandler::launch('handleToken'); // vérifie le token

        $page = (new PageManager())->find($_GET["page_id"]);

        echo json_encode($page->jsonSerialize()); // revoie les données de la page sous format json

    }
}
