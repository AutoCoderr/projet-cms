<?php
namespace cmsProject\controllers;

use cmsProject\core\helpers;
use cmsProject\core\MiddlewareHandler;
use cmsProject\core\View;
use cmsProject\core\Exceptions\UnauthorizedException;

use cmsProject\managers\OeuvreManager;

use cmsProject\models\oeuvres;

class OeuvreController {

	public function __construct() {
		MiddlewareHandler::launch('checkRights', ["administrateur", "mainteneur"]); // accès aux admins et mainteneur
	}

	/*
     * Affichage des oeuvre
     */
	public function oeuvresAction(){
        $view = new View("oeuvres");

        $oeuvreManager = new OeuvreManager();
        $oeuvres = $oeuvreManager->findAll();

        $view->assign("oeuvres", $oeuvres);
    }

	/*
     * Ajout d'une oeuvre
     */
	public function addAction() {

		$oeuvreManager = new OeuvreManager();

		$oeuvre = new oeuvres();
		$oeuvre->setNom($_POST['nom']);
		$oeuvre->setDescription($_POST['description']);
		$oeuvre->setArtiste($_POST['artiste']);
		$oeuvre->setDimensions($_POST['dimensions']);
		$oeuvre->setUrl_image($_POST['url_image']);
		$oeuvre->setDate_realisation($_POST['date_realisation']);
		$oeuvre->setCreated_at();

		$oeuvreManager->save($oeuvre);

        $_POST = array();

		$this->oeuvresAction();
	}

	/*
     * Modifier une oeuvre
     */
	public function updateAction() {

		$oeuvreManager = new OeuvreManager();

		$oeuvre = $oeuvreManager->find($_GET['oeuvre_id']);
		$oeuvre->setNom($_POST['nom']);
		$oeuvre->setDescription($_POST['description']);
		$oeuvre->setArtiste($_POST['artiste']);
		$oeuvre->setDimensions($_POST['dimensions']);
		$oeuvre->setUrl_image($_POST['url_image']);
		$oeuvre->setDate_realisation($_POST['date_realisation']);

		$oeuvreManager->save($oeuvre);

        $_POST = array();

		$this->oeuvresAction();
	}

	/*
     * Supprimer une oeuvre
     */
	public function deleteAction() {

		if (empty($_GET['oeuvre_id'])) {
			new UnauthorizedException("Invalid Parameter", 401);
		}

		$oeuvreManager = new OeuvreManager();
		$oeuvreManager->delete($_GET['oeuvre_id']);

		$this->oeuvresAction();
	}

	/*
     * Affichage des oeuvres par leurs id
     */
	public function getOeuvreByIdAction() {

		MiddlewareHandler::launch('handleToken'); // vérifie le token

        $oeuvre = (new OeuvreManager())->find($_GET["oeuvre_id"]);

        echo json_encode($oeuvre->jsonSerialize()); // renvoie les informations de l'oeuvre sous format json

    }

}
