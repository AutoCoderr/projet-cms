<?php
namespace cmsProject\controllers;

use cmsProject\core\Validator;
use cmsProject\managers\ExpositionManager;

use cmsProject\core\helpers;
use cmsProject\core\MiddlewareHandler;
use cmsProject\core\View;
use cmsProject\core\QueryBuilder;
use cmsProject\core\Exceptions\ExceptionHandler;

use cmsProject\models\expositions;
use cmsProject\models\oeuvres;

class ExpositionController {

	public function __construct() {
		MiddlewareHandler::launch('checkRights', ["administrateur", "mainteneur"]); // accès aux admins et mainteneur
	}

	public function expositionsAction(){
        $view = new View("expositions");

        $expositionManager = new ExpositionManager();
        $expositions = $expositionManager->findAll();
        
        $view->assign("expositions", $expositions);

    }

    /**
     * ajoute une exposition
     */
	public function addAction() {

        $form = expositions::getAddExpositionForm();
        $errors[$form["config"]["actionName"]] = Validator::checkForm($form,$_POST);

        $expositionManager = new ExpositionManager();

        if (count($errors[$form["config"]["actionName"]]) == 0) {

            $exposition = new expositions();
            $exposition->setNom($_POST['nom']);
            $exposition->setDescription($_POST['description']);
            $exposition->setLieux($_POST['lieux']);
            $exposition->setStart_date($_POST['start_date']);
            $exposition->setEnd_date($_POST['end_date']);
            $exposition->setCreated_at();

            $expositionManager->save($exposition);

            $_POST = array();
        }

        $view = new View("expositions");
        $expositions = $expositionManager->findAll();

        $view->assign("expositions", $expositions);
        $view->assign("errors", $errors);

	}

    /**
     * modifie une exposition
     */
	public function updateAction() {

        $form = expositions::getAddExpositionForm();
        $errors[$form["config"]["actionName"]] = Validator::checkForm($form,$_POST);

        $expositionManager = new ExpositionManager();

        if (count($errors[$form["config"]["actionName"]]) == 0) {
            $exposition = $expositionManager->find($_GET['exposition_id']);
            $exposition->setNom($_POST['nom']);
            $exposition->setDescription($_POST['description']);
            $exposition->setLieux($_POST['lieux']);
            $exposition->setStart_date($_POST['start_date']);
            $exposition->setEnd_date($_POST['end_date']);

            $expositionManager->save($exposition);

            $oeuvresIdsInList = $_POST['oeuvres']??[];
            $oeuvresInBdd = $exposition->getOeuvres();
            $oeuvresIdsInBdd = [];
            foreach ($oeuvresInBdd as $oeuvre) {
                $oeuvresIdsInBdd[] = $oeuvre->getId();
            }
            foreach ($oeuvresIdsInList as $oeuvreIdInList) {
                $found = false;
                foreach ($oeuvresIdsInBdd as $oeuvreIdInBdd) {
                    if ($oeuvreIdInBdd == $oeuvreIdInList) {
                        $found = true;
                        break;
                    }
                }
                if (!$found) {
                    $exposition->addOeuvre($oeuvreIdInList);
                }
            }
            foreach ($oeuvresIdsInBdd as $oeuvreIdInBdd) {
                $found = false;
                foreach ($oeuvresIdsInList as $oeuvreIdInList) {
                    if ($oeuvreIdInList == $oeuvreIdInBdd) {
                        $found = true;
                        break;
                    }
                }
                if (!$found) {
                    $exposition->removeOeuvre($oeuvreIdInBdd);
                }
            }

            $_POST = array();
        }

        $view = new View("expositions");
        $expositions = $expositionManager->findAll();

        $view->assign("expositions", $expositions);
        $view->assign("errors", $errors);
	}

    /*
     * supprime une exposition
     */
	public function deleteAction() {
		
		if (empty($_GET['exposition_id'])) {
			new UnauthorizedException("Invalid Parameter", 401);	
		}

		$expositionManager = new ExpositionManager();
		$exposition = $expositionManager->find($_GET['exposition_id']);
        $exposition->removeOeuvre();
		$expositionManager->delete($_GET['exposition_id']);

		$this->expositionsAction();
		
	}

    /*
     * Affiche une exposition par son id
     */
	public function getExpositionByIdAction() {

        MiddlewareHandler::launch('handleToken'); // vérifie le token
        
        $exposition = (new ExpositionManager())->find($_GET["exposition_id"]);
        $oeuvresModels = $exposition->getOeuvres();
        $oeuvres = [];
        foreach ($oeuvresModels as $oeuvre) {
            $oeuvres[] = ["id" => $oeuvre->getId(), "name" => $oeuvre->getNom()];
        }
        $queryBuilder = new QueryBuilder();
        if (count($oeuvres) > 0) {
            $id_oeuvres = [];
            foreach ($oeuvres as $oeuvre) {
                $id_oeuvres[] = $oeuvre["id"];
            }
            $otherOeuvresModels = $queryBuilder->select()
                ->from("oeuvres", "O")
                ->where("O.id NOT IN (" . implode(",", $id_oeuvres) . ")")
                ->getQuery()->getArrayResult(oeuvres::class);
        } else {
            $otherOeuvresModels = $queryBuilder->select()
                ->from("oeuvres", "O")->getQuery()->getArrayResult(oeuvres::class);
        }
        $otherOeuvres = [];
        foreach ($otherOeuvresModels as $oeuvre) {
            $otherOeuvres[] = ["id" => $oeuvre->getId(), "name" => $oeuvre->getNom()];
        }

        $json = $exposition->jsonSerialize();
        $json["oeuvres"] = $oeuvres;
        $json["other_oeuvres"] = $otherOeuvres;
        echo json_encode($json);
    }
	
}