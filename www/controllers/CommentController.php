<?php
namespace cmsProject\controllers;

use cmsProject\core\Exceptions\ExceptionHandler;
use cmsProject\core\MiddlewareHandler;
use cmsProject\managers\CommentManager;
use cmsProject\managers\CommentsZoneManager;

use cmsProject\core\helpers;
use cmsProject\core\View;
use cmsProject\models\comments_zones;
use cmsProject\models\comments;

class CommentController {

    /*
     *    Controller permettant de gérer à la fois les zones de commentaires que l'on ajoutera sur le Dashboard (methodes avec "Zone")
     *    mais aussi les commentaires en eux même (methodes sans "Zone")
    */

    /**
     * Permet de gérer l'ajout des commentaires (pas les zones) et lie le nouveau commentaire à une zone
     */
    public function addAction() {

        // Vérifie si l'utilisateur est connecté
        if (!empty($_SESSION['user_id'])) {
            $commentManager = new CommentManager();

            $comment = new comments();
            $comment->setContent($_POST['content']);
            $comment->setComments_zone_id($_POST['comments_zone_id']);
            $comment->setUser_id($_SESSION['user_id']);
            $comment->setCreated_at();

            $commentManager->save($comment);
        }
        header("Location: ". $_SERVER['HTTP_REFERER']);
    }

    /*
     * permet de gérrer l'ajout des zones de commentaires (pas les commentaires en eux même)
     */
    public function addZoneAction() {
        MiddlewareHandler::launch('checkRights', ["administrateur"]); // accès aux admins et mainteneur

        $commentsZoneManager = new CommentsZoneManager();

        $comments_zone = new comments_zones();
        $comments_zone->setNom($_POST['nom']);
        $comments_zone->setCreated_at();

        $commentsZoneManager->save($comments_zone);

        header("Location: ". helpers::getUrl('dashboard','commentsZones'));

    }

    /*
     * permet la modification des zones de commentaires
     */
    public function updateZoneAction() {
        MiddlewareHandler::launch('checkRights', ["administrateur"]); // accès aux admins et mainteneur
		$commentsZoneManager = new CommentsZoneManager();

        $comments_zone = $commentsZoneManager->find($_GET['comments_zone_id']);
        $comments_zone->setNom($_POST['nom']);
        $comments_zone->setUpdated_at();

        $commentsZoneManager->save($comments_zone);

        header("Location: ". helpers::getUrl('dashboard','commentsZones'));

    }

    /*
     * Affiche les commentaires en fonction de la zone de commentaire
     */
    public function getCommentAction() {

		if (empty($_GET['comments_zone_id'])) {
			new ExceptionHandler("Invalid parameter : il manque un id de zone de commentaire");
		}

		$commentZoneManager = new CommentsZoneManager();
		$commentZone = $commentZoneManager->find($_GET['comments_zone_id']);
		if ($commentZone == null) {
			new ExceptionHandler("Cette zone de commentaire n'existe pas");
		}

        $commentManager = new CommentManager();
        $comments = $commentManager->findByCommentsZone($_GET["comments_zone_id"]);
        $view = new View("post-comment", "blank");

        $view->assign("comments", $comments);
        $view->assign("commentZone", $commentZone);
    }

    /*
     * Supprimer un commentaire
     */
    public function deleteAction():void{

        if (empty($_GET['comment_id'])) {
			new ExceptionHandler("Invalid parameter : il manque un id de commentaire");
		}

        $commentManager = new CommentManager();
        $comment = $commentManager->find($_GET['comment_id']);

        $comments_zone_id = $comment->getComments_zone_id();
        $commentManager->delete($_GET['comment_id']);

		header("Location: ". $_SERVER['HTTP_REFERER']);

    }

    /*
     * Supprimer une zone de commentaire
     */
    public function deleteZoneAction():void{

        MiddlewareHandler::launch('checkRights', ["administrateur"]); // accès aux admins et mainteneur
        if (empty($_GET['comments_zone_id'])) {
			new ExceptionHandler("Invalid parameter : Il manque un id de zone");
		}
        $commentsZoneManager = new CommentsZoneManager();
        $comment = $commentsZoneManager->find($_GET['comments_zone_id']);
        if ($comment == null) {
            throw new ExceptionHandler("Ce commentaire n'existe pas");
        }

        $commentManager = new CommentManager();

        $comments = $commentManager->findByCommentsZone($_GET['comments_zone_id']);
        foreach($comments as $comment){
            $commentManager->delete($comment->getId());
        }
        $commentsZoneManager->delete($_GET['comments_zone_id']);

		header("Location: ". helpers::getUrl('dashboard','commentsZones'));

    }

    public function getCommentsZoneByIdAction() {

        MiddlewareHandler::launch('handleToken'); // vérifie le token

        $commentsZone = (new CommentsZoneManager())->find($_GET["comments_zone_id"]);

        echo json_encode($commentsZone->jsonSerialize());

    }
}
