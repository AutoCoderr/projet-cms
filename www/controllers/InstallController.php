<?php
namespace cmsProject\controllers;
use cmsProject\core\Installer\InitDB;
use cmsProject\core\Installer\Installer;
use cmsProject\core\Validator;
use cmsProject\core\View;
use cmsProject\services\AccountService;

class InstallController {

    public function __construct() {
        if (!isset($_SESSION["token"]) || !isset($_POST["token"]) || $_SESSION["token"] != $_POST["token"])
            die ("Vous n'avez pas accès à cette page");
    }

    /*
     * Creation de l'admin
     */
    public function createAdminAction() {

        $form = InitDB::getAdminRegisterForm();
        $errors[$form["config"]["actionName"]] = Validator::checkForm($form, $_POST);

        unset($_SESSION["token"]);
        unset($_POST["token"]);

        if (count($errors[$form["config"]["actionName"]]) == 0) {

            $accountService = AccountService::getInstance();
            $added = $accountService->addUser($_POST, 1); // 1 => admin rights
            if ($added) {
                $accountService->authenticate($_POST['email'], $_POST['password']);
                header("Location: /");
                exit();
            } else {
                $errors[$form["config"]["actionName"]] = [$form["config"]["errorMsg"]];
            }
        }

        $view = new View("createfirstuser", "installer");
        $view->assign("errors", $errors);
    }

    /*
     * Creation du .env lors par l'installeur
     */
    public function setEnvAction(){
        $installer = new Installer();

        $installer->hydrateConsts();

        unset($_SESSION['token']);
        unset($_POST['token']);
        $errors = $installer->detectErrors();
        if (count($errors) > 0) {
            $installer->checkPresentsConstants();
            $view = new \cmsProject\core\View('setup', 'installer');
            $view->assign('installer', $installer);
            $view->assign('errors', $errors);
            die();
        }
        $installer->checkPresentsConstants();
        $installer->writeEnv();

        $installer->checkPresentsDefaultsConstants();
        $installer->writeDefaultConst();

        header("Location: /");
    }
}