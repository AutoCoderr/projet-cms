<?php
namespace cmsProject\controllers;

use cmsProject\managers\MenuManager;

use cmsProject\core\helpers;
use cmsProject\core\Validator;
use cmsProject\core\View;
use cmsProject\core\MiddlewareHandler;
use cmsProject\core\QueryBuilder;
use cmsProject\core\Exceptions\ExceptionHandler;
use cmsProject\core\Exceptions\UnauthorizedException;

use cmsProject\models\menus;
use cmsProject\models\pages;
use cmsProject\services\PageService;

class MenuController {

    public function __construct() {
		MiddlewareHandler::launch('checkRights', ["administrateur", "mainteneur"]); // accès aux admins et mainteneur
	}

    /*
     * Creation du menu
     */
    public function menusAction(){

        $view = new View("menus");

        $menuManager = new MenuManager();
        $menus = $menuManager->findAll();

        $view->assign("menus", $menus);

    }

    /*
     * Ajout d'un menu
     */
    public function addAction() {

        $menuManager = new MenuManager();

        $menu = new menus();
        $menu->setNom($_POST['nom']);
        $menu->setCreated_at();
        $menu->setUpdated_at();

        $menuManager->save($menu);
        $_POST = array();

        $this->menusAction();

    }

    /*
     * mise à jour du menu
     */
    public function updateAction() {

        $menuManager = new MenuManager();

        $menu = $menuManager->find($_GET['menu_id']);
        $menu->setNom($_POST['nom']);
        $menu->setUpdated_at();

        $menuManager->save($menu);

        $pagesIdsInList = $_POST['pages'];
        $itemsInBdd = $menu->getItems();
        $pagesIdsInBdd = [];
        foreach ($itemsInBdd as $item) {
            $pagesIdsInBdd[] = $item->getPage()->getId();
        }
        foreach ($pagesIdsInList as $pageIdInList) {
            $found = false;
            foreach ($pagesIdsInBdd as $pageIdInBdd) {
                if ($pageIdInBdd == $pageIdInList) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $menu->addItem($pageIdInList);
            }
        }
        foreach ($pagesIdsInBdd as $pageIdInBdd) {
            $found = false;
            foreach ($pagesIdsInList as $pageIdInList) {
                if ($pageIdInList == $pageIdInBdd) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $menu->removeItem($pageIdInBdd);
            }
        }

        $_POST = array();

        $this->menusAction();

    }

    /*
     * Supprimer un menu
     */
    public function deleteAction() {

        if (empty($_GET['menu_id'])) {
            new UnauthorizedException("Invalid Parameter", 401);
        }

        $menuManager = new MenuManager();
        $menu = $menuManager->find($_GET['menu_id']);
        $menu->deletePages();
        $menu->deleteItems();
        $menuManager->delete($_GET['menu_id']);

        $this->menusAction();

    }

    /*
     * Ajout d'un menu à une page
     */
    public function addToPageAction() {
        $form = pages::getAddMenuToPageForm();
        $errors[$form["config"]["actionName"]] = Validator::checkForm($form,$_POST);
        if (count($errors[$form["config"]["actionName"]]) == 0) {
            $pageService = PageService::getInstance();
            $menuAdded = $pageService->addMenu($_POST["menu"],$_POST["page"]);
            if ($menuAdded) {
                header("Location: " . helpers::getUrl("page", "edit") . "?id=" . $_POST["page"]);
                exit();
            } else {
                $errors[$form["config"]["actionName"]] = [$form["config"]["errorMsg"]];
            }
        }
        if (isset($_POST["page"])) {
            $pageService = PageService::getInstance();
            $infos = $pageService->getInfoPage($_POST["page"]);
            $view = new View("editpage", "edit");
            $view->assign("errors", $errors);
            $view->assign("page", $infos["page"]);
            $view->assign("menus", $infos["menus"]);
            $view->assign("otherMenus", $infos["otherMenus"]);
        } else {
            new ExceptionHandler("Il manque l'id de la page");
        }
    }

    /*
     * retrait d'un menu à une page
     */
    public function removeFromPageAction() {
        if (empty($_POST["menu"]) || empty($_POST["page"])) {
            new ExceptionHandler("Merci de spécifier les bons id");
        }
        $pageService = PageService::getInstance();
        $removed = $pageService->removeMenu($_POST["menu"], $_POST["page"]);
        if ($removed) {
            header("Location: " . helpers::getUrl("page", "edit") . "?id=" . $_POST["page"]);
            exit();
        } else {
            new ExceptionHandler("Echec de suppression du menu");
        }
    }

    /*
     * Changer l'ordre des menus qui s'affichent sur une page
     */
    public function upOrDownDisplayAction() {
        if (empty($_POST["menu"]) || empty($_POST["page"]) || empty($_POST["action"])) {
            new ExceptionHandler("Merci de spécifier les bons id");
        }
        if ($_POST["action"] != "up" && $_POST["action"] != "down") {
            new ExceptionHandler("Merci de spécifier une action valide");
        }
        $pageService = PageService::getInstance();
        $upped = $pageService->upOrDownMenuInList($_POST["menu"], $_POST["page"], $_POST["action"]);
        if ($upped) {
            header("Location: " . helpers::getUrl("page", "edit") . "?id=" . $_POST["page"]);
            exit();
        } else {
            new ExceptionHandler("Echec de déplacement du menu");
        }
    }

    /*
     * afficher les menus par leurs id
     */
    public function getMenuByIdAction() {

        MiddlewareHandler::launch('handleToken'); // vérifie le token
        
        $menu = (new MenuManager())->find($_GET["menu_id"]);
        $itemsModels = $menu->getItems();
        $pages = [];
        foreach ($itemsModels as $item) {
            $pages[] = ["id" => $item->getPage()->getId(), "name" => $item->getPage()->getUri()];
        }
        $queryBuilder = new QueryBuilder();
        if (count($pages) > 0) {
            $id_pages = [];
            foreach ($pages as $page) {
                $id_pages[] = $page["id"];
            }
            $otherPagesModels = $queryBuilder->select()
                ->from("pages", "P")
                ->where("P.id NOT IN (" . implode(",", $id_pages) . ")")
                ->getQuery()->getArrayResult(pages::class);
        } else {
            $otherPagesModels = $queryBuilder->select()
                ->from("pages", "M")->getQuery()->getArrayResult(pages::class);
        }
        $otherPages = [];
        foreach ($otherPagesModels as $page) {
            $otherPages[] = ["id" => $page->getId(), "name" => $page->getUri()];
        }

        $json = $menu->jsonSerialize();
        $json["pages"] = $pages;
        $json["other_pages"] = $otherPages;
        echo json_encode($json);

    }
}
