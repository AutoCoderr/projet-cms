<?php

namespace cmsProject\core\Exceptions;

class NotFoundException extends ExceptionHandler
{

  public function __construct($message = "Page non trouvée", $code = 404){
    parent::__construct($message, $code);
  }
}