<?php
namespace cmsProject\core\Exceptions;
use cmsProject\core\View;

class ExceptionHandler extends \Exception implements \Throwable {
	protected $message;
	protected $code;

	public function __construct($message, $code = 0) {
		parent::__construct($message, $code);
		if ($code == 0){
			$vue = new View('error', 'blank');
		}else{
			$vue = new View(strval($code), 'blank');
		}
		$vue->assign("error", $message);

	}

	public function to_string(){
		return $this->message;
	}
}