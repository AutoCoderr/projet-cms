<?php
namespace cmsProject\core;

class helpers
{
    public static function getUrl($controller, $action) {
        $listOfRoutes = yaml_parse_file("routes.yml");

        foreach ($listOfRoutes as $url=>$route) {
            if ($route["controller"] == $controller && $route["action"]==$action) {
                return $url;
            }
        }
        echo 'controller->'.$controller.'<br>';

        echo 'action->'.$action.'<br>';
        die("Aucune correspondance pour la route");
    }

    public static function getPageUri($page) {
        return $page->getUri();
    }

    public static function generateRandomString(){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789$%!?*";
        $string = "";

        for ($i=0;$i<25;$i++) {
            $string .= $chars[rand(0,strlen($chars)-1)];
        }

        return $string;
    }
    
}
