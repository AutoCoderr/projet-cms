<?php
namespace cmsProject\core\Installer;

use cmsProject\core\helpers;
use cmsProject\core\Mail;
use cmsProject\services\TokenService;

class Installer
{
    private $list_const;
    private $file;
    private $list_default_const;

    public function __construct($file='.env')
    {
        $this->file = $file;
        $this->list_const =
            [
                'DB_PREFIXE'=>['label'=>'Préfixe de la Database','present'=>false, 'type' => 'text', 'value'=>''],
                'DB_USER'=>['label'=>'Identifiant de la Database','present'=>false, 'type' => 'text',  'value'=>''],
                'DB_PWD'=>['label'=>'Mot de passe de la Database','present'=>false, 'type' => 'password',  'value'=>''],
                'DB_HOST'=>['label'=>'Hôte de la Database','present'=>false, 'type' => 'text',  'value'=>''],
                'DB_NAME'=>['label'=>'Nom de la Database','present'=>false, 'type' => 'text',  'value'=>''],
                'MAIL_HOST'=>['label'=>'Hôte de la boîte mail','present'=>false, 'type' => 'text',  'value'=>''],
                'MAIL_USERNAME'=>['label'=>'Identifiant de la boîte mail','present'=>false, 'type' => 'text',  'value'=>''],
                'MAIL_PASSWORD'=>['label'=>'Mot de passe de la boîte mail','present'=>false, 'type' => 'password',  'value'=>''],
                'MAIL_ALIAS'=>['label'=>'Alias de la boîte mail','present'=>false, 'type' => 'text',  'value'=>''],
                'MAIL_PORT'=>['label'=>'Port de la boîte mail','present'=>false, 'type' => 'text',  'value'=>''],
                'MAIL_ENCRYPTION'=>['label'=>'Protocole (TLS/SSL) de la boîte mail','present'=>false, 'type' => 'select', 'choices' => ['ssl', 'tls'] ,  'value'=>''],
                'SITE_NAME'=>['label'=>'Nom de votre site','present'=>false, 'type' => 'text',  'value'=>'']
            ];
        $this->list_default_const =
            [
                'PASSWORD_SALT'=>['value'=>$this->generateRandomString(),'present'=>false],
                'NB_SALT_PASSWD'=>['value'=>50,'present'=>false],
                'AJAX_TOKEN_ENCODER'=>['value'=>$this->generateRandomString(),'present'=>false],
                'DB_DRIVER'=>['value'=>'mysql','present'=>false],
            ];
    }

    //Verifie la presence des constantes dites "fixes"
    public function checkPresentsDefaultsConstants(){
        $list_env = file_get_contents($this->file);
        $lines = explode("\n", $list_env);
        foreach ($lines as $line) {
            $const = explode("=", $line)[0];
            if(isset($this->list_default_const[$const])) {
                $this->list_default_const[$const]['present']= true;
            }
        }
    }

    //Verifie la presence de toutes les constantes "fixes"
    public function anyDefaultConstantMissing() {
        foreach ($this->list_default_const as $key => $values){
            if (!$values['present']){
                return true;
            }
        }
        return false;
    }

    //Remplit le .env des constantes "fixes"
    public function writeDefaultConst(){
        foreach ($this->list_default_const as $key => $value){
            if(!$value['present']){
                file_put_contents($this->file, "\n".$key.'='.strip_tags($value['value']),FILE_APPEND);
            }
        }
    }

    //Genere le .env s'il n'existe pas
    public function generateEnv(){
        $file = '.env';
        file_put_contents($file, '');
    }

    //Verifie les presences des constantes
    public function checkPresentsConstants(){
        $list_env = file_get_contents($this->file);
        $lines = explode("\n", $list_env);
        foreach ($lines as $line) {
            $const = explode("=", $line)[0];
            if(isset($this->list_const[$const])) {
                $this->list_const[$const]['present']= true;
            }
        }
    }

    //Verifie la presence de toutes les constantes
    public function anyConstantMissing(){
        foreach ($this->list_const as $key => $values){
            if (!$values['present']){
                return true;
            }
        }
        return false;
    }

    //Genere le formulaire qui demande les constantes
    public function generateForm(){
        $_SESSION['token'] = $this->generateRandomString();

        echo "<form method='POST' action='".helpers::getUrl('install','setEnv')."' class='form-style'>";
        foreach ($this->list_const as $key => $value){
            if (!$value['present']){
                echo "<h2 class='text-center'>".$value['label']." * : </h2>";
                if ($value['type'] == 'select') {
                    echo "<select name='$key'>";
                    foreach ($value['choices'] as $choice) {
                        echo "<option value='$choice'>$choice</option>";
                    }
                    echo "</select>";
                } else {
                    echo "<input type='".$value['type']."' name='$key' value='" . $value['value'] . "' required/>";
                }
            }
        }
        echo "<input type='hidden' name='token' value='".$_SESSION['token']."'/>";
        echo "<input class='button-blue button-center' type='submit'/>";
        echo "</form>";
    }

    //Remplit le .env
    public function writeEnv(){
        foreach ($this->list_const as $key => $value){
            if(isset($_POST[$key]) && !$this->list_const[$key]['present']){
                file_put_contents($this->file, "\n".$key.'='.strip_tags($_POST[$key]),FILE_APPEND);
            }
        }
    }


    private function generateRandomString(){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789$%!?*";
        $string = "";

        for ($i=0;$i<25;$i++) {
            $string .= $chars[rand(0,strlen($chars)-1)];
        }
        return $string;
    }

    //Hydrate les constantes
    public function hydrateConsts(){
        foreach ($this->list_const as $key => $value){
            if(defined($key)){
                $this->list_const[$key]['value'] = constant($key);
            }

            if (isset($_POST[$key])){
                $this->list_const[$key]['value'] = $_POST[$key];
            }
        }
    }

    //Detecte les erreurs des connexions (mail ou DB)
    public function detectErrors() {
        $errors = [];
        if (!$this->DBConnectionWorking()) {
            $errors[] = "L'accès à la base de données a échoué";
        }
        if (!$this->mailConnectionWorking()) {
            $errors[] = "Connexion email échouée";
        }
        return $errors;
    }

    //Verifie la connexion Mail
    private function mailConnectionWorking() {
        $mail = new Mail([
            "MAIL_HOST" => $this->list_const['MAIL_HOST']['value'],
            "MAIL_USERNAME" => $this->list_const['MAIL_USERNAME']['value'],
            "MAIL_PASSWORD" => $this->list_const['MAIL_PASSWORD']['value'],
            "MAIL_ENCRYPTION" => $this->list_const['MAIL_ENCRYPTION']['value'],
            "MAIL_PORT" => $this->list_const['MAIL_PORT']['value'],
            "MAIL_ALIAS" => $this->list_const['MAIL_ALIAS']['value'],
        ]);

        $mail->setDestinataire("test@test.com", "Connection test");
        $mail->setObject('Connection test');
        $mail->setBody("<html><head></head><body>Connection test</body></html>");
        $mail->setAltBody("Connection test");


        if (!$mail->send()) {
            return false;
        }
        return true;
    }

    //Verifie la connexion a la DB
    private function DBConnectionWorking()
    {
        try {
            $this->pdo = new \PDO($this->list_default_const['DB_DRIVER']['value'].":host=".$this->list_const['DB_HOST']['value'].";dbname=".$this->list_const['DB_NAME']['value'], $this->list_const['DB_USER']['value'], $this->list_const['DB_PWD']['value']);
            return true;
        } catch (\Throwable $e) {
            return false;
        }
    }
}