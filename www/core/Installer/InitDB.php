<?php

namespace cmsProject\core\Installer;

use cmsProject\core\helpers;
use cmsProject\core\QueryBuilder;
use cmsProject\core\Exceptions\ExceptionHandler;
use cmsProject\managers\PageManager;
use cmsProject\models\pages;
use cmsProject\models\users;

class InitDB {
    private $queryBuilder;


    public function __construct() {
        $this->queryBuilder = new QueryBuilder();
    }

    public function checkIfDatabaseIsEmpty() {
        $this->queryBuilder->reset();
        $query = $this->queryBuilder->select("1")->from("rights", "r")->getQuery()->getArrayResult();
        return count($query) == 0;
    }

    public function createDatabase() {
        if (!file_exists("core/Installer/datas/db.sql")) {
            new ExceptionHandler ("File '/core/Installer/datas/firsthomepage.html' not found");
            exit();
        }
        $script = file_get_contents("core/Installer/datas/db.sql");
        $this->queryBuilder->reset();
        $this->queryBuilder->addToQuery($script);
        $this->queryBuilder->getQuery();
        $tables = [
            "comments_zones",
            "comments",
            "expositions",
            "menu_items",
            "menus",
            "oeuvres",
            "pages",
            "rights",
            "users",
            "expositions_oeuvres",
            "menus_pages"
        ];
        $this->queryBuilder->reset();
        foreach ($tables as $table) {
            $this->queryBuilder->addToQuery("ALTER TABLE ".$table." RENAME TO ".DB_PREFIXE.$table.";");
        }
        $this->queryBuilder->getQuery();
    }

    public function thereIsNothingAdmin() {
        $this->queryBuilder->reset();
        $nbAdmins = count($this->queryBuilder->select()
            ->from("users", "U")
            ->where("U.right_id = 1") // Admin role
            ->getQuery()->getArrayResult());
        return $nbAdmins == 0;
    }

    public function thereIsNothingHomepage() {
        $this->queryBuilder->reset();
        $nbPage = count($this->queryBuilder->select()
            ->from("pages", "P")
            ->where("P.uri = '/'") // Uri of homepage
            ->getQuery()->getArrayResult());
        return $nbPage == 0;
    }

    public function createFirstHomepage() {
        if (!file_exists("core/Installer/datas/firsthomepage.html")) {
            new ExceptionHandler ("File '/core/Installer/datas/firsthomepage.html' not found");
            exit();
        }
        $homepage = file_get_contents("core/Installer/datas/firsthomepage.html");
        $page = new pages();
        $page->setContent($homepage);
        $page->setNom("Homepage");
        $page->setUri("/");
        $page->setCreated_at();
        $page->setUpdated_at();

        (new PageManager())->save($page);
    }

    public static function getAdminRegisterForm() {
        $form = users::getRegisterForm();
        $form["config"]["action"] = helpers::getUrl("install", "createAdmin");
        $_SESSION['token'] = helpers::generateRandomString();
        $form["fields"]["token"] = [
            "type"=>"hidden",
            "required"=>true,
            "value"=>$_SESSION['token']
        ];
        return $form;
    }
}