<?php
namespace cmsProject\core;

class ConstantLoader
{
    public $text;

    public function __construct() {
        $this->getContentFiles();
        $this->load();
    }

    public function getContentFiles() {
        $this->text .= "\n".file_get_contents('.env');
    }

    public function load() {
        if (!defined('root_dir')) {
            define('root_dir', dirname(__DIR__));
        }
        $lines = explode("\n", $this->text);
        foreach ($lines as $line) {
            $data = explode("=", $line);
            if (!defined($data[0]) && isset($data[1])) {
                define($data[0], trim($data[1]));
            }
        }
    }
}
