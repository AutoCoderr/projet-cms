<?php

namespace cmsProject\core;

use cmsProject\core\connection\BDDInterface;
use cmsProject\core\connection\PDOConnection;

class Manager
{
    private $table;
    private $connection;
    protected $class;

    /*
    * Initialise la connection PDO
    */
    public function __construct(string $class, string $table, BDDInterface $connection = null)
    {
        $this->class = $class;
        $this->table =  DB_PREFIXE.$table;

        $this->connection = $connection;
        if(NULL === $connection)
            $this->connection = new PDOConnection();
    }

    /*
    * Permet de sauvegarder un objet
    */
    public function save($objectToSave)
    {
        $objectArray =  $objectToSave->__toArray(); //transforme l'objet en array

        // supprime les clés qu'on une valeur null sauf l'id
        foreach ($objectArray as $key => $value) {
            if ($key != "id" && gettype($value) == "NULL") {
                unset($objectArray[$key]);
            }
        }

        // récupère les clés et les valeurs
        $columnsData = array_values($objectArray);
        $columns = array_keys($objectArray);

        // On met 2 points devant chaque clé du tableau
        $params = array_combine(
            array_map(function($k){ return ':'.$k; }, $columns),
            $columnsData
        );;

        // Regarde si l'id de l'object est renseigné
        if (!is_numeric($objectToSave->getId())) {

            // supprime l'id des deux tableaux
            array_shift($columns);
            array_shift($params);

            //INSERT            
            $sql = "INSERT INTO ".$this->table." (".implode(",", $columns).") VALUES (:".implode(",:", $columns).");";
        } else {

            //UPDATE
            foreach ($columns as $column) {
                $sqlUpdate[] = $column."=:".$column;
            }

            $sql = "UPDATE ".$this->table." SET ".implode(",", $sqlUpdate)." WHERE id=:id;";
        }
        $this->connection->query($sql, $params);

    }

    /*
    * Renvoie un élément en fonction d'un id
    */
    public function find(?int $id)
    {
        if (is_null($id)) {
            return null;
        }

        $sql = "SELECT * FROM $this->table where id = :id";

        $result = $this->connection->query($sql, [':id' => $id]);

        return $result->getOneOrNullResult($this->class);

    }

    /*
    * Récupère toutes les données d'une table
    */
    public function findAll(): array
    {
        $sql = "SELECT * FROM $this->table";

        $result = $this->connection->query($sql);

        return $result->getArrayResult($this->class);

    }

    /*
    * Récupère les données en fonction des paramètres donnés dans un tableau
    */
    public function findBy(array $params, array $order = null): array
    {
        $results = array();

        $sql = "SELECT * FROM $this->table where ";

        // Select * FROM users WHERE firstname LIKE :firstname ORDER BY id desc

        foreach($params as $key => $value) {
            if(is_string($value))
                $comparator = 'LIKE';
            else
                $comparator = '=';

            $sql .= " $key $comparator :$key and";
            // Select * FROM users WHERE firstname LIKE :firstname and
            // [":firstname" => 'Fadyl%']
            // ["firstname" => 'Fadyl%']
            $params[":$key"] = $value;
            // ["firstname" => 'Fadyl%', ":firstname" => 'Fadyl%']
            unset($params[$key]);
           // [":firstname" => 'Fadyl%']
        }

        $sql = rtrim($sql, 'and');
        // Select * FROM users WHERE firstname LIKE :firstname

        if($order) {
            $sql .= "ORDER BY ". key($order). " ". $order[key($order)];
        }
        // Select * FROM users WHERE firstname LIKE :firstname ORDER BY id desc

        $result = $this->connection->query($sql, $params);
        return $result->getArrayResult($this->class);

    }

    /*
    * renvoie le nombre de lignes en fonction des paramètres renseignés
    */
    public function count(array $params): int
    {


        $sql = "SELECT COUNT(*) FROM $this->table where ";

        foreach($params as $key => $value) {
            if(is_string($value))
                $comparator = 'LIKE';
            else
                $comparator = '=';
            $sql .= " $key $comparator :$key and";

            $params[":$key"] = $value;
            unset($params[$key]);
        }

        $sql = rtrim($sql, 'and');

        $result = $this->connection->query($sql, $params);
        return $result->getValueResult();


    }

    /*
    * Supprime un élément en fonction d'un id
    */
    public function delete(int $id): bool
    {


        $sql = "DELETE FROM $this->table where id = :id";

        $result = $this->connection->query($sql, [':id' => $id]);

        return true;


    }

    /*
    * Permet d'executer une requête sql
    */
    protected function sql($sql, $parameters = null)
    {
        if ($parameters) {
            $queryPrepared = $this->pdo->prepare($sql);
            $queryPrepared->execute($parameters);

            return $queryPrepared;
        } else {
            $queryPrepared = $this->pdo->prepare($sql);
            $queryPrepared->execute();

            return $queryPrepared;
        }
    }
}
