<?php
namespace cmsProject\core;

use cmsProject\Router;

class Validator{

	public static function checkForm($configForm, $data){
        $model = explode("\\", $configForm['config']['model'])[count(explode("\\", $configForm['config']['model']))-1];
        if ($model[strlen($model)-1] == "s") {
            $model = substr($model, 0, strlen($model)-1);
        }
		$listOfErrors = [];

        foreach ($configForm["fields"] as $name => $config) {
            if ($config["type"] == "listCanAddOrDelete" && empty($data[$name])) {
                unset($configForm["fields"][$name]);
            }
        }

		//Vérifications

		//Vérifier le nb de input
		if( count($configForm["fields"]) == count($data) ) {

			foreach ($configForm["fields"] as $name => $config) {
				
				//Vérifie que l'on a bien les champs attendus
				//Vérifier les required
				if( !array_key_exists($name, $data) || 
					( $config["required"] && empty($data[$name]))
				){
					return ["Tentative de hack !!!"];
				}
				
				//Vérifier l'email
				if($config["type"]=="email"){
					if(!self::checkEmail($data[$name])) {
					    $listOfErrors[]=$config["errorMsg"];
					}
				}

                //Vérifier le type datetime-local
                if($config["type"]=="datetime-local"){
                    if(!self::checkDateTime($data[$name])) {
                        $listOfErrors[]=$config["errorMsg"];
                    }
                }

				//Vérifier le captcha
				if ($config["type"]=="captcha") {
					if($_SESSION["captcha"] != $data[$name]){
						return ["Captcha incorrect"];
					}
				}

				//Vérifier le password
				if($config["type"]=="password"){
				    if (!self::checkPwd($data[$name])) {
				        $listOfErrors[]=$config["errorMsg"];
				    }
				}

                if (isset($config["confirmWith"]) && $data[$name] != $data[$config["confirmWith"]]) { //Si il faut comparer ce champs avec un autre
                    //et qu'ils sont differents
                    $listOfErrors[] = $config["errorMsg"];
                }

				if (isset($config["maxlength"]) && strlen($data[$name]) > $config["maxlength"]) {
				    $listOfErrors[] = "Champs ".$name." supérieur à ".$config["maxlength"];
                }
                if (isset($config["minlength"]) && strlen($data[$name]) < $config["minlength"]) {
                    $listOfErrors[] = "Champs ".$name." inferieur à ".$config["minlength"];
				}
				// Vérifie si le champs est unique dans la table
                if (isset($config['uniq'])) {
                    $queryBuilder = new QueryBuilder();
                    $query = $queryBuilder->select()
                        ->from($config['uniq']['table'], "T")
                        ->where($config['uniq']['column']." = :".$config['uniq']['column']);
                    if (isset($_GET[$model."_id"])) {
                        $query = $query->andWhere("T.id != :id")
                        ->setParameter("id", $_GET[$model."_id"]);
                    }
                    $query = $query->setParameter($config['uniq']['column'], $data[$name])->getQuery()->getArrayResult();
                    if (count($query) > 0) {
                        $listOfErrors[] = "Le champs ".$name." est déjà utilisé";
                    }
                }
                if (isset($config['notIn'])) {
                    switch ($config['notIn']) {
                        case "routes": // Vérifie si le champs corespond à l'une des routes présentes sur le site
                            $router = new Router();
                            if ($router->isInRoutes($data[$name])) {
                                $listOfErrors[] = "Cette route est déjà présente sur ce site";
                            }
                    }
                }
			}

		}else{
			return ["Tentative de hack !!!"];
		}

		return $listOfErrors;
	}

	public static function checkDateTime($dateTime) {
	    return preg_match("/[0-9]{4}\\-([0-1][0-9]?)\\-([0-3][0-9]?)\s([0-2][0-9])\\:([0-5][0-9])\\:([0-5][0-9])/i", $dateTime);
    }

	public static function checkEmail($email){
		$email = trim($email);
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}

	public static function checkPwd($pwd){
		//return preg_match("pattern", $pwd);
        return true;
	}

}