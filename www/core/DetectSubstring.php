<?php


namespace cmsProject\core;


class DetectSubstring
{
    public $starts;
    private $content;

    public function __construct($content)
    {
        $this->starts = [];
        $this->content = $content;
    }

    // Detecte la ou les posititions d'un mot dans une chaine de caractère
    public function getStartAndEndIndex($word) { 
        $content = $this->content;
        for ($i = 0;$i < strlen($content); $i++) {
            $match = true;
            for ($j = 0; $j+$i<strlen($content) && $j < strlen($word); $j++) {
                if ($word[$j] != $content[$i+$j]) {
                    $match = false;
                    break;
                }
            }
            if ($match) {
                $this->starts[] = $i;
            }
        }
    }
}