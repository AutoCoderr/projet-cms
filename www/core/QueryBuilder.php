<?php

namespace cmsProject\core;


use cmsProject\core\connection\BDDInterface;
use cmsProject\core\connection\PDOConnection;
use cmsProject\core\connection\ResultInterface;


class QueryBuilder

{

    protected $connection;
    protected $query;
    protected $parameters;
    protected $alias;


    public function __construct(BDDInterface $connection = NULL)
    {
        $this->connection = $connection;
        if(NULL === $connection)
            $this->connection = new PDOConnection();
            
        $this->query = "";
        $this->parameters = [];
    }

    public function select( string $values = '*'): QueryBuilder
    {
        $this->addToQuery("SELECT $values");
        
        return $this;
    }

    public function from( string $table, string $alias): QueryBuilder
    {
        $this->addToQuery("FROM ".DB_PREFIXE.$table." ".$alias);
        $this->alias = $alias;

        return $this;
    }

    public function where( string $conditions): QueryBuilder
    {
        $this->addToQuery("WHERE $conditions");
        
        return $this;
    }

    public function andWhere( string $condition): QueryBuilder
    {
        $this->addToQuery("AND $condition");

        return $this;
    }

    public function groupBy( string $values): QueryBuilder
    {
        $this->addToQuery("GROUP BY $values");
        
        return $this;
    }

    public function setParameter($keyOrArray, string $value = null): QueryBuilder
    {
        if (gettype($keyOrArray) == "string" && $value != null) {
            $this->parameters[":$keyOrArray"] = $value;
        } else if (gettype($keyOrArray) == "array") {
            $this->parameters = $keyOrArray;
        }

        return $this;
    }

    public function join(string $table, string $aliasTarget, string $fieldSource = 'id', string $fieldTarget = 'id'): QueryBuilder
    {
        $aliasSource = $this->alias;

        $this->addToQuery("JOIN ".DB_PREFIXE.$table." $aliasTarget ON $aliasTarget.$fieldTarget = $aliasSource.$fieldSource");

        return $this;
    }

    public function innerJoin(string $table, string $aliasTarget, string $fieldSource = 'id', string $fieldTarget = 'id'): QueryBuilder
    {
        $aliasSource = $this->alias;

        $this->addToQuery("INNER JOIN ".DB_PREFIXE.$table." $aliasTarget ON $aliasTarget.$fieldTarget = $aliasSource.$fieldSource");

        return $this;
    }

    public function leftJoin(string $table, string $aliasTarget, string $fieldSource = 'id', string $fieldTarget = 'id'): QueryBuilder
    {
        $aliasSource = $this->alias;

        $this->addToQuery("LEFT JOIN ".DB_PREFIXE.$table." $aliasTarget ON $aliasTarget.$fieldTarget = $aliasSource.$fieldSource");

        return $this;
    } 


    public function addToQuery(string $query): QueryBuilder
    {
        $this->query .= $query. " ";

        return $this;
    }

    public function getQuery(): ResultInterface
    {
        
        $result =  $this->connection->query($this->query, $this->parameters);

        return $result;
        
    }

    public function reset(): QueryBuilder
    {
        $this->query = "";
        $this->parameters = [];
        $this->alias = "";

        return $this;
    }
}
