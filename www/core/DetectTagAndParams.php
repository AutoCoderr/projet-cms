<?php


namespace cmsProject\core;


class DetectTagAndParams
{
    private $content;
    public $lengths;
    public $starts;
    public $params;

    public function __construct($content)
    {
        $this->lengths = [];
        $this->params = [];
        $this->starts;
        $this->content = $content;
    }
    // Detecte les positions et les paramettres d'une balise html dans une page web
    public function getTagAndParams($tagOpen, $tagClose, $params = []) { 
        $content = $this->content;
        $tagOpen = str_replace(">", "", $tagOpen);
        $openTagIndex = new DetectSubstring($content);
        $openTagIndex->getStartAndEndIndex($tagOpen);
        $closeTagIndex = new DetectSubstring($content);
        $closeTagIndex->getStartAndEndIndex($tagClose);
        $tags = [];
        for ($i = 0;$i<count($openTagIndex->starts);$i++) {
            $this->lengths[] = ($closeTagIndex->starts[$i]+strlen($tagClose))-$openTagIndex->starts[$i];
            $this->params[] = [];
            $j = $openTagIndex->starts[$i]+strlen($tagOpen);
            while ($j < strlen($content) && $content[$j] != ">") {
                for ($k = 0; $k<count($params);$k++) {
                    $match = true;
                    for ($l = 0; $l < strlen($params[$k]) && $j+$l < strlen($content); $l++) {
                        if ($params[$k][$l] != $content[$j+$l]) {
                            $match = false;
                            break;
                        }
                    }
                    if ($match) {
                        $m = $j+$l+2;
                        $str = "";
                        while ($m < strlen($content) && $content[$m] != '"') {
                            $str .= $content[$m];
                            $m += 1;
                        }
                        $this->params[count($this->params)-1][$params[$k]] = $str;
                    }
                }
                $j += 1;
            }
        }
        $this->starts = $openTagIndex->starts;
    }
}