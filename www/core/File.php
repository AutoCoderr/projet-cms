<?php
namespace cmsProject\core;

class File {

    private $filepath;

    public function __construct(string $filepath){
        $this->filepath = $filepath;
    }

    /*
    * Renvoie contenu du fichier
    */
    public function read(): string
    {
        return file_get_contents($this->filepath);
    }

    /*
    * Permet d'écrire dans le fichier 
    */
    public function write(string $str): bool
    {
        return file_put_contents($this->filepath, strip_tags($str));
    }

    /*
    * Permet de remplacer un élément dans le fichier 
    */
    public function replace(string $search, string $replace, string $str = null): bool
    {

        if(null === $str) {
            $str = $this->read();
        }

        $str = str_replace($search, strip_tags($replace), strip_tags($str));

        return $this->write($str);
    }

    /*
    * Renvoie le chemin d'accès du fichier
    */
    public function getFilepath(): string
    {
        return $this->filepath;
    }

 
}