<?php

namespace cmsProject\core\connection;

use cmsProject\core\Model;
use PDO;
use Throwable;
use PDOStatement;

class PDOResult implements ResultInterface
{

    protected $statement;

    public function __construct(PDOStatement $statement)
    {
        $this->statement = $statement;
    }


    public function getArrayResult(string $class = null): array
    {
        $result =  $this->statement->fetchAll(PDO::FETCH_ASSOC);

        if($class) {
            $results = [];
            foreach ($result as $key => $value) {
               array_push($results, (new $class())->hydrate($value));
            }
            return $results;
        }

        return $result;


    }

    public function getOneOrNullResult(string $class = null)
    {
        $result =  $this->statement->fetch();
        if (!$result) $result = null;
        if($class && $result != null) {
            return (new $class())->hydrate($result);
        }

        return $result;


    }

    public function getValueResult()
    {
        return $this->statement->fetchColumn();
    }

}
