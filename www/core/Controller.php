<?php 

namespace cmsProject\core;

use cmsProject\core\FormBuilder\FormBuilder;
use cmsProject\core\FormBuilder\Form;

use cmsProject\core\Model;

class Controller
{
    public function __construct()
    {
    }

    /*
    * Permet de rediriger vers un controlleur ou vers une route en fonction du nombre de paramètres renseignés
    */
    public function redirectTo(string $controllerOrRoute, string $action = null)
    {
        if ($action) {
            header("Location: " . helpers::getUrl($controllerOrRoute, $action));
        } else {
            header("Location: ".$controllerOrRoute);
        }
    }

    /*
    * Permet d'initialiser un formulaire
    */
    public function createForm(string $class, &$model = null): Form
    {
        $form = new $class;
        $form->configureOptions();
        $form->buildForm(new FormBuilder());

        // Si un model est renseigné permet d'associer ses valeurs dans les champs du formulaire
        if($model){
            $form->setModel($model);
            $form->associateValue();
        }
            

        return $form;
    }

    /*
    * Appelle une vue et assigne les paramètres renseignés
    */
    public function render(string $view, string $template = "back", array $params = null)
    { 

        $myView = new View($view, $template);
        if( !is_null($params) ) {
            foreach($params as $key => $param) {
                $myView->assign($key, $param);
            }
        }
    }


}
