<?php

namespace cmsProject\Core;

class MiddlewareHandler
{
    public function __construct()
    {
       
    }

    public static function launch(string $method, $args = null)
    {
        $fileMiddleware =  root_dir.'/middlewares';
        $files = scandir($fileMiddleware); // récupère tous les fichiers du dossier middlewares
        foreach($files as $file)
        {
            if(strpos($file,'.php'))
            {
                $namespace =  self::extract_namespace($fileMiddleware.'/'.$file); 
                $startMiddleware = self::start($namespace, rtrim($file, '.php'), $method, $args);
            }
        }
    }

    /*
    * Permet de récupérer le namespace du dossier renseigné
    */
    public static function extract_namespace($file) {
        $ns = NULL;
        $handle = fopen($file, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if (strpos($line, 'namespace') === 0) {
                    $parts = explode(' ', $line);
                    $ns = rtrim(trim($parts[1]), ';');
                    break;
                }
            }
            fclose($handle);
        }
        return $ns;
    }

    /*
    * Permet d'éxécuter la méthode demandé si elle se trouve dans la classe
    */
    public static function start(string $namespace, string $class, string $method, $args = null)
    {
        $class = $namespace.'\\'.$class;
        if(method_exists($class, $method)) {
            $reflection = new \ReflectionMethod($class, $method); // Créé l'image de notre méthode de classe
            $params = $reflection->getParameters(); // Récupère les paramètres de la méthode
            $paramsToLaunch = [];
            for( $i = 0; $i <count($params); $i++)
            {
                $param =  $params[$i]->getType()->getName(); // récupère le type du paramètre
                if(!is_null($args)) // si on a renseigné args il met la valeur de args
                {
                    $paramsToLaunch[] = $args;
                }
                elseif(class_exists($param)) // Sinon il cherche s'il ne trouve pas une classe portant le même nom
                {
                    $paramToLaunch = new $param;
                    $paramsToLaunch[] = $paramToLaunch;
                }
                elseif($param == 'int' || $param == 'string') 
                {
                    $paramsToLaunch[] = 0;
                }
            }
            $reflection->invokeArgs(new $class, $paramsToLaunch); // Execute la méthode avec les paramètres
        }
    }
}
