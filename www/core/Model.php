<?php
namespace cmsProject\core;

use cmsProject\core\connection\PDOConnection;

class Model implements \JsonSerializable
{

    public function __toArray(): array
    {
        return get_object_vars($this); // retourne les variables de la classe sous forme de tableau
    }

    /*
    * Permet d'initialiser un model à partir d'un tableau de données
    */
    public function hydrate(array $row) 
    {
        $className = get_class($this);
        $articleObj = new $className(); // instance de notre objet model
        foreach ($row as $key => $value) {
          
            $method = 'set'.ucFirst($key);
            if (method_exists($articleObj, $method)) {
                
                // vérifie s'il ne trouve pas relation pour cette clé dans le model
                if($relation = $articleObj->getRelation($key)) {
                    
                    $tmp = new $relation();
                    $tmp = $tmp->hydrate($row);

                    $tmp->setId($value);
                    $articleObj->$method($tmp);
                } else {
                    $articleObj->$method($value);
                }
            }
        }

        return $articleObj;
    }

    public function jsonSerialize() {
        return $this->__toArray(); // Serialize en un object json
    }

    /*
    * Permet de récupérer le tableau retourné dans la méthode initRelation d'un model
    */
    public function getRelation(string $key): ?string
    {
        $relations = $this->initRelation();

        if(isset($relations[$key]))
            return $this->initRelation()[$key];

        return null;
    }

    /**
     * Genere un inner Join sur les tables de liaision à partir de la table donné en argument et de la table source
     */
    public function getFromManyToMany($tableToQuery, $params = null) {
        $tableFromQuery = explode("\\",DB_PREFIXE.get_called_class())[count(explode("\\",DB_PREFIXE.get_called_class()))-1];
        $list = [$tableToQuery,$tableFromQuery];
        // trie dans l'ordre alphabetique pour savoir quel est le nom de la table de liaison
        sort($list);
        $linkTable = $list[0]."_".$list[1];
        $queryBuilder = new QueryBuilder(new PDOConnection());
        $query = $queryBuilder->select("D.*".
            ($params != null && isset($params['otherColumns']) ? ", L." . implode(", L.", $params['otherColumns']) : ""))
            ->from($tableToQuery, "D")
            ->innerJoin($linkTable, "L", "id", $tableToQuery."_id")
            ->where("L.".$tableFromQuery."_id = :id")->setParameter('id', $this->id);

        if ($params != null && isset($params['orderBy'])) {
            $query = $query->addToQuery(" ORDER BY " . $params['orderBy']);
        }
        $query = $query->getQuery();
        return $query->getArrayResult("\\cmsProject\models\\".$tableToQuery);
    }

    /**
    * Insert des données à partir dans une table de liaison par rapport à la table source
    */
    public function insertInManyToMany($tableToInsert, $idToInsert, $params = null) {
        if ($params != null && isset($params["otherToAdd"])) {
            $otherColumnsToAddKey = array_keys($params["otherToAdd"]);
        }
        $tableFromInsert = explode("\\",DB_PREFIXE.get_called_class())[count(explode("\\",DB_PREFIXE.get_called_class()))-1];
        $list = [$tableToInsert,$tableFromInsert];
        sort($list);
        $linkTable = $list[0]."_".$list[1];
        $sql = "INSERT INTO ".DB_PREFIXE.$linkTable." (".$tableFromInsert."_id, ".$tableToInsert."_id";
        if (isset($otherColumnsToAddKey)) {
            $sql .= ", ".implode(", ", $otherColumnsToAddKey);
        }
        $sql .= ") VALUE (:".$tableFromInsert."_id, :".$tableToInsert."_id";
        if (isset($otherColumnsToAddKey)) {
            $sql .= ", :".implode(", :", $otherColumnsToAddKey);
        }
        $sql .= ")";

        $datas = [
            $tableFromInsert."_id" => $this->id,
            $tableToInsert."_id" => $idToInsert
        ];
        if (isset($otherColumnsToAddKey)) {
            $datas = array_merge($datas, $params["otherToAdd"]);
        }

        $queryBuilder = new QueryBuilder(new PDOConnection());
        $queryBuilder->addToQuery($sql);
        $queryBuilder->setParameter($datas);

        $queryBuilder->getQuery();

        return true;
    }

    /**
    * Supprime les données à partir de la table de liaison par rapport à la table source
    */
    public function deleteFromManyToMany($tableToRemove, $idToRemove = null) {
        $tableSource = explode("\\",DB_PREFIXE.get_called_class())[count(explode("\\",DB_PREFIXE.get_called_class()))-1];
        $list = [$tableToRemove,$tableSource];
        sort($list);
        $linkTable = $list[0]."_".$list[1];

        $sql = "DELETE FROM ".DB_PREFIXE.$linkTable." WHERE ".$tableSource."_id = :".$tableSource."_id";

        $datas = [$tableSource."_id" => $this->id];

        if ($idToRemove != null) {
            $sql .= " AND ".$tableToRemove."_id = :".$tableToRemove."_id";
            $datas[$tableToRemove."_id"] = $idToRemove;
        }

        $queryBuilder = new QueryBuilder(new PDOConnection());
        $queryBuilder->addToQuery($sql);
        $queryBuilder->setParameter($datas);

        $queryBuilder->getQuery();

        return true;
    }

    /**
    * met à jour les données à partir de la table de liaison par rapport à la table source
    */
    public function updateManyToMany($tableToUpdate, $idForUpdate, $columsToUpdate) {
        $tableSource = explode("\\",DB_PREFIXE.get_called_class())[count(explode("\\",DB_PREFIXE.get_called_class()))-1];

        $list = [$tableToUpdate,$tableSource];
        sort($list);
        $linkTable = $list[0]."_".$list[1];

        $sql = "UPDATE ".DB_PREFIXE.$linkTable." SET";
        $updateString = "";
        $i = 0;
        foreach ($columsToUpdate as $column => $value) {
            if ($i > 0) {
                $updateString .= " ,";
            }
            $updateString .= " ".$column." = :".$column;
            $i =+ 1;
        }
        $sql .= $updateString." WHERE ".$tableToUpdate."_id = :".$tableToUpdate."_id AND ".$tableSource."_id = :".$tableSource."_id";

        $datas = array_merge($columsToUpdate, [
            $tableToUpdate."_id" => $idForUpdate,
            $tableSource."_id" => $this->id
        ]);

        $queryBuilder = new QueryBuilder(new PDOConnection());
        $queryBuilder->addToQuery($sql);
        $queryBuilder->setParameter($datas);

        $queryBuilder->getQuery();
        return true;
    }
}

