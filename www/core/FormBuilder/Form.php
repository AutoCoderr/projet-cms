<?php

namespace cmsProject\core\FormBuilder;

use cmsProject\core\FormBuilder\Constraints\Validator;
use cmsProject\core\Manager;

class Form
{
    private $builder;
    private $config = [];
    private $model;
    private $name;
    private $isSubmit = false;
    private $isValid = false;
    private $validator;
    private $errors = [];

    //Initialise le validator et mets les valeurs par défaut dans la config
    public function __construct()
    {
        $this->validator = new Validator();

        $this->config = [
            "method"=>"POST", 
            "action"=>"",
            "attr" => [ ]
           
        ];
    }


    //Parcours les élements du Builder en récupérant le nom
    // Si le getter de ce nom existe dans le model lié à la page on le modifie
    public function associateValue()
    {
      
        foreach($this->builder->getElements() as $key => $element)
        {
            $method = 'get'.ucfirst($key);

            if(method_exists($this->model, $method))
            {
                $this->builder->setValue($key, $this->model->$method());
            }
        }

    }

    // Récupèrer les élèments du builder
    public function getElements(): ?array
    {
        return $this->builder->getElements();
    }

    /**
     *  Si on est en POST, on update isSubmit en lançant checkIsSubmit
     *      si on est isSubmit on update isValid en lançant checkIsValid 
     */ 
    public function handle(): void
    {
        if($_SERVER['REQUEST_METHOD'] === $this->config["method"])
        {
            $isSubmit = $this->checkIsSubmitted();

            if($isSubmit)
            {
                $this->checkIsValid();
                
                if($this->isValid && !empty($this->model)) {
                    $this->updateObject();
                }
            }
        }
    }
 
    /*
     * Verifie que chaque variables de $_POST fait partie des éléments du formulaire
     */
    private function checkIsSubmitted()
    {
        foreach($_POST as $key => $value)
        {
            if(!array_key_exists($key, $this->getElements()))
            {
                return false;
            }
        }

        $this->isSubmit = true;
        return true;

    }

    /**
     * Cette méthode regarde pour chaque élements du builder l'ensemble des contraintes
     * Si il y a des contraintes, alors elles les enregistres dans $errors 
     * Elle update $isValid
     */
    public function checkIsValid(): void
    {
        $this->isValid = true;

        if( count($_POST) === (count($this->getElements()) - 1) )
        {

            foreach($_POST as $key => $value)
            {
           
                $element = $this->builder->getElement($key);
                
                if(isset($element->getOptions()['constraints']))
                {
                    foreach($element->getOptions()['constraints'] as $constraint)
                    {
                        $responseValidator = $this->validator->checkConstraint($constraint, $value);
                        
                        if(NULL !== $responseValidator)
                        {
                            $this->isValid = false;
                            $this->errors[$key] = $responseValidator;
                        }
                    }
                    
                }
            }

        } else {
            $this->isValid = false;
            $this->errors["form"] = "Tentative de hack!!!";
        }

    }

    // Insere les valeurs du formulaire dans $model
    public function updateObject(): void
    {

        foreach($_POST as $key => $value)
        {
           
            $method = 'set'.ucfirst($key);

            if(method_exists($this->model, $method))
            {
                $this->model->$method($value);
            }
        }

        ( new Manager(get_class($this->model), str_replace('cmsProject\models\\', '', get_class($this->model) )) )->save($this->model);

        $this->associateValue();
    }

    // SETTER AND GETTER
    public function isSubmit(): bool
    {
        return $this->isSubmit;
    }

    public function isValid(): bool
    {
        return $this->isValid;
    }

    public function setModel($model): self
    {
        $this->model = $model;
        return $this;
    }
    
    public function getModel(): Model
    {
        return $this->model;
    }

    public function getBuilder()
    {
        return $this->builder;
    }

    public function setBuilder(FormBuilder $formBuilder): self
    {
        $this->builder = $formBuilder;

        return $this;
    }

    public function addConfig(string $key, $newConfig): self
    {
        $this->config[$key] = $newConfig;

        return $this;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function setError($key, $value): self
    {
        $this->errors[$key] = [$value];
        return $this;
    }

}
