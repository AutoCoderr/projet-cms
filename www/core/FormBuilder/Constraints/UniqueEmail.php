<?php

namespace cmsProject\core\FormBuilder\Constraints;

use cmsProject\managers\UserManager;


class UniqueEmail implements ConstraintInterface
{

    protected $errorMessage;
    protected $errors = [];

    // mettre un message par défaut si minMessage et maxMessage sont nuls, et setter les valeurs
    public function __construct(string $errorMessage = null)
    {

        $this->errorMessage = $errorMessage;

        if(NULL == $this->errorMessage)
            $this->minMessage = "Cette adresse mail est déjà utilisée";

    }

    // vérifie que la valeur est entre min et max
    // sinon on ajoute dans errors l'erreur associé
    public function isValid(string $value): bool
    {
        $this->errors = [];

        $users = (new UserManager())->findBy( ['email' => $value] );

        if( !empty($users) ) {
            $this->errors[] = $this->errorMessage;
        }

        return (0 == count($this->errors));
    }

    // On retourne le tableau d'erreurs, vide si pas d'erreur
    public function getErrors(): array
    {
        return $this->errors;
    }
}