<?php

namespace cmsProject\core\FormBuilder\Constraints;

interface ConstraintInterface
{
    public function isValid(string $value): bool;

    public function getErrors(): array;
}
