<?php

namespace cmsProject\core\FormBuilder\Constraints;

use cmsProject\managers\UserManager;

use cmsProject\models\users;

class isActualPwd implements ConstraintInterface
{

    protected $errorMessage;
    protected $errors = [];

    // mettre un message par défaut si minMessage et maxMessage sont nuls, et setter les valeurs
    public function __construct(string $errorMessage = null)
    {
        $this->errorMessage = $errorMessage;

        if(NULL == $this->errorMessage)
            $this->errorMessage = "Mauvais mot de passe";

    }

    // vérifie que la valeur est entre min et max
    // sinon on ajoute dans errors l'erreur associé
    public function isValid(string $value): bool
    {
        $this->errors = [];

        $user = (new UserManager())->find($_SESSION['user_id']);
    
        if($user->getPassword() !== users::hashPasswd($value)) {
            $this->errors[] = $this->errorMessage;
        }

        return (0 == count($this->errors));
    }

    // On retourne le tableau d'erreurs, vide si pas d'erreur
    public function getErrors(): array
    {
        return $this->errors;
    }
}