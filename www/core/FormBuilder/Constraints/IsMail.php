<?php

namespace cmsProject\core\FormBuilder\Constraints;


class IsMail implements ConstraintInterface
{

    protected $errors = [];

    // mettre un message par défaut si minMessage et maxMessage sont nuls, et setter les valeurs
    public function __construct()
    {
    }

    // vérifie que la valeur est entre min et max
    // sinon on ajoute dans errors l'erreur associé
    public function isValid(string $value): bool
    {
        $this->errors = [];

        if(!filter_var($value, FILTER_VALIDATE_EMAIL)){
            $this->errors[] = "Ce champ n'est pas un email";
        }

        return (0 == count($this->errors));
    }

    // On retourne le tableau d'erreurs, vide si pas d'erreur
    public function getErrors(): array
    {
        return $this->errors;
    }
}