<?php

namespace cmsProject\core\FormBuilder\Constraints;


class ConfirmPwd implements ConstraintInterface
{

    protected $confirmWith;
    protected $errorMessage;
    protected $errors = [];

    // mettre un message par défaut si minMessage et maxMessage sont nuls, et setter les valeurs
    public function __construct(string $confirmWith, string $errorMessage = null)
    {
        $this->confirmWith = $confirmWith;
        $this->errorMessage = $errorMessage;

        if(NULL == $this->errorMessage)
            $this->errorMessage = "Les champs ne correspondent pas";

    }

    // vérifie que la valeur est entre min et max
    // sinon on ajoute dans errors l'erreur associé
    public function isValid(string $value): bool
    {
        $this->errors = [];

        if($value !== $_POST[$this->confirmWith])
        {
            $this->errors[] = $this->errorMessage;
        }

        return (0 == count($this->errors));
    }

    // On retourne le tableau d'erreurs, vide si pas d'erreur
    public function getErrors(): array
    {
        return $this->errors;
    }
}