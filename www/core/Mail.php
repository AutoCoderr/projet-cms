<?php
namespace cmsProject\core;

use cmsProject\vendor\PHPMailer\PHPMailer\src\PHPMailer;

class Mail extends PHPMailer {

    /*
    * Configuration du serveur mail
    */
    public function __construct($datas = []){
        $mail_host = isset($datas['MAIL_HOST']) ? $datas['MAIL_HOST'] : MAIL_HOST;
        $mail_username = isset($datas['MAIL_USERNAME']) ? $datas['MAIL_USERNAME'] : MAIL_USERNAME;
        $mail_password = isset($datas['MAIL_PASSWORD']) ? $datas['MAIL_PASSWORD'] : MAIL_PASSWORD;
        $mail_encryption = isset($datas['MAIL_ENCRYPTION']) ? $datas['MAIL_ENCRYPTION'] : MAIL_ENCRYPTION;
        $mail_port = isset($datas['MAIL_PORT']) ? $datas['MAIL_PORT'] : MAIL_PORT;
        $mail_alias = isset($datas['MAIL_ALIAS']) ? $datas['MAIL_ALIAS'] : MAIL_ALIAS;

        $this->Host = $mail_host; // Spécifier le serveur SMTP
        $this->SMTPAuth = true; // Activer authentication SMTP
        $this->Username =  $mail_username; // Votre adresse email d'envoi
        $this->Password = $mail_password; // Le mot de passe de cette adresse email
        $this->SMTPSecure = $mail_encryption; // Accepter SSL
        $this->Port = $mail_port;
        $this->CharSet = 'UTF-8';

        $this->isSMTP(); // Paramétrer le Mailer pour utiliser SMTP
        $this->isHTML(true);

        $this->setFrom($mail_username, $mail_alias); // Personnaliser l'envoyeur

    }

    public function setReplyTo($email, $alias = "") {
        $this->addReplyTo($email, $alias);
    }

    public function setDestinataire($email, $alias = "") {
        $this->addAddress($email, $alias); 
    }

    public function setObject($object) {
        $this->Subject = $object;
    }

    public function setBody($body) {
        $this->Body = $body;
    }

    public function setAltBody($altBody) {
        $this->AltBody = $altBody;
    }
}