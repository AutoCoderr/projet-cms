<?php
namespace cmsProject\core;
use cmsProjec\core\Exceptions\ExceptionHandler;

class View
{
    private $template;
    private $view;
    private $content;
    private $data = [];
    private $rended = false;


    public function __construct($view, $template="back") {
        $this->setTemplate($template);
        $this->setView($view);
    }


    public function setTemplate($t) {
        $this->template = strtolower(trim($t));

        if (!file_exists("views/templates/".$this->template.".php")) {
            new ExceptionHandler("Le template n'existe pas");
        }
    }


    public function setView($v) {
        $this->view = strtolower(trim($v));

        if (!file_exists("views/".$this->view.".php")) {
            $this->content = $v;
        }
    }


    public function assign($key, $value) {
        $this->data[$key] = $value;
    }

    public function addModal($modal, $data) {
        if (!file_exists("views/modals/".$modal.".php")) {
            new ExceptionHandler("Le modal n'existe pas!!!");
        }

        include "views/modals/".$modal.".php";
    }

    public function addForm($form, $data) {
        if (!file_exists("views/forms/".$form.".php")) {
            new ExceptionHandler("Le form n'existe pas!!!");
        }

        include "views/forms/".$form.".php";
    }

    private function setContent() {
        extract($this->data);
        if ($this->content == null) {
            ob_start();
            include("views/" . $this->view . ".php");
            $this->content = ob_get_clean();
        }
    }

    // Retourne la vue dans une chaine de caractère au lieu de l'aficher directement
    public function renderView(){
        $this->setContent();
        $this->rended = true;
        return $this->content;
    }

    public function __destruct() {
        if (!$this->rended) {
            extract($this->data);
            $this->setContent();;

            include "views/templates/" . $this->template . ".php";
        }
    }
}
