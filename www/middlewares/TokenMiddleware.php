<?php

namespace cmsProject\middlewares;

use cmsProject\services\TokenService;



class TokenMiddleware
{

    /*
    * Permet de vérifier qu'un token est renseigné dans le header Authorization 
    * et vérifie qu'il est valide
    */
    public function handleToken()
    {

        $headers = apache_request_headers(); // récupère les headers de la requête http

        if (!empty($headers['Authorization'])) {

            $tokenService = new TokenService();

            $user_id = $tokenService->setSecret_key(AJAX_TOKEN_ENCODER)
                                    ->handle($headers['Authorization']);
            
            if($user_id == $_SESSION['user_id']){
                return;
            }
            
        }
        
        echo json_encode(["status" => 401, "message" => "Vous ne pouvez pas acceder a cette ressource", "data" => null]); // renvoie erreur 401
        exit();
    }
}