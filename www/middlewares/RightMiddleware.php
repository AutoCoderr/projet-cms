<?php

namespace cmsProject\middlewares;

use cmsProject\core\Exceptions\UnauthorizedException;

use cmsProject\managers\UserManager;
use cmsProject\managers\RightManager;

class RightMiddleware
{

    /*
    * Vérifie que l'utilisateur connecté a un des droits renseignés dans un tableau 
    */
    public function checkRights(array $authorizedRightLevels)
    {

        if(!empty($_SESSION['user_id'])) {
            $userManager = new UserManager();

            $user = $userManager->find($_SESSION['user_id']); // récupère utilisateur connecté

            if(!empty($user)) {

                $rightManager = new RightManager();

                $right = $rightManager->find( $user->getRight_id() ); //recupère le niveau de droit de l'utilisateur

                // Vérifie que le niveau de droits de l'utilisateur est dans le tableau
                if( is_integer(array_search($right->getRole(), $authorizedRightLevels)) ) {
                    return true;
                }
            }
        } 
        new UnauthorizedException("Vous n'avez les droits nécessaires pour accéder à cette page"); // renvoie erreur 401
    }
}
