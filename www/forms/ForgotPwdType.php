<?php

namespace cmsProject\forms;

use cmsProject\core\FormBuilder\Form;
use cmsProject\core\FormBuilder\Constraints\Length;
use cmsProject\core\FormBuilder\Constraints\IsMail;
use cmsProject\core\FormBuilder\FormBuilder;
use cmsProject\core\helpers;

class ForgotPwdType extends Form {


    public function buildForm(FormBuilder $builder)
    {

        $this->setBuilder(
            $builder
                ->add('email', 'email', [
                    'label' => 'Email',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Votre email"
                    ],
                    'constraints' => [
                        new IsMail(),
                        new Length(5,255, 'Votre email doit contenir au moins 5 caractères', 'Votre nom doit contenir au plus 255 caractères')
                    ]
                ])
                ->add('submit', 'submit', [
                    'label' => 'Réinitialiser le mot de passe',
                    'attr' => [
                        'class' => "button-blue input-button"
                    ]
                ])
                    );

                

    }

    public function configureOptions(): void
    {
        $this
            ->addConfig('method', 'POST')
            ->setName('resetPassword')
            ->addConfig('attr', [
                "class"=>"form-style",
            ]);
    }
}