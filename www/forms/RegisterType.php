<?php

namespace cmsProject\forms;

use cmsProject\core\FormBuilder\Form;
use cmsProject\core\FormBuilder\Constraints\Length;
use cmsProject\core\FormBuilder\Constraints\IsMail;
use cmsProject\core\FormBuilder\Constraints\UniqueEmail;
use cmsProject\core\FormBuilder\Constraints\ConfirmPwd;
use cmsProject\core\FormBuilder\FormBuilder;
use cmsProject\core\helpers;

class RegisterType extends Form {


    public function buildForm(FormBuilder $builder)
    {

        $this->setBuilder(
            $builder
                ->add('firstname', 'text', [
                    'label' => 'Prénom',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Votre prénom"
                    ],
                    'constraints' => [
                        new Length(2,50, 'Votre prénom doit contenir au moins 2 caractères', 'Votre prénom doit contenir au plus 50 caractères')
                    ]
                ])
                ->add('lastname', 'text', [
                    'label' => 'Nom',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Votre nom"
                    ],
                    'constraints' => [
                        new Length(2,100, 'Votre nom doit contenir au moins 2 caractères', 'Votre nom doit contenir au plus 100 caractères')
                    ]
                ])
                ->add('email', 'email', [
                    'label' => 'Email',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Votre email"
                    ],
                    'constraints' => [
                        new IsMail(),
                        new UniqueEmail('Cette adresse mail est déjà utilisée'),
                        new Length(5,255, 'Votre email doit contenir au moins 5 caractères', 'Votre nom doit contenir au plus 255 caractères')
                    ]
                ])
                ->add('password', 'password', [
                    'label' => 'Mot de passe',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Votre mot de passe"
                    ],
                    'constraints' => [
                        new Length(8,50, 'Votre mot de passe doit contenir au moins 8 caractères', 'Votre mot de passe doit contenir au plus 50 caractères'),
                        new ConfirmPwd('passwordConfirm', 'Les mots de passe ne correspondent pas')
                    ]
                ])
                ->add('passwordConfirm', 'password', [
                    'label' => 'Confirmation mot de passe',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Confirmer votre mot de passe"
                    ],
                    'constraints' => [
                        new Length(8,50, 'La confirmation du mot de passe doit contenir au moins 8 caractères', 'La confirmation du mot de passe doit contenir au plus 50 caractères')
                    ]
                ])
                ->add('submit', 'submit', [
                    'label' => "S'inscrire",
                    'attr' => [
                        'class' => "button-blue input-button"
                    ]
                ])
                    );

                

    }

    public function configureOptions(): void
    {
        $this
            ->addConfig('method', 'POST')
            ->setName('register')
            ->addConfig('attr', [
                "class"=>"form-style",
            ]);
    }
}