<?php

namespace cmsProject\forms;

use cmsProject\core\FormBuilder\Form;
use cmsProject\core\FormBuilder\Constraints\Length;
use cmsProject\core\FormBuilder\Constraints\IsMail;
use cmsProject\core\FormBuilder\FormBuilder;
use cmsProject\core\helpers;

class LoginType extends Form {


    public function buildForm(FormBuilder $builder)
    {

        $this->setBuilder(
            $builder
                ->add('email', 'email', [
                    'label' => 'Email',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Votre email"
                    ],
                    'constraints' => [
                        new IsMail(),
                        new Length(5,255, 'Votre email doit contenir au moins 5 caractères', 'Votre nom doit contenir au plus 255 caractères')
                    ]
                ])
                ->add('password', 'password', [
                    'label' => 'Mot de passe',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Votre mot de passe"
                    ],
                    'constraints' => [
                        new Length(8,50, 'Votre mot de passe doit contenir au moins 8 caractères', 'Votre mot de passe doit contenir au plus 50 caractères')
                    ]
                ])
                ->add('submit', 'submit', [
                    'label' => 'Connexion',
                    'attr' => [
                        'class' => "button-blue input-button"
                    ]
                ])
                    );

                

    }

    public function configureOptions(): void
    {
        $this
            ->addConfig('method', 'POST')
            ->setName('login')
            ->addConfig('attr', [
                "class"=>"form-style",
            ]);
    }
}
