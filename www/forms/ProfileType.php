<?php

namespace cmsProject\forms;

use cmsProject\core\FormBuilder\Form;
use cmsProject\core\FormBuilder\Constraints\Length;
use cmsProject\core\FormBuilder\Constraints\IsMail;
use cmsProject\core\FormBuilder\FormBuilder;

class ProfileType extends Form {


    public function buildForm(FormBuilder $builder)
    {

        $this->setBuilder(
            $builder
                ->add('firstname', 'text', [
                    'label' => 'Prénom',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Votre prénom"
                    ],
                    'constraints' => [
                        new Length(2,50, 'Votre prénom doit contenir au moins 2 caractères', 'Votre prénom doit contenir au plus 50 caractères')
                    ]
                ])
                ->add('lastname', 'text', [
                    'label' => 'Nom',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Votre nom"
                    ],
                    'constraints' => [
                        new Length(2,100, 'Votre nom doit contenir au moins 2 caractères', 'Votre nom doit contenir au plus 100 caractères')
                    ]
                ])
                ->add('email', 'email', [
                    'label' => 'Email',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Votre email"
                    ],
                    'constraints' => [
                        new IsMail(),
                        new Length(5,255, 'Votre email doit contenir au moins 5 caractères', 'Votre nom doit contenir au plus 255 caractères')
                    ]
                ])
                ->add('submit', 'submit', [
                    'label' => "Sauvegarder",
                    'attr' => [
                        'class' => "button-blue input-button"
                    ]
                ])
                    );

                

    }

    public function configureOptions(): void
    {
        $this
            ->addConfig('method', 'POST')
            ->setName('profile')
            ->addConfig('attr', [
                "class"=>"form-style",
            ]);
    }
}