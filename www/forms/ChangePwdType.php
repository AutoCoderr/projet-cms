<?php

namespace cmsProject\forms;

use cmsProject\core\FormBuilder\Form;
use cmsProject\core\FormBuilder\Constraints\Length;
use cmsProject\core\FormBuilder\Constraints\ConfirmPwd;
use cmsProject\core\FormBuilder\Constraints\isActualPwd;
use cmsProject\core\FormBuilder\FormBuilder;
use cmsProject\core\helpers;

class ChangePwdType extends Form {


    public function buildForm(FormBuilder $builder)
    {

        $this->setBuilder(
            $builder
                ->add('actualPassword', 'password', [
                    'label' => 'Mot de passe actuel',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Votre mot de passe actuel"
                    ],
                    'constraints' => [
                        new Length(8,50, 'Votre mot de passe doit contenir au moins 8 caractères', 'Votre mot de passe doit contenir au plus 50 caractères'),
                        new isActualPwd("Mauvais mot de passe") 
                    ]
                ])
                ->add('newPassword', 'password', [
                    'label' => 'Nouveau mot de passe',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Votre nouveau mot de passe"
                    ],
                    'constraints' => [
                        new Length(8,50, 'Votre mot de passe doit contenir au moins 8 caractères', 'Votre mot de passe doit contenir au plus 50 caractères'),
                        new ConfirmPwd('newPasswordConfirm', 'Les mots de passe ne correspondent pas')
                    ]
                ])
                ->add('newPasswordConfirm', 'password', [
                    'label' => 'Confirmer nouveau mot de passe',
                    'required' => true,
                    'attr' => [
                        'placeholder' => "Votre nouveau mot de passe"
                    ],
                    'constraints' => [
                        new Length(8,50, 'La confirmation du mot de passe doit contenir au moins 8 caractères', 'La confirmation du mot de passe doit contenir au plus 50 caractères')
                    ]
                ])
                ->add('submit', 'submit', [
                    'label' => "Sauvegarder",
                    'attr' => [
                        'class' => "button-blue input-button"
                    ]
                ])
                    );

                

    }

    public function configureOptions(): void
    {
        $this
            ->addConfig('method', 'POST')
            ->setName('changePwd')
            ->addConfig('attr', [
                "class"=>"form-style",
            ]);
    }
}