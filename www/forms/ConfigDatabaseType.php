<?php

namespace cmsProject\forms;

use cmsProject\core\FormBuilder\Form;
use cmsProject\core\FormBuilder\Constraints\Length;
use cmsProject\core\FormBuilder\FormBuilder;

class ConfigDatabaseType extends Form {


    public function buildForm(FormBuilder $builder)
    {

        $this->setBuilder(
            $builder
                ->add('host', 'text', [
                    'label' => 'Host',
                    'required' => true,
                    'value' => DB_HOST,
                    'attr' => [
                        'placeholder' => "Host de la base de données",
                        'disabled' => 'disabled'
                    ]
                ])
                ->add('user', 'text', [
                    'label' => 'Identifiant',
                    'required' => true,
                    'value' => DB_USER,
                    'attr' => [
                        'placeholder' => "Identifiant du serveur",
                        'disabled' => 'disabled'
                    ],
                    'constraints' => [
                        new Length(1,20, 'Le nom du site doit contenir au moins 1 caractères', 'Le nom du site doit contenir au plus 20 caractères')
                    ],
                ])
                ->add('pwd', 'text', [
                    'label' => 'Mot de passe',
                    'required' => true,
                    'value' => DB_PWD,
                    'attr' => [
                        'placeholder' => "Mot de passe du serveur",
                        'disabled' => 'disabled'
                    ]
                ])
                ->add('name', 'text', [
                    'label' => 'Nom de la base de données',
                    'required' => true,
                    'value' => DB_NAME,
                    'attr' => [
                        'placeholder' => "Nom de la base de données",
                        'disabled' => 'disabled'
                    ]
                ])
                ->add('driver', 'text', [
                    'label' => 'Driver',
                    'required' => true,
                    'value' => DB_DRIVER,
                    'attr' => [
                        'placeholder' => "Driver de la base de données",
                        'disabled' => 'disabled'
                    ]
                ])
                ->add('prefixe', 'text', [
                    'label' => 'Préfixe des tables',
                    'required' => true,
                    'value' => DB_PREFIXE,
                    'attr' => [
                        'placeholder' => "Préfixe des tables",
                        'disabled' => 'disabled'
                    ]
                ])
                ->add('description', 'text', [
                    'label' => 'Pour modifier ces données, veuillez vous rendre dans le .env',
                    'attr_label' => [
                        'style' => 'color : red; font-weight : bold;'
                    ]
                ])
        );

                

    }

    public function configureOptions(): void
    {
        $this
            ->addConfig('method', 'POST')
            ->setName('configDatabase')
            ->addConfig('attr', [
                "class"=>"form-style",
            ]);
    }
}