<?php

namespace cmsProject\forms;

use cmsProject\core\FormBuilder\Form;
use cmsProject\core\FormBuilder\Constraints\Length;
use cmsProject\core\FormBuilder\FormBuilder;

class ConfigSiteType extends Form {


    public function buildForm(FormBuilder $builder)
    {

        $this->setBuilder(
            $builder
                ->add('name', 'text', [
                    'label' => 'Nom du site',
                    'required' => true,
                    'value' => SITE_NAME,
                    'attr' => [
                        'placeholder' => "Nom du site"
                    ],
                    'constraints' => [
                        new Length(1,20, 'Le nom du site doit contenir au moins 1 caractères', 'Le nom du site doit contenir au plus 20 caractères')
                    ]
                ])
                ->add('submit', 'submit', [
                    'label' => 'Sauvegarder',
                    'attr' => [
                        'class' => "button-blue input-button"
                    ]
                ])
                    );

                

    }

    public function configureOptions(): void
    {
        $this
            ->addConfig('method', 'POST')
            ->setName('configSite')
            ->addConfig('attr', [
                "class"=>"form-style",
            ]);
    }
}