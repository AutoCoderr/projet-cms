<?php

namespace cmsProject\forms;

use cmsProject\core\FormBuilder\Form;
use cmsProject\core\FormBuilder\Constraints\Length;
use cmsProject\core\FormBuilder\Constraints\IsMail;
use cmsProject\core\FormBuilder\FormBuilder;

class ConfigMailType extends Form {


    public function buildForm(FormBuilder $builder)
    {

        $this->setBuilder(
            $builder
                ->add('host', 'text', [
                    'label' => 'Host du serveur',
                    'required' => true,
                    'value' => MAIL_HOST,
                    'attr' => [
                        'placeholder' => "Host du serveur"
                    ]
                ])
                ->add('username', 'text', [
                    'label' => 'Identifiant du serveur',
                    'required' => true,
                    'value' => MAIL_USERNAME,
                    'attr' => [
                        'placeholder' => "Identifiant du serveur"
                    ],
                    'constraints' => [
                        new IsMail()
                    ]
                ])
                ->add('password', 'text', [
                    'label' => 'Mot de passe du serveur',
                    'required' => true,
                    'value' => MAIL_PASSWORD,
                    'attr' => [
                        'placeholder' => "Mot de passe du serveur"
                    ]
                ])
                ->add('alias', 'text', [
                    'label' => 'Alias de l\'adresse mail',
                    'required' => false,
                    'value' => MAIL_ALIAS,
                    'attr' => [
                        'placeholder' => "Alias de l\'adresse maile"
                    ]
                ])
                ->add('port', 'text', [
                    'label' => 'Port du serveur',
                    'required' => true,
                    'value' => MAIL_PORT,
                    'attr' => [
                        'placeholder' => "Port du serveur"
                    ]
                ])
                ->add('encryption', 'text', [
                    'label' => 'Encryption de l\'adresse mail',
                    'required' => true,
                    'value' => MAIL_ENCRYPTION,
                    'attr' => [
                        'placeholder' => "Encryption de l\'adresse mail"
                    ]
                ])
                ->add('submit', 'submit', [
                    'label' => 'Sauvegarder',
                    'attr' => [
                        'class' => "button-blue input-button"
                    ]
                ])
            );

                

    }

    public function configureOptions(): void
    {
        $this
            ->addConfig('method', 'POST')
            ->setName('configMail')
            ->addConfig('attr', [
                "class"=>"form-style",
            ]);
    }
}