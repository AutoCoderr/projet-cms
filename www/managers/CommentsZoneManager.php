<?php 

namespace cmsProject\managers;

use cmsProject\models\comments_zones;

use cmsProject\core\Manager;

use cmsProject\core\QueryBuilder;
use cmsProject\core\connection\PDOConnection;

class CommentsZoneManager extends Manager {

    public function __construct() {
        parent::__construct(comments_zones::class, 'comments_zones');
    }

}