<?php 

namespace cmsProject\managers;

use cmsProject\models\expositions;

use cmsProject\core\Manager;

use cmsProject\core\QueryBuilder;
use cmsProject\core\connection\PDOConnection;

class ExpositionManager extends Manager {

    public function __construct() {
        parent::__construct(expositions::class, 'expositions');
    }

}