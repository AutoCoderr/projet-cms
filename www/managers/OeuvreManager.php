<?php 

namespace cmsProject\managers;

use cmsProject\models\oeuvres;

use cmsProject\core\Manager;

use cmsProject\core\QueryBuilder;
use cmsProject\core\connection\PDOConnection;

class OeuvreManager extends Manager {

    public function __construct() {
        parent::__construct(oeuvres::class, 'oeuvres');
    }
}