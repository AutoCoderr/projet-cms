<?php 

namespace cmsProject\managers;

use cmsProject\models\comments;

use cmsProject\core\Manager;

use cmsProject\core\QueryBuilder;
use cmsProject\core\connection\PDOConnection;

class CommentManager extends Manager {

    public function __construct() {
        parent::__construct(comments::class, 'comments');
    }

    public function findByCommentsZone($comments_zone_id) {
        $query = (new QueryBuilder())
            ->select()
            ->from('comments', 'c')
            ->where('c.comments_zone_id = :comments_zone_id')
            ->setParameter('comments_zone_id', $comments_zone_id);

        return $query->getQuery()->getArrayResult(comments::class);
    }

    public function findByUser($user_id) {
        $query = (new QueryBuilder())
            ->select()
            ->from('comments', 'c')
            ->where('c.user_id = :user_id')
            ->setParameter('user_id', $user_id);

        return $query->getQuery()->getArrayResult(comments::class);
    }

}