<?php 

namespace cmsProject\managers;

use cmsProject\models\menu_items;

use cmsProject\core\Manager;

use cmsProject\core\QueryBuilder;
use cmsProject\core\connection\PDOConnection;

class ItemMenuManager extends Manager {

    public function __construct() {
        parent::__construct(menu_items::class, 'menu_items');
    }

    public function findByMenuId($menu_id) {
        $query = (new QueryBuilder())
            ->select()
            ->from('menu_items', 'm')
            ->where('m.menu_id = :menu_id')
            ->setParameter('menu_id', $menu_id);

        return $query->getQuery()->getArrayResult(menu_items::class);
    }

}