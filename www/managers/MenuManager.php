<?php 

namespace cmsProject\managers;

use cmsProject\models\menus;

use cmsProject\core\Manager;

use cmsProject\core\QueryBuilder;
use cmsProject\core\connection\PDOConnection;

class MenuManager extends Manager {

    public function __construct() {
        parent::__construct(menus::class, 'menus');
    }

}