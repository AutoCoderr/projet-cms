<?php 

namespace cmsProject\managers;

use cmsProject\models\users;

use cmsProject\core\Manager;

use cmsProject\core\QueryBuilder;
use cmsProject\core\connection\PDOConnection;

class UserManager extends Manager {

    public function __construct() {
        parent::__construct(users::class, 'users');
    }

    public function findUserByEmailAndPassword(string $email, string $password): array {

        $query = (new QueryBuilder())
            ->select()
            ->from('users', 'u')
            ->where('u.email = :email AND u.password = :password')
            ->setParameter('email', $email)
            ->setParameter('password', users::hashPasswd($password));
           
            return $query->getQuery()->getArrayResult(users::class);

    }

    public function findAllUserWithoutMe() {
        $query = (new QueryBuilder())
            ->select()
            ->from('users', 'u')
            ->where('u.id != :user_id')
            ->setParameter('user_id', $_SESSION["user_id"]);

        return $query->getQuery()->getArrayResult(users::class);
    }
}
