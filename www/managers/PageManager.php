<?php 

namespace cmsProject\managers;

use cmsProject\models\pages;

use cmsProject\core\Manager;

use cmsProject\core\QueryBuilder;
use cmsProject\core\connection\PDOConnection;

class PageManager extends Manager {

    public function __construct() {
        parent::__construct(pages::class, 'pages');
    }

    public function findPageWhereRouteIsNot($uri){
        $query = (new QueryBuilder())
        ->select()
        ->from('pages', 'p')
        ->where('p.uri != :uri')
        ->setParameter('uri', $uri);

    return $query->getQuery()->getArrayResult(pages::class);
    }
    
}