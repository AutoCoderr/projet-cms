<?php

namespace cmsProject\managers;

use cmsProject\core\Manager;
use cmsProject\models\menu_items;

use cmsProject\core\QueryBuilder;
use cmsProject\core\connection\PDOConnection;
use cmsProject\models\rights;

class RightManager extends Manager {

    public function __construct() {
        parent::__construct(rights::class, 'rights');
    }
}