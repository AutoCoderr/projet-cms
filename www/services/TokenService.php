<?php
namespace cmsProject\services;

class TokenService {

	private $encoding_method;

	private $secret_key;
	private $user_id;

	public function __construct($encoding_method = "AES-128-CBC") {
		$this->encoding_method = $encoding_method;
	}
	
	/*
	* Permet de générer un token a partir d'une secret_key et d'un user_id
	*/
	public function generate(): string {

		$ivlength = openssl_cipher_iv_length( $this->encoding_method ); // détermine la longueur de encoded_string en fonction de la méthode donné
		$encoded_string = openssl_random_pseudo_bytes($ivlength);

		$openssl_string = openssl_encrypt($this->user_id, $this->encoding_method, $this->secret_key, OPENSSL_RAW_DATA, $encoded_string);
		$hmac_string = hash_hmac('sha256', $openssl_string, $this->secret_key, true);

		$timestamp = time(); // récupère le timestamp

		return base64_encode( $encoded_string . $timestamp . $hmac_string . $openssl_string );
	}
	
	/*
	* Permet de décoder le token pour récuperer un user_id tout en vérifiant que le token n'est pas expiré
	*/
	public function handle(string $token) {

		$iv_length = openssl_cipher_iv_length($this->encoding_method);

		$decodedToken = base64_decode($token);

		$encoded_string = substr($decodedToken, 0, $iv_length);
		$timestamp = substr($decodedToken, $iv_length, $timestamp_length=10);
		$hmac_string = substr($decodedToken, $iv_length + $timestamp_length, $hmac_length=32);
		$openssl_string = substr($decodedToken, $iv_length + $timestamp_length + $hmac_length);

		if(!$this->isExpired($timestamp)){
			
			$data = openssl_decrypt($openssl_string, $this->encoding_method, $this->secret_key, OPENSSL_RAW_DATA, $encoded_string);

			if( intval($data) ) {
				return $data;
			}
		}
		
		return null;
	}

	private function isExpired(int $timestamp){

		// vérifie que le timestamp token est inférieur au timestamp actuel et a été généré il y a moins d'une minute  
		if($timestamp >= (time() - 60) && $timestamp <= time()) {
			return false;
		} else {
			return true;
		}
	}

	public function setSecret_key(string $secret_key): self 
	{
		$this->secret_key = $secret_key;

		return $this;
	}

	public function getSecret_Key(): string 
	{
		return $this->secret_key;
	}

	public function setUser_id(int $user_id): self 
	{
		$this->user_id = $user_id;

		return $this;
	}

	public function getUser_id(): int 
	{
		return $this->user_id;
	}
}