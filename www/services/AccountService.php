<?php
namespace cmsProject\services;
use cmsProject\models\users;

use cmsProject\core\Mail;
use cmsProject\core\helpers;

use cmsProject\managers\UserManager;
use cmsProject\managers\CommentManager;


class AccountService {
	private static $instance;

	private function __construct() {}

	public static function getInstance() {
		if (!(self::$instance instanceof self)) {
			self::$instance = new AccountService();
		}
		return self::$instance;
	}

	public function authenticate($username, $password) {

		$userManager = new UserManager();
        $result = $userManager->findUserByEmailAndPassword($_POST['email'], $_POST['password']);

        if (!empty($result)) {

        	$_SESSION['user_id'] = $result[0]->getId();
        	$_SESSION['email'] = $result[0]->getEmail();
            $_SESSION['firstname'] = $result[0]->getFirstname();
            $_SESSION['lastname'] = $result[0]->getLastname();
            $_SESSION['right'] = $result[0]->getRight()->getRole();

            return true;

        }

        return false;
	}

    public function resetPassword($email)
    {

        $userManager = new UserManager();
        $users = $userManager->findBy(['email' => $email]);

        if (empty($users)) {
            return false;
        }

        // génere un mot de passe aléatoire
        $password = helpers::generateRandomString();

        // Attribue le mot de passe à l'utilisateur
        $user = $users[0];
        $user->setPassword(users::hashPasswd($password));
        if ($user->getHas_rights() == "0") {
            $user->setHas_rights(false);
        } else {
            $user->setHas_rights(true);
        }
        $userManager->save($user);


        $mail = new Mail();

        $mail->setDestinataire($email, $user->getFirstname() . " " . $user->getLastname());
        $mail->setObject('Nouveau mot de passe - Art CMS');
        $mail->setBody("<html><head></head><body><b>Bonjour " . $user->getFirstname() . " " . $user->getLastname() . "</b>,<br> Voici votre nouveau mot de passe pour le site Art CMS : <i>" . $password . "</i></body></html>");
        $mail->setAltBody("Bonjour " . $user->getFirstname() . " " . $user->getLastname() . ", Voici votre nouveau mot de passe pour le site Art CMS : " . $password);


        if (!$mail->send()) {
            return false;
        }


        unset($_POST['email']);
        return true;
    }

	public function logout() {

	    session_destroy();

        return true;
        
    }

	public function addUser($data, $right = 3) { //3 per default => classic user rights

        $userManager = new UserManager();

		$user = new users();
		$user->setFirstname($data['firstname']);
        $user->setLastname($data['lastname']);
		$user->setEmail($data['email']);
		$user->setPassword(users::hashPasswd($data['password']));
		$user->setRight_id($right);
		$user->setCreated_at(date("Y-m-d"));

        $mail = new Mail();

        $mail->setDestinataire($user->getEmail(), $user->getFirstname() . " " . $user->getLastname());
        $mail->setObject('Creation de compte - '.SITE_NAME);
        $mail->setBody("<html><head></head><body><b>Bonjour " . $user->getFirstname() . " " . $user->getLastname() . "</b>,<br> Votre compte a bien été créé sur le site ".SITE_NAME."</body></html>");
        $mail->setAltBody("Bonjour " . $user->getFirstname() . " " . $user->getLastname() . ", Votre compte a bien été créé sur le site ".SITE_NAME);


        if (!$mail->send()) {
            return false;
        }

        $userManager->save($user);

        return true;
	}

	public function updateUser() {
		
	}

	public function deleteUser() {
        if(empty($_SESSION['user_id']) || empty($_GET["user_id"])){
            new UnauthorizedException("Vous n'êtes pas connecté ou vous n'avez pas renseigner le bon id", 401);
            return false;
        }elseif ($_SESSION['user_id'] != $_GET['user_id'] && $_SESSION['right'] != "administrateur") {
            new UnauthorizedException("Vous devez être admin pour supprimer l'utilisateur", 401);	
            return false;
		}else{
            $userManager = new UserManager();
            $user = $userManager->find($_GET['user_id']);
            $commentManager = new CommentManager();
            $comments = $commentManager->findByUser($user->getId());
            foreach($comments as $comment){
                $commentManager->delete($comment->getId());
            }
            $userManager->delete($_GET['user_id']);
            if($_SESSION['user_id'] == $_GET['user_id']){
                $this->logout();
            }
            
            
            return true;
        }
		
	}
}