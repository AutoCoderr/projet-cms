<?php
namespace cmsProject\services;

use cmsProject\core\connection\PDOConnection;
use cmsProject\core\DetectTagAndParams;
use cmsProject\core\helpers;
use cmsProject\core\QueryBuilder;
use cmsProject\core\View;
use cmsProject\core\ExceptionHandler;

use cmsProject\managers\CommentManager;
use cmsProject\managers\ExpositionManager;
use cmsProject\managers\CommentsZoneManager;
use cmsProject\managers\MenuManager;
use cmsProject\managers\OeuvreManager;
use cmsProject\managers\PageManager;

use cmsProject\models\menus;
use cmsProject\models\pages;

class PageService {
	private static $instance;

	private function __construct() {}

	public static function getInstance() {
		if (!(self::$instance instanceof self)) {
			self::$instance = new PageService();
		}
		return self::$instance;
	}

    /**
     * Récupere toutes les infos pour l'édition d'une page
     */
	public function getInfoPage($id) {
	    $page = (new PageManager())->find($id);
        if ($page->getNom() == null) {
            new ExceptionHandler ("Cette page n'existe pas ou vous ne pouvez pas l'éditer");
        }
        $menus = $page->getMenus();
        $query = new QueryBuilder(new PDOConnection());
        if (count($menus) > 0) {
            $ids_menus = [];
            foreach ($menus as $menu) {
                $ids_menus[] = $menu->getId();
            }
            $otherMenusModels = $query->select("*")->from("menus", "M")
                ->where("id NOT IN (" . implode(",", $ids_menus) . ")")
                ->getQuery();
        } else {
            $otherMenusModels = $query->select("*")->from("menus", "M")
                ->getQuery();
        }

        $otherMenusModels = $otherMenusModels->getArrayResult(menus::class);

        $otherMenus = [];
        foreach ($otherMenusModels as $menu) {
            $otherMenus[] = ["value" => $menu->getId(), "display" => $menu->getNom()];
        }
        $oeuvres = (new OeuvreManager())->findAll();
        $expositions = (new ExpositionManager())->findAll();
        $comments_zones = (new CommentsZoneManager())->findAll();
        return ["page" => $page, "menus" => $menus, "otherMenus" => $otherMenus, "oeuvres" => $oeuvres, 'expositions' => $expositions, 'comments_zones' => $comments_zones];
    }

    /**
     * Ajoute un menu dans une page
     */
    public function addMenu($idMenu,$idPage) {
        $menu = (new MenuManager())->find($idMenu);
        $page = (new PageManager())->find($idPage);

	    $menusPages = $page->getMenus();
	    $biggerRank = 0;
	    foreach ($menusPages as $unMenu) {
	        if ($unMenu->getId() == $menu->getId()) {
                return false;
            } if ($unMenu->getRank() > $biggerRank) {
                $biggerRank = $unMenu->getRank();
            }
        }
	    return $page->addMenu($menu->getId(), $biggerRank+1);
	}

	public function removeMenu($idMenu,$idPage) {
	    $menu = (new MenuManager())->find($idMenu);
        $page = (new PageManager())->find($idPage);

        return $page->removeMenu($idMenu);
    }

    public function upOrDownMenuInList($idMenu,$idPage,$action = "up") {
	    $page = (new PageManager())->find($idPage);
        $menu = (new MenuManager())->find($idMenu);

	    $menus = $page->getMenus();
        if ($action == "down") {
            $menus = array_reverse($menus);
        }
        $precedentMenu = null;
        foreach ($menus as $unMenu) {
            if ($unMenu->getId() == $menu->getId()) {
                $menu = $unMenu;
                break;
            } else {
                $precedentMenu = $unMenu;
            }
        }

        if (!$page->updateMenu($menu->getId(), ["rank" => $precedentMenu->getRank()])) {
            return false;
        }
        if (!$page->updateMenu($precedentMenu->getId(), ["rank" => $menu->getRank()])) {
            return false;
        }

	    return true;
    }

    // Detecte toutes les balises de <login></login>, et affiche un lien de connexion/deconnexion
    private function detectAndDisplayLoginLinkInPage($content) {
        $content = $this->detectAndDisplayTag($content, "<login>", "</login>", [], function ($params, $i) {
            if (!isset($_SESSION['user_id'])) {
                $html = "<a href='" . helpers::getUrl("user", "login") . "'>Se connecter</a>";
            } else {
                $html = "<a href='javascript:logout()'>Se déconnecter</a>";
            }
            return ["html" => $html, "javascript" => ""];
        });
        return $content;
    }

    // Detecte toutes les balises de <register></register>, et affiche un lien d'inscritpion
    private function detectAndDisplayRegisterLinkInPage($content) { 
        $content = $this->detectAndDisplayTag($content, "<register>", "</register>", [], function ($params, $i) {
            if (!isset($_SESSION['user_id'])) {
                $html = "<a href='" . helpers::getUrl("user", "register") . "'>S'inscrire</a>";
            } else {
                $html = "";
            }
            return ["html" => $html, "javascript" => ""];
        });
        return $content;
    }

    // Detecte toutes les balises de <comment></comment-zone> et affiche les commentaires
    private function detectAndDisplayCommentZoneInPage($content) {
        $content = $this->detectAndDisplayTag($content, "<comment-zone>", "</comment-zone>", ["id"], function ($params, $i, $nbCommentsZones) {
            $javascript = "";
            $comments_zone = (new CommentsZoneManager())->find((int)$params["id"]);
            if ($comments_zone) {
                $commentManager = new CommentManager();
                $comments = $commentManager->findByCommentsZone($params["id"]);

                $view = new View("post-comment", "blank");

                $view->assign("comments", $comments);
                $view->assign("commentZone", $comments_zone);
                $html = $view->renderView();
            } else {
                $html = "<b>Cette zone de commentaire n'existe plus</b><br/>";
            }
            if ($i == $nbCommentsZones-1) {
                $javascript .= "$('.btn-delete-comment').click(function (){
                    event.preventDefault();
                    if (confirm('Voulez vous bien supprimer ce commentaire?')) {
                        location.href = this.href;
                    }
                });";
            }
            return ["html" => $html, "javascript" => $javascript];
        });
        return $content;
    }

    // Detecte toutes les balises de <oeuvre></oeuvre>, et affiche les oeuvres
    private function detectAndDisplayOeuvresInPage($content) { 
        $content = $this->detectAndDisplayTag($content, "<oeuvre>", "</oeuvre>", ["id"], function ($params, $i) {
            $oeuvre = (new OeuvreManager())->find((int)$params["id"]);
            if ($oeuvre) {
                $html = $oeuvre->getNom() . " : <br/>" . $oeuvre->getDescription() . "<br/>";
                if ($oeuvre->getUrl_image() != "") {
                    $html .= "<img width='300' height='auto' src='" . $oeuvre->getUrl_image() . "'/>";
                }
            } else {
                $html = "<b>Cette oeuvre n'existe plus</b><br/>";
            }
            return ["html" => $html, "javascript" => ""];
        });
       return $content;
    }

    /**
     * detecter toutes les balises '<exposition></exposition>' et affiche leur
     * et remplace les balises '<exposition></expostion>', par les listes des expositions "oeuvres";
     * générer, le code html du <select>, pour sélectionner les 'oeuvres', et le code javascript, pour détecter 'l'oeuvre' sélectionnée, et l'afficher
     */
    private function detectAndDisplayExposisionsInPage($content) {
        $content = $this->detectAndDisplayTag($content, "<exposition>", "</exposition>", ["id"], function ($params, $i) {
            $exposition = (new ExpositionManager())->find((int)$params["id"]);
            $javascript = "";
            if ($exposition) {
                $oeuvres = $exposition->getOeuvres();
                $html = $exposition->getNom() . " : <br/>" . $exposition->getDescription() . "<br/>";
                if (count($oeuvres) == 0) {
                    $html .= "<strong>Il n'y a aucune oeuvre dans cette exposition</strong><br/>";
                } else {
                    $html .= "<select id='selectOeuvreInExposition" . $i . "'>";
                    $html .= "<option value='0' selected='selected'>Choississez une oeuvre</option>";
                    $javascript .= "document.getElementById('selectOeuvreInExposition" . $i . "').onchange = function() {
                                    let sel = document.getElementById('selectOeuvreInExposition" . $i . "');
                                    switch(sel.value) {";
                    foreach ($oeuvres as $oeuvre) {
                        $html .= "<option value='" . $oeuvre->getId() . "'>" . $oeuvre->getNom() . "</option>";
                        $javascript .= "
                                        case '" . $oeuvre->getId() . "': 
                                        document.getElementById('displayOeuvreInExposition" . $i . "').innerHTML =
                                            `Oeuvre : <br/>
                                                " . $oeuvre->getNom() . "<br/>
                                                " . $oeuvre->getDescription() . "<br/>
                                                <img width='300' height='auto' src='" . $oeuvre->getUrl_image() . "'/>`
                                        break;";
                    }
                    $html .= "</select>";
                    $html .= "<div id='displayOeuvreInExposition" . $i . "'></div>";
                    $javascript .= "\ndefault:
                                   document.getElementById('displayOeuvreInExposition" . $i . "').innerHTML = '';";
                    $javascript .= "\n} \n};";
                }
            } else {
                $html = "<b>Cette exposition n'existe plus</b><br/>";
            }
            return ["html" => $html, "javascript" => $javascript];
        });
        return $content;
    }
    
    public function detectAndDisplayCommentsZonesInPage($content) {
        $content = $this->detectAndDisplayTag($content, "<commentsZone>", "</commentsZone>", ["id"], function ($params, $i) {
            $commentsZone = (new CommentsZoneManager())->find((int)$params["id"]);
            $displayCommentsZone = $commentsZone->getContent()." : <br/>";
            return ["html" => $displayCommentsZone, "javascript" => ""];
        });
       return $content;
    }
    
    public function detectAndDisplayCommentsInPage($content) {
        $content = $this->detectAndDisplayTag($content, "<comment>", "</comment>", ["id"], function ($params, $i) {
            $comment = (new CommentManager())->find((int)$params["id"]);
            $displayComment = $comment->getContent()." : <br/>";
            return ["html" => $displayComment, "javascript" => ""];
        });
       return $content;
    }

    // Détecte les positions et les paramètres d'une balise html dans une page html
    // et, remplace la balise par un contenu spécifique à partir d'un callback
    private function detectAndDisplayTag($content, $tagOpen, $tagClose, $params, $callback) { 
	    
        $detectTagAndParams = new DetectTagAndParams($content);
        $detectTagAndParams->getTagAndParams($tagOpen, $tagClose, $params);

        $javascript = "";
        $diff = 0;

        for ($i = 0;$i<count($detectTagAndParams->starts);$i++) {
            $display = $callback($detectTagAndParams->params[$i], $i, count($detectTagAndParams->starts));
            $javascript .= $display["javascript"];
            $html = $display["html"];

            $lenOfContent = strlen($content);
            $content =
                substr($content, 0, $detectTagAndParams->starts[$i]-$diff)
                .$html
                .substr($content, $detectTagAndParams->starts[$i]-$diff+$detectTagAndParams->lengths[$i], strlen($content));
            $diff += $lenOfContent-strlen($content);
        }
        if ($javascript != "") {
            $content .= "\n<script>\n".$javascript."\n</script>";
        }
        return $content;
    }

    public function accessPage($page) {
        $content = $this->detectAndDisplayOeuvresInPage($page->getContent());
        $content = $this->detectAndDisplayExposisionsInPage($content);
        $content = $this->detectAndDisplayLoginLinkInPage($content);
        $content = $this->detectAndDisplayRegisterLinkInPage($content);
        $content = $this->detectAndDisplayCommentZoneInPage($content);
        $menus = $page->getMenus();
        // Génère une barre des taches avec les menus
        $view = new View($content, "custom-front");
        $view->assign('menus', $menus);
    }

    public function findPageByUri() {
        $queryBuilder = new QueryBuilder();
        $route = explode("?",$_SERVER["REQUEST_URI"])[0];
        $queryBuilder->reset();
        $pages = $queryBuilder->select("*")
            ->from("pages", "P")
            ->where("uri = :uri")
            ->setParameter("uri", $route)
            ->getQuery()->getArrayResult(pages::class);
        if (count($pages) == 0) {
            return null;
        }
        return $pages[0];
    }
}
