$(document).ready(function () {
  $('#configDatabase').css('display', 'none');
  $('#configMail').css('display', 'none');

  $('.tabs a').click(function (event) {
    event.preventDefault();

    $('.tabs a').removeClass('active');
    $(event.target).addClass('active');

    if (event.target.id == 'configSiteTab') {
      $('#configSite').css('display', 'block');
      $('#configDatabase').css('display', 'none');
      $('#configMail').css('display', 'none');
    } else if (event.target.id == 'configDatabaseTab') {
      $('#configSite').css('display', 'none');
      $('#configDatabase').css('display', 'block');
      $('#configMail').css('display', 'none');
    } else {
      $('#configSite').css('display', 'none');
      $('#configDatabase').css('display', 'none');
      $('#configMail').css('display', 'block');
    }
  });
});
