$(document).ready(function () {
  $.ajax({
    url: '/artcms/access_token',
    type: 'GET',
    data: {},
    dataType: 'json',
    success: function (response) {
      generateGraph(response.token);
    },
  });
});

// Génère le token
// En cas de succès, affiche le graphique dans l'overview grace chartJS
function generateGraph(token) {
  $.ajax({
    url: '/artcms/dashboard/graph',
    type: 'GET',
    dataType: 'json',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', token);
    },
    data: {},
    success: function (response, status) {
      var data = {
        labels: [],
        datasets: [
          {
            label: 'Overview',
            data: [],
            backgroundColor: [
              'rgba(255, 99, 132, 0.5)',
              'rgba(54, 162, 235, 0.5)',
              'rgba(255, 206, 86, 0.5)',
              'rgba(75, 192, 192, 0.5)',
              'rgba(153, 102, 255, 0.5)',
              'rgba(255, 159, 64, 0.5)',
            ],
          },
        ],
      };

      response.data.forEach(function (object) {
        data.labels.push(object.label);
        data.datasets[0].data.push(object.value);
      });

      var options = {};

      var myChart = new Chart($('#chart'), {
        type: 'bar',
        data: data,
        options: options,
      });
    },
    error: function (response, status, error) {
      alert(error);
    },
  });
}
