let logoutPath;
let deletePagePath;
let deleteMenuPath;
let deleteOeuvrePath;
let deleteExpositionPath;

function confirmationPage(id) {
	if (confirm("Etes-vous sur de vouloir supprimer cette page ?")){
		location.href=deletePagePath+"?page_id="+id;
	}
}

function confirmationMenu(id) {
	if (confirm("Etes-vous sur de vouloir supprimer ce menu ?")){
		location.href=deleteMenuPath+"?menu_id="+id;
	}
}

function confirmationOeuvre(id) {
	if (confirm("Etes-vous sur de vouloir supprimer cette oeuvre ?")){
		location.href=deleteOeuvrePath+"?oeuvre_id="+id;
	}
}

function confirmationExposition(id) {
	if (confirm("Etes-vous sur de vouloir supprimer cette exposition ?")){
		location.href=deleteExpositionPath+"?exposition_id="+id;
	}
}

function confirmationCommentsZone(id) {
	if (confirm("Etes-vous sur de vouloir supprimer cette zone de commentaire ?")){
		location.href=deleteCommentsZonePath+"?comments_zone_id="+id;
	}
}

function confirmationUser(id) {
	if (confirm("Etes-vous sur de vouloir supprimer cet utilisateur ?")){
		location.href=deleteUserPath+"?user_id="+id;
	}
}

$(document).ready(function() {
	const logout = document.getElementById('button-logout');
	if (logout) {
		logout.addEventListener("click", function () {
				if (confirm("Voulez vous vous déconnecter ?")) {
					location.href = logoutPath;
				}
			}
		)
	}
});