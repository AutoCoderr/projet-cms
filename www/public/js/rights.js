$(document).ready(function () {
  $('.select-role').change(function (event) {
    $.ajax({
      url: '/artcms/access_token',
      type: 'GET',
      data: {},
      dataType: 'json',
      success: function (response) {
        update_rights(event, response.token);
      },
    });
  });
});

// Met à jour le role d'un utilisateur
function update_rights(event, token) {
  var element = event.target.parentNode.parentNode.getElementsByTagName('h3');

  var mail = element[0].textContent;
  var idRight = event.target.value;

  $.ajax({
    url: '/artcms/ajax/right',
    type: 'post',
    data: {
      right_id: idRight,
      email: mail,
    },
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', token);
    },
    dataType: 'json',
  });
}
