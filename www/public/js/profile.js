$(document).ready(function () {
  $('#changePwd').css('display', 'none');

  $('.tabs a').click(function (event) {
    event.preventDefault();

    $('.tabs a').removeClass('active');
    $(event.target).addClass('active');

    if (event.target.id == 'profileTab') {
      $('#profile').css('display', 'block');
      $('#changePwd').css('display', 'none');
    } else {
      $('#profile').css('display', 'none');
      $('#changePwd').css('display', 'block');
    }
  });
});
