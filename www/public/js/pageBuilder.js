let editor;
// Instanciation de TinyMCE avec tout les parametres (boutons, Modules etc...) qui s'affichent
tinymce.init({
  selector: '#tinyMCE_elem',
  plugins: 'autolink lists media table image link imagetools',
  toolbar:
    'a11ycheck addcomment showcomments casechange checklist code formatpainter pageembed permanentpen table basicDateButton AddOeuvreButton AddExpositionButton AddCommentZoneButton AddLoginButton AddRegisterButton',
  toolbar_mode: 'floating',
  tinycomments_mode: 'embedded',
  tinycomments_author: 'Author name',
  valid_elements: '*[*]',
  setup: function (ed) {
    ed.on('init', function (ed) {
      if (content != null) {
        tinymce.activeEditor.setContent(content);
      }
    });
    ed.on('change', function (e) {
      window.onbeforeunload = function () {
        return '';
      };
    });
    // Affichage des boutons pour afficher les modals pour ajouter les Oeuvres/Expositions/ZonesDeCommentaires/Inscription/Connexion
    ed.ui.registry.addButton('AddOeuvreButton', {
      text: 'Ajouter oeuvre',
      onAction: function (_) {
        editor = ed;
        document.getElementById('selectOeuvre-modal').style.display = 'block';
      },
    });
    ed.ui.registry.addButton('AddExpositionButton', {
      text: 'Ajouter exposition',
      onAction: function (_) {
        editor = ed;
        document.getElementById('selectExposition-modal').style.display =
          'block';
      },
    });
    ed.ui.registry.addButton('AddCommentZoneButton', {
      text: 'Ajouter zone de commentaire',
      onAction: function (_) {
        editor = ed;
        document.getElementById('selectComments_zone-modal').style.display =
          'block';
      },
    });
    ed.ui.registry.addButton('AddLoginButton', {
      text: 'Ajouter liens de connexion',
      onAction: function (_) {
        ed.insertContent(
          "<login style='font-weight: bold'>Lien de connexion</login>"
        );
      },
    });
    ed.ui.registry.addButton('AddRegisterButton', {
      text: "Ajouter liens d'inscription",
      onAction: function (_) {
        ed.insertContent(
          "<register style='font-weight: bold'>Lien d'inscription</register>"
        );
      },
    });
  },
});

// Requete ajax pour sauvegarder les modifications de la page dans base de donnée
function save() {
  $('#save_answer').css('color', 'orange');
  $('#save_answer').html('Sauvegarde en cours...');
  $.post(
    '/artcms/page/save',
    {
      id: idPage,
      content: tinymce.activeEditor.getContent(),
    },
    (data) => {
      if (data.rep === 'failed') {
        $('#save_answer').css('color', 'red');
        $('#save_answer').html(data.msg);
      } else if (data.rep === 'success') {
        $('#save_answer').css('color', 'green');
        $('#save_answer').html('Page sauvegardée avec succès !');
        window.onbeforeunload = null;
      }
    },

    'json'
  );
}

// Validation des modals pour l'ajouts des oeuvres, expositions et zones de commentaires
$('#addOeuvre').click(() => {
  let sel = document.getElementById('oeuvre');
  editor.insertContent(
    "&nbsp;<oeuvre style='font-weight: bold' id='" +
      sel.value +
      "'>Oeuvre : " +
      sel.options[sel.selectedIndex].text +
      '</oeuvre>&nbsp;'
  );
  document.getElementById('selectOeuvre-modal').style.display = 'none';
});
$('#addExposition').click(() => {
  let sel = document.getElementById('exposition');
  editor.insertContent(
    "&nbsp;<exposition style='font-weight: bold' id='" +
      sel.value +
      "'>Exposition : " +
      sel.options[sel.selectedIndex].text +
      '</exposition>&nbsp;'
  );
  document.getElementById('selectExposition-modal').style.display = 'none';
});
$('#addComments_zone').click(() => {
  let sel = document.getElementById('comments_zone');
  editor.insertContent(
    "&nbsp;<comment-zone style='font-weight: bold' id='" +
      sel.value +
      "'>Zone de commentaire : " +
      sel.options[sel.selectedIndex].text +
      '</comment-zone>&nbsp;'
  );
  document.getElementById('selectComments_zone-modal').style.display = 'none';
});
