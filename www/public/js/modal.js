let controllerToCall;
// Adapte le modal au démarrage en fonction de si c'est un ajout ou une modification
$(document).ready(() => {
  const url = location.href.split('?');
  const args = typeof url[1] != 'undefined' ? url[1] : '';
  let action = url[0].split('/');
  action = action[action.length - 1];
  switch (action) {
    case 'add':
      setAddModal();
      break;
    case 'update':
      setUpdateModal(args);
  }
});

/* Modal Dashboard Ajout  Pages/Oeuvres/Menus/Billeterie */
var btn = document.getElementById('dasboardplus');
if (btn) {
  var modal = document.getElementById('dashboard-modal');
  var span_close = document.getElementsByClassName('close')[0];

  btn.onclick = function () {
    setAddModal();
    modal.style.display = 'block';
  };

  span_close.onclick = function () {
    $('input').val('');
    modal.style.display = 'none';
  };

  window.onclick = function (event) {
    if (event.target === modal) {
      modal.style.display = 'none';
    }
  };
}

// Défini le modal pour les ajouts
function setAddModal() {
  var redirectUrl = '/artcms/' + controllerToCall + '/add';
  $('form').attr('action', redirectUrl);
  $('input:submit').val('Ajouter');
}
// Défini le modal pour les modifications
function setUpdateModal(args) {
  var redirectUrl = '/artcms/' + controllerToCall + '/update?' + args;
  $('form').attr('action', redirectUrl);
  $('input:submit').val('Modifier');
}

$('.btn_edit').click(function (event) {
  event.preventDefault();
  $.ajax({
    url: '/artcms/access_token',
    type: 'GET',
    data: {},
    dataType: 'json',
    success: function (response) {
      build_modal(event, response.token);
    },
  });

  modal.style.display = 'block';
});

// Contruit le modal en detectant les différents types d'input
function build_modal(event, token) {
  $.ajax({
    url: event.currentTarget.href,
    type: 'GET',
    dataType: 'json',
    data: {},
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', token);
    },
    success: function (response, status) {
      for (var property in response) {
        var input = $("input[name='" + property + "']");

        if (input.length == 0) {
          input = $("div[name='" + property + "']");
        }

        if (input.length != 0) {
          if (
            typeof response[property] == 'string' ||
            response[property] == null
          ) {
            input.val(response[property] != null ? response[property] : '');
          } else if (
            typeof response[property] == 'object' &&
            property.substring(0, 6) !== 'other_'
          ) {
            // Si un input correspond à des éléments que l'on peut retirer ou ajouter à l'élément que l'on modifie permet de retirer ou ajouter des éléments à l'interface
            input.html('');
            input.removeProp('name');
            let str = "<ul id='" + property + "_ul'>";
            for (let i = 0; i < response[property].length; i++) {
              str +=
                '<li>' +
                response[property][i].name +
                "<input type='hidden' name='" +
                property +
                "[]' value='" +
                response[property][i].id +
                "' />";
              str += "<div style='display: inline-block; width: 80px'>";
              str +=
                "<input type='button' value='Remove' data-property='" +
                property +
                "' data-id='" +
                response[property][i].id +
                "'/></div></li>";
            }
            str += '</ul>';
            const ul = $(str);
            const inputRemoves = ul.find('input');
            for (let i = 0; i < inputRemoves.length; i++) {
              $(inputRemoves[i]).click(removeElementFromList);
            }
            input.append(ul);
            var divOthers = $("div[name='other_" + property + "']");
            if (divOthers != 0) {
              divOthers.html('');
              divOthers.removeProp('name');
              if (typeof response['other_' + property] == 'object') {
                let str = "<ul id='other_" + property + "_ul'>";
                for (let i = 0; i < response['other_' + property].length; i++) {
                  str +=
                    '<li>' +
                    response['other_' + property][i].name +
                    "<input type='hidden' value='" +
                    response['other_' + property][i].id +
                    "' />";
                  str += "<div style='display: inline-block; width: 80px'>";
                  str +=
                    "<input type='button' value='Add' data-property='" +
                    property +
                    "' data-id='" +
                    response['other_' + property][i].id +
                    "'/></div></li>";
                }
                str += '</ul>';
                let ulOthers = $(str);
                const inputAdds = ulOthers.find('input');
                for (let i = 0; i < inputAdds.length; i++) {
                  $(inputAdds[i]).click(AddElementToList);
                }
                ulOthers.prepend('<label>Ajouter :</label>');
                divOthers.append(ulOthers);
              }
            }
          }
        }
      }

      var splitUrl = event.currentTarget.href.split('?');
      setUpdateModal(splitUrl[1]);
    },

    error: function (response, status, error) {
      alert(error);
    },
  });
}

// Action qui s'execute lorsque l'on retire un élément dans la liste
function removeElementFromList() {
  let inputRemove = $(this);
  let property = inputRemove.attr('data-property');
  let liRemove = inputRemove.parent().parent();
  let liAdd = liRemove.clone();
  let inputAdd = $(liAdd.find('input')[0]);
  inputAdd.removeAttr('name');
  let inputAddButton = $(liAdd.find('input')[1]);
  inputAddButton.val('Add');
  inputAddButton.click(AddElementToList);
  $('#other_' + property + '_ul').append(liAdd);
  liRemove.remove();
}

// Action qui s'execute lorsque l'on ajoute un élément dans la liste
function AddElementToList() {
  let inputAdd = $(this);
  let property = inputAdd.attr('data-property');
  let liAdd = inputAdd.parent().parent();
  let liRemove = liAdd.clone();
  let inputRemove = $(liRemove.find('input')[0]);
  inputRemove.attr('name', property + '[]');
  let inputRemoveButton = $(liRemove.find('input')[1]);
  inputRemoveButton.val('Remove');
  inputRemoveButton.click(removeElementFromList);
  $('#' + property + '_ul').append(liRemove);
  liAdd.remove();
}

/* Modal choix oeuvre dans pageBuilder */
var modal_selectOeuvre = document.getElementById('selectOeuvre-modal');
var span_selectOeuvre = document.getElementsByClassName(
  'close-selectOeuvre'
)[0];

span_selectOeuvre.onclick = function () {
  modal_selectOeuvre.style.display = 'none';
};

window.onclick = function (event) {
  if (event.target === modal_selectOeuvre) {
    modal_selectOeuvre.style.display = 'none';
  }
};

/* Modal choix exposition dans pageBuilder */
var modal_selectExposition = document.getElementById('selectExposition-modal');
var span_selectExposition = document.getElementsByClassName(
  'close-selectExposition'
)[0];

span_selectExposition.onclick = function () {
  modal_selectExposition.style.display = 'none';
};

window.onclick = function (event) {
  if (event.target === modal_selectExposition) {
    modal_selectExposition.style.display = 'none';
  }
};

/* Modal choix comment dans pageBuilder */
var modal_selectCommentZone = document.getElementById(
  'selectComments_zone-modal'
);
var span_selectCommentZone = document.getElementsByClassName(
  'close-selectComments_zone'
)[0];

span_selectCommentZone.onclick = function () {
  modal_selectCommentZone.style.display = 'none';
};

window.onclick = function (event) {
  if (event.target === modal_selectCommentZone) {
    modal_selectCommentZone.style.display = 'none';
  }
};
