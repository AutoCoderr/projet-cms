<?php
session_cache_expire(30);
session_start();

include 'autoloader.php';
use cmsProject\core\ConstantLoader;
use cmsProject\core\Installer\InitDB;
use cmsProject\Router;

new ConstantLoader();

$uriExploded = explode("/", $_SERVER['REQUEST_URI']);
if ($uriExploded[1] == "artcms" && $uriExploded[2] == "install" && $_SERVER['REQUEST_METHOD'] == "POST") {
    (new Router())->manageUrl();
    die();
}

//on lance l'installeur quand on connecte la première fois
$installer = new cmsProject\core\Installer\Installer();

//Genere un .env si le .env n'existe pas
if (!file_exists('.env')) $installer->generateEnv();

//Verifie l'integrité des variables dans .env
$installer->checkPresentsConstants();
if($installer->anyConstantMissing()) {
    //Redirige vers le form pour demander les variables du .env
    $view = new \cmsProject\core\View('setup', 'installer');
    $view->assign('installer', $installer);

    die();
}
$installer->checkPresentsDefaultsConstants();
if ($installer->anyDefaultConstantMissing()) {
    $installer->writeDefaultConst();
    new ConstantLoader();
}

//On instancie la classe initialisation de la base de données
$initDB = new InitDB();
if ($initDB->checkIfDatabaseIsEmpty()) {
    $initDB->createDatabase();
}
if ($initDB->thereIsNothingAdmin()) {
    $view = new \cmsProject\core\View('createfirstuser', 'installer');
    die();
}

if ($initDB->thereIsNothingHomepage()) {
    $initDB->createFirstHomepage();
}

(new Router())->manageUrl();

