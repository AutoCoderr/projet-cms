# Arts CMS

Projet Annuel de 3ème année Ingénieurie du WEB à l'école du génie informatique (ESGI).

Le sujet est de réaliser un **CMS** en **PHP from scratch**.

## Installation

Ce projet utilise la virtualisation **Docker** pour fonctionner !

Pour importer le projet, vous pouvez exécutez la commande suivante:  
````
git clone https://gitlab.com/AutoCoderr/projet-cms.git
````
Pour build le projet, il faut se mettre à la racine du projet et lancer la commande suivante :
````
docker-compose up -d
````
Il faut ensuite générer le fichier main.css dans www/public/dist. Pour cela, allez dans le répertoire www/public/webpack et lancer les commandes suivantes:
````
npm install
npm run watch
````

## Lancement

Après avoir générer le fichier main.css, vous pouvez démarrer le projet.

L'installeur se lancera et vous demandera des informations sur la database : 
- le préfix
- l'identifiant
- le mot de passe
- l'hôte
- le nom

Mais aussi des informations sur une boîte mail pour votre site :
- l'hôte
- l'identifiant
- le mot de passe
- un alias
- le port 
- le protocole (SSL/TLS)

L'installeur vous demandera également un nom pour votre site.

Enfin, après ce formulaire, il vous sera demander de renseigner les informations pour le compte administrateur.

Après ces 2 étapes, vous pouvez accéder à votre site et le modifier en allant sur le Dashboard.

## Auteurs
Étudiants 3ème année Ingénieurie du WEB à l'**ESGI**.

**BOUVET Julien** (mainteneur) @AutoCoderr

**CAI Jacques** (mainteneur) @JacquesCAI

**CHATEL Thomas** (mainteneur) @thomas.chatel 

**SEKHRI Khalil** (mainteneur) @KhalilSekhri